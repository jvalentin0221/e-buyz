@extends('layouts.frontend.master')
@section('title', "Login")
@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Home</a></li>
                    <li class='active'>Login</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="body-content">
        <div class="container">
            <div class="sign-in-page">
                <div class="row">
                    <!-- Sign-in -->
                    <div class="col-md-6 col-sm-6 sign-in">
                        <h4 class="">
                            @if(session()->get('language') == 'spanish') Iniciar sesión @else Sign in @endif
                        </h4>
                        <form method="POST" action="{{ route('login') }}" class="register-form outer-top-xs" role="form">
                            @csrf
                            <div class="form-group">
                                <label class="info-title" for="email">
                                    @if(session()->get('language') == 'spanish') Dirección de correo electrónico @else Email Address @endif
                                    <span>
                                        *
                                    </span>
                                </label>
                                <input type="email"
                                       name="email"
                                       placeholder="Email"
                                       class="form-control unicase-form-control text-input"
                                       id="email"
                                       value="{{ old('email') }}"
                                       required autocomplete="email"
                                       autofocus>
                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                            </div>
                            <div class="form-group">
                                <label class="info-title" for="password">
                                    @if(session()->get('language') == 'spanish') contraseña @else Password @endif
                                    <span>*</span>
                                </label>
                                <input type="password" name="password" placeholder="Password" class="form-control unicase-form-control text-input" id="password" required autocomplete="password" autofocus>
                                <strong class="text-danger">{{ $errors->first('password') }}</strong>
                            </div>
                            @if (session('errorMsg'))
                                <div class="text-center">
                                    <strong class="text-danger text-center">{{ session('errorMsg') }}</strong>
                                </div>
                            @endif
                            <div class="radio outer-xs">
                                <a href="{{ route('password.request') }}" class="forgot-password pull-right">
                                    @if(session()->get('language') == 'spanish') ¿Olvidaste tu contraseña? @else Forgot Your Password? @endif
                                </a>
                            </div>
                            <button type="submit" class="btn-upper btn btn-primary checkout-page-button">
                                @if(session()->get('language') == 'spanish') Acceso @else Login @endif
                            </button>
                        </form>
                    </div>
                    <!-- Sign-in -->

                    <!-- Create A New Account -->
                    <div class="col-md-6 col-sm-6 create-new-account">
                        <h4 class="checkout-subtitle">
                            @if(session()->get('language') == 'spanish') Crea una cuenta nueva @else Create a new account @endif
                        </h4>
                        <form method="POST" action="{{ route('register') }}" class="register-form outer-top-xs" role="form">
                            @csrf
                            <div class="form-group">
                                <label class="info-title" for="name">
                                    Name
                                    <span>*</span>
                                </label>
                                <input type="text" name="name" class="form-control unicase-form-control text-input"   value="{{ old('name') }}" id="name" required autocomplete="name" autofocus>
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            </div>
                            <div class="form-group">
                                <label class="info-title" for="email">
                                    @if(session()->get('language') == 'spanish') Correo electrónico @else Email @endif
                                    <span>*</span>
                                </label>
                                <input type="email" name="email"   value="{{ old('email') }}" class="form-control unicase-form-control text-input" id="email" required autocomplete="email" autofocus>
                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                            </div>
                            <div class="form-group">
                                <label class="info-title" for="role">
                                    @if(session()->get('language') == 'spanish') Papel @else Role @endif
                                    <span>*</span>
                                </label>
                                <select name="role"  id="role" class="form-control">
                                    <option value="user">
                                        @if(session()->get('language') == 'spanish') Usuario @else User @endif
                                    </option>
                                </select>
                                <strong class="text-danger">{{ $errors->first('role') }}</strong>
                            </div>
                            <div class="form-group">
                                <label class="info-title" for="password">
                                    @if(session()->get('language') == 'spanish') Contraseña @else Password @endif
                                    <span>*</span>
                                </label>
                                <input type="password" name="password" class="form-control unicase-form-control text-input" id="password" required autocomplete="password">
                                <strong class="text-danger">{{ $errors->first('password') }}</strong>
                            </div>
                            <div class="form-group">
                                <label class="info-title" for="password_confirmation">
                                    @if(session()->get('language') == 'spanish') Confirmar Contraseña @else Confirm Password @endif
                                    <span>*</span>
                                </label>
                                <input type="password" name="password_confirmation" class="form-control unicase-form-control text-input" id="password_confirmation" required autocomplete="password_confirmation">
                                <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                            </div>
                            <button type="submit" class="btn-upper btn btn-primary checkout-page-button">
                                @if(session()->get('language') == 'spanish') Inscribirse @else Sign Up @endif
                            </button>
                        </form>
                    </div>
                    <!-- create a new account -->
                </div><!-- /.row -->
            </div><!-- /.sign-in-->
        </div><!-- /.container -->
    </div><!-- /.body-content -->
@endsection

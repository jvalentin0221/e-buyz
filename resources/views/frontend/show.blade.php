@extends('layouts.frontend.master')
@section('title', "Product Details")
@section('content')
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row single-product'>
                <div class='col-md-3 sidebar'>
                    <div class="sidebar-module-container">
                        <!-- ============================================== HOT DEALS ============================================== -->
                        @include('frontend.partials.hot_deals')
                        <!-- ============================================== HOT DEALS: END ============================================== -->
                    </div>
                </div><!-- /.sidebar -->
                <div class='col-md-9'>
                    <div class="detail-block">
                        <div class="row  wow fadeInUp">
                            <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
                                <div class="product-item-holder size-big single-product-gallery small-gallery">
                                    <div id="owl-single-product">
                                        @foreach($product->multiImgs as $img)
                                        <div class="single-product-gallery-item" id="slide{{ $img->id }}">
                                            <a  data-title="Gallery"
                                                @if(file_exists('uploads/multi_img/' . $product->id ."/" . $img->photo_name))
                                                href="{{ asset('uploads/multi_img/' . $product->id ."/" . $img->photo_name) }}">
                                                @else
                                                href="{{ Storage::disk('s3')->url('multi_img/' . $product->id ."/" . $img->photo_name) }}">
                                                @endif
                                                @if(!empty($img->photo_name))
                                                    @if(file_exists('uploads/multi_img/' . $product->id ."/" . $img->photo_name))
                                                        <img src="{{ asset('uploads/multi_img/' . $product->id ."/" . $img->photo_name) }}"
                                                             data-echo="{{ asset('uploads/multi_img/' . $product->id ."/" . $img->photo_name) }}"
                                                             class="img-responsive"
                                                             alt="product image">
                                                    @else
                                                    <img src="{{ Storage::disk('s3')->url('multi_img/' . $product->id ."/" . $img->photo_name) }}"
                                                         data-echo="{{ Storage::disk('s3')->url('multi_img/' . $product->id ."/" . $img->photo_name) }}"
                                                         class="img-responsive"
                                                         alt="product image">
                                                    @endif
                                                @endif
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->
                                        @endforeach
                                    </div><!-- /.single-product-slider -->

                                    <div class="single-product-gallery-thumbs gallery-thumbs">
                                        <div id="owl-single-product-thumbnails">
                                            @foreach($product->multiImgs as $img)
                                            <div class="item">
                                                <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="1" href="#slide{{ $img->id }}">
                                                    @if(!empty($img->photo_name))
                                                        @if(file_exists('uploads/multi_img/' . $product->id ."/" . $img->photo_name))
                                                            <img src="{{ asset('uploads/multi_img/' . $product->id ."/" . $img->photo_name) }}"
                                                                 data-echo="{{ asset('uploads/multi_img/' . $product->id ."/" . $img->photo_name) }}"
                                                                 class="img-responsive"
                                                                 alt="product image">
                                                        @else
                                                            <img src="{{ Storage::disk('s3')->url('multi_img/' . $product->id ."/" . $img->photo_name) }}"
                                                             data-echo="{{ Storage::disk('s3')->url('multi_img/' . $product->id ."/" . $img->photo_name) }}"
                                                             class="img-responsive"
                                                             alt="product image">
                                                        @endif
                                                    @endif
                                                </a>
                                            </div>
                                            @endforeach
                                        </div><!-- /#owl-single-product-thumbnails -->
                                    </div><!-- /.gallery-thumbs -->
                                </div><!-- /.single-product-gallery -->
                            </div><!-- /.gallery-holder -->

                            <div class='col-sm-6 col-md-7 product-info-block'>
                                <div class="product-info">
                                    <h1 class="name">
                                        @if(session()->get('language') == 'spanish') {{ $product->product_name_es }} @else {{ $product->product_name_es }} @endif
                                    </h1>

                                    <div class="rating-reviews m-t-20">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                @if($avgReviews == 0)
                                                    @if(session()->get('language') == 'spanish') sin calificación todavía @else No rating yet @endif
                                                @elseif($avgReviews == 1 || $avgReviews < 2)
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                @elseif($avgReviews == 2 || $avgReviews < 3)
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                @elseif($avgReviews == 3 || $avgReviews < 4)
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                @elseif($avgReviews == 4 || $avgReviews < 5)
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                @elseif($avgReviews == 5 || $avgReviews < 5)
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                @endif
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="reviews">
                                                    <a href="#" class="lnk">({{ count($relatedReviews) }}
                                                        @if(session()->get('language') == 'spanish') Críticas @else Reviews )@endif </a>
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.rating-reviews -->

                                    <div class="stock-container info-container m-t-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="stock-box">
                                                    <span class="label">
                                                         @if(session()->get('language') == 'spanish') Disponibilidad: @else Availibility: @endif
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="stock-box">
                                                    @if($product->product_qty <= 10)
                                                        <span class="text-danger">
                                                            @if(session()->get('language') == 'spanish')
                                                                Solamente {{ $product->product_qty }} quedaban en almacenaje
                                                            @else Only {{ $product->product_qty }} left in stock
                                                            @endif
                                                        </span>
                                                    @else
                                                        <span class="text-success">
                                                            @if(session()->get('language') == 'spanish') En stock @else In Stock @endif
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.stock-container -->

                                    <div class="description-container m-t-20">
                                        @if(session()->get('language') == 'spanish')
                                            {{ $product->productExtension->short_description_es }}
                                        @else
                                            {{ $product->productExtension->short_description_en }}
                                        @endif
                                    </div><!-- /.description-container -->

                                    <div class="price-container info-container m-t-20">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="price-box">
                                                    @if ( $product->discount_percentage == 0)
                                                        <span class="price">${{ $product->selling_price }}</span>
                                                    @else
                                                        <span class="price">${{ $product->discount_price }}</span>
                                                        <span class="price-strike">${{ $product->selling_price }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.price-container -->

                                    <!-- Add Color And Size -->
                                <form method="POST" action="{{ route('cart.add_to_cart') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                @if($product->product_color_en == null && $product->product_color_es == null)
                                                @else
                                                @if(session()->get('language') == 'spanish')
                                                <label class="info-title control-label">
                                                    elegir color
                                                </label>
                                                <select name="product_colors" class="form-control unicase-form-control selectpicker" style="display: none;">
                                                    @foreach(explode(',', $product->product_color_es) as $color)
                                                        <option name="product_colors">{{ ucwords($color) }}</option>
                                                    @endforeach
                                                </select>
                                                @else
                                                    <label class="info-title control-label">
                                                        Choose Color
                                                    </label>
                                                    <select name="product_colors" class="form-control unicase-form-control selectpicker" style="display: none;">
                                                        @foreach(explode(',', $product->product_color_en) as $color)
                                                            <option name="product_colors">{{ ucwords($color) }}</option>
                                                        @endforeach
                                                    </select>
                                                @endif
                                                @endif
                                            </div> <!-- // end form group -->
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                @if($product->product_size_en == null && $product->product_size_es == null)
                                                @else
                                                    @if(session()->get('language') == 'spanish')
                                                    <label class="info-title control-label">
                                                        Elegir Talla
                                                    </label>
                                                    <select name="product_sizes" class="form-control unicase-form-control selectpicker" style="display: none;">
                                                        @foreach(explode(',', $product->product_size_es) as $size)
                                                            <option name="product_sizes">
                                                                {{ ucwords($size) }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @else
                                                        <label class="info-title control-label">Choose Size <span> </span></label>
                                                        <select name="product_sizes" class="form-control unicase-form-control selectpicker" style="display: none;">
                                                            @foreach(explode(',', $product->product_size_en) as $size)
                                                                <option name="product_sizes">
                                                                    {{ ucwords($size) }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div><!-- /.row -->
                                    <!--END Add Color And Size -->

                                    <div class="quantity-container info-container">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <span class="label">Qty :</span>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="cart-quantity">
                                                    <div class="quant-input">
                                                        <input min="1" max="{{ $product->product_qty }}" name="requested_quantity" value="1" type="number">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <ul class="list-unstyled">
                                                    <li class="add-cart-button btn-group">
                                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                                        <input type="hidden" name="quantity" value="{{ $product->product_qty }}">
                                                        <input type="hidden" name="product_color_en" value="{{ $product->product_color_en }}">
                                                        <input type="hidden" name="product_color_es" value="{{ $product->product_color_es }}">
                                                        <input type="hidden" name="product_size_en" value="{{ $product->product_size_en }}">
                                                        <input type="hidden" name="product_size_es" value="{{ $product->product_size_es }}">
                                                        <input type="hidden" name="product_thumbnail" value="{{ $product->product_thumbnail }}">
                                                        <input type="hidden" name="product_slug_en" value="{{$product->product_slug_en }}">
                                                        <input type="hidden" name="product_name_en" value="{{ $product->product_name_en }}">
                                                        <input type="hidden" name="product_name_es" value="{{ $product->product_name_es }}">
                                                        <input type="hidden" name="product_code" value="{{ $product->productExtension->product_code }}">
                                                        <input type="hidden" name="product_price" value="{{ $product->discount_price }}">
                                                        @if($product->product_qty > 0)
                                                        <button data-toggle="tooltip" class="btn btn-primary icon" type="submit" title="Add Cart">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </button>
                                                        @else
                                                        @endif
                                                    </li>
                                                </ul>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.quantity-container -->
                                </form>
                                </div><!-- /.product-info -->
                            </div><!-- /.col-sm-7 -->
                        </div><!-- /.row -->
                    </div>

                    <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                        <div class="row">
                            <div class="col-sm-3">
                                <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                    <li class="active">
                                        <a data-toggle="tab" href="#description">
                                            @if(session()->get('language') == 'spanish') Descripción @else Description @endif
                                        </a>
                                    </li>
                                    <li><a data-toggle="tab" href="#review">
                                            @if(session()->get('language') == 'spanish') Revisar @else Review @endif
                                        </a>
                                    </li>
                                </ul><!-- /.nav-tabs #product-tabs -->
                            </div>
                            <div class="col-sm-9">
                                <div class="tab-content">
                                    <div id="description" class="tab-pane in active">
                                        <div class="product-tab">
                                            <p class="text">
                                                @if(session()->get('language') == 'spanish')
                                                    {{ strip_tags($product->productExtension->long_description_es) }}
                                                @else
                                                    {{ strip_tags($product->productExtension->long_description_en) }}
                                                @endif
                                            </p>
                                        </div>
                                    </div><!-- /.tab-pane -->

                                    <div id="review" class="tab-pane">
                                        <div class="product-tab">
                                            <div class="product-reviews">
                                                <h4 class="title">

                                                </h4>
                                                <div class="reviews">
                                                @foreach($reviews as $review)
                                                    @if($review->status == 0)
                                                    @else
                                                    <div class="review">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                @if(!empty($review->user_avatar))
                                                                    @if(file_exists('uploads/avatar/' . $review->user_id ."/" . $review->user_avatar))
                                                                        <img class="card-img-top"
                                                                             style="border-radius: 50%"
                                                                             height="60px"
                                                                             width="60px"
                                                                             src="{{ asset('uploads/avatar/' . $review->user_id ."/" . $review->user_avatar) }}"
                                                                             alt="user profile image">
                                                                        <strong>{{ $review->user->name }}</strong>
                                                                    @else
                                                                        <img class="card-img-top rounded-circle"
                                                                             src="{{ Storage::disk('s3')->url('avatar/' . $review->user_id ."/" . $review->user_avatar)  }}"
                                                                             style="border-radius: 50%"
                                                                             height="60px"
                                                                             width="60px"
                                                                             alt="user profile image">
                                                                        <strong>{{ $review->user->name }}</strong>
                                                                    @endif
                                                                @else
                                                                    <img class="card-img-top rounded-circle"
                                                                         src="{{ asset('assets/images/user_placeholder.png') }}"
                                                                         style="border-radius: 50%"
                                                                         height="60px"
                                                                         width="60px"
                                                                         alt="user profile image">
                                                                    <strong>{{ $review->user->name }}</strong>
                                                                    @if($review->rating == NULL)

                                                                    @elseif($review->rating == 1)
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star"></span>
                                                                        <span class="fa fa-star"></span>
                                                                        <span class="fa fa-star"></span>
                                                                        <span class="fa fa-star"></span>
                                                                    @elseif($review->rating == 2)
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star"></span>
                                                                        <span class="fa fa-star"></span>
                                                                        <span class="fa fa-star"></span>

                                                                    @elseif($review->rating == 3)
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star"></span>
                                                                        <span class="fa fa-star"></span>

                                                                    @elseif($review->rating == 4)
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star"></span>
                                                                    @elseif($review->rating == 5)
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                            <div class="col-md-6">

                                                            </div>
                                                        </div>
                                                        <div class="review-title">
                                                            <span class="summary">
                                                                {{ $review->summary }}
                                                            </span>
                                                            <span class="date">
                                                                <i class="fa fa-calendar"></i>
                                                            <span>
                                                                {{ $review->created_at->diffForHumans() }}
                                                            </span>
                                                            </span>
                                                        </div>
                                                        <div class="text">{{ $review->comment }}</div>
                                                    </div>
                                                    @endif
                                                @endforeach
                                                </div><!-- /.reviews -->
                                            </div><!-- /.product-reviews -->

                                            <div class="product-add-review">
                                                @auth
                                                    @if(Auth::user()->role == 'admin')
                                                        <h5>An admin cannot write reviews for products.</h5>
                                                    @else
                                                        <h4 class="title">
                                                            @if(session()->get('language') == 'spanish') Escribe tu propia reseña @else Write your Own Review @endif
                                                        </h4>
                                                        <div class="review-form">
                                                            <div class="form-container">
                                                                <form method="POST" class="cnt-form" action="{{ route('review.store', $product->product_name_en) }}">
                                                                    @csrf
                                                                    <table class="table">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="cell-label">&nbsp;</th>
                                                                            <th>1 @if(session()->get('language') == 'spanish') estrella @else star @endif</th>
                                                                            <th>2 @if(session()->get('language') == 'spanish') estrellas @else stars @endif</th>
                                                                            <th>3 @if(session()->get('language') == 'spanish') estrellas @else stars @endif</th>
                                                                            <th>4 @if(session()->get('language') == 'spanish') estrellas @else stars @endif</th>
                                                                            <th>5 @if(session()->get('language') == 'spanish') estrellas @else stars @endif</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="cell-label">Quality</td>
                                                                            <td><input type="radio" name="rating" class="radio" value="1"></td>
                                                                            <td><input type="radio" name="rating" class="radio" value="2"></td>
                                                                            <td><input type="radio" name="rating" class="radio" value="3"></td>
                                                                            <td><input type="radio" name="rating" class="radio" value="4"></td>
                                                                            <td><input type="radio" name="rating" class="radio" value="5"></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <strong class="text-danger">{{ $errors->first('rating') }}</strong>
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="summary">
                                                                                    @if(session()->get('language') == 'spanish') Resumen @else Summary @endif
                                                                                </label>
                                                                                <input name="summary" type="text" class="form-control txt" id="summary">
                                                                            </div>
                                                                            <strong class="text-danger">{{ $errors->first('summary') }}</strong>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="comment">
                                                                                    @if(session()->get('language') == 'spanish') Revisar @else Review @endif
                                                                                </label>
                                                                                <textarea name="comment" class="form-control txt txt-review" id="comment" rows="4"></textarea>
                                                                            </div>
                                                                            <strong class="text-danger">{{ $errors->first('comment') }}</strong>
                                                                        </div>
                                                                    </div><!-- /.row -->

                                                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                                                    <input type="hidden" name="product_slug_en" value="{{ $product->product_slug_en }}">
                                                                    <input type="hidden" name="user_avatar" value='{{ Auth::user()->avatar ?? NULL }}'>
                                                                    <div class="action text-right">
                                                                        <button type="submit" class="btn btn-primary">
                                                                            @if(session()->get('language') == 'spanish') Enviar opinión @else Submit Review @endif
                                                                        </button>
                                                                    </div>
                                                                </form><!-- /.cnt-form -->
                                                            </div><!-- /.form-container -->
                                                        </div><!-- /.review-form -->
                                                    @endif
                                                @else
                                                    <h5>Please sign in to write a review for this product.</h5>
                                                @endauth
                                            </div><!-- /.product-add-review -->
                                        </div><!-- /.product-tab -->
                                    </div><!-- /.tab-pane -->
                                </div><!-- /.tab-content -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.product-tabs -->

                    <!-- ============================================== RELATED PRODUCTS ============================================== -->
                    <section class="section featured-product wow fadeInUp">
                        <h3 class="section-title">
                            @if(session()->get('language') == 'spanish') Productos relacionados @else Related Products @endif
                        </h3>
                        <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">
                            @foreach($relatedProducts as $relatedProduct)
                            <div class="item item-carousel">
                                <div class="products">
                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="{{ route('index.show', $relatedProduct->product_slug_en) }}">
                                                    @if(!empty($relatedProduct->product_thumbnail))
                                                        @if(file_exists('uploads/product_thumbnail/' . $relatedProduct->id ."/" . $relatedProduct->product_thumbnail))
                                                            <img src="{{ asset('uploads/product_thumbnail/' . $relatedProduct->id ."/" . $relatedProduct->product_thumbnail) }}"
                                                                 alt="product image"
                                                        @else
                                                            <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $relatedProduct->id ."/" . $relatedProduct->product_thumbnail)}}"
                                                                 alt="product image">
                                                        @endif
                                                    </a>
                                                </div><!-- /.image -->
                                                <div>
                                                    @if ($relatedProduct->discount_percentage == 0)
                                                        <div class="tag new"><span>new</span></div>
                                                    @else
                                                        <div class="tag hot"><span>{{ $relatedProduct->discount_percentage }}%</span></div>
                                                    @endif
                                                </div>
                                            </div><!-- /.product-image -->

                                            <div class="product-info text-left">
                                                <h3 class="name">
                                                    <a href="{{ route('index.show', $relatedProduct->product_slug_en) }}">
                                                        @if(session()->get('language') == 'spanish') {{ $relatedProduct->product_name_es }} @else {{ $relatedProduct->product_name_en  }} @endif
                                                    </a>
                                                </h3>
                                                @include('frontend.partials.reviews')
                                                <div class="description">
                                                    @if(session()->get('language') == 'spanish')
                                                        {{ strip_tags($product->productExtension->short_description_es ) }}
                                                    @else
                                                        {{ strip_tags($product->productExtension->short_description_en )  }}
                                                    @endif
                                                </div>
                                                <div class="product-price">
                                                    @if ($relatedProduct->discount_percentage == 0)
                                                        <span class="price">${{ $relatedProduct->selling_price }}</span>
                                                    @else
                                                        <span class="price">${{ $relatedProduct->discount_price}}</span>
                                                        <span class="price-before-discount">${{ $relatedProduct->selling_price }}</span>
                                                    @endif
                                                </div><!-- /.product-price -->
                                            </div><!-- /.product-info -->
                                            @endif
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    @include('frontend.partials.add_to_cart')
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->
                                </div><!-- /.products -->
                            </div><!-- /.item -->
                            @endforeach
                        </div>
                    </section><!-- /.section -->
                    <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
                </div><!-- /.col -->
                <div class="clearfix"></div>
            </div><!-- /.row -->
        </div>
        @include('frontend.body.brands')
    </div>
@endsection

@extends('layouts.frontend.master')
@section('title', "Welcome to E-Buyz")
@section('content')
<div class="body-content outer-top-xs" id="top-banner-and-menu">
    <div class="container">
        <div class="row">
            <!-- ============================================== SIDEBAR ============================================== -->
            <div class="col-xs-12 col-sm-12 col-md-3 sidebar">
                <!-- ================================== TOP NAVIGATION ================================== -->
                @include('frontend.partials.vertical_column')
                @include('frontend.partials.hot_deals')
{{--                <!-- ============================================= SPECIAL OFFERS ============================================== -->--}}
                @include('frontend.partials.special_offers')
{{--                <!-- ============================================== PRODUCT TAGS ============================================== -->--}}
                @include('frontend.partials.product_tags')
            </div>
        <!-- ============================================== CONTENT ============================================== -->
            <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
                <!-- ========================================== SECTION – HERO ========================================= -->
                <div id="hero">
                    <h1></h1>
                    <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                    @if(cache('sliderCompanyInfo')['sliders'])
                    @foreach(cache('sliderCompanyInfo')['sliders'] as $slider)
                        <div class="item"
                             style="background-image:
                             @if(!empty($slider->slider_img))
                                @if(file_exists('uploads/slider_img/' . $slider->id ."/" . $slider->slider_img))
                                    url({{ asset('uploads/slider_img/'. $slider->id . '/' . $slider->slider_img) }});">
                                @else
                                    {{ Storage::disk('s3')->url('slider_img/' . $slider->slider_img ."/" . $slider->slider_img)}}"
                                @endif
                            @endif
                            <div class="container-fluid">
                                <div class="caption bg-color vertical-center text-left">
                                    <div class="big-text fadeInDown-1"> {{  $slider->title }} </div>
                                    <div class="excerpt fadeInDown-2 hidden-xs"> <span>{{ $slider->description }}</span> </div>
                                </div><!-- /.caption -->
                            </div><!-- /.container-fluid -->
                        </div><!-- /.item -->
                    @endforeach
                    @endif
                    </div><!-- /.owl-carousel -->
                </div>
                <!-- ========================================= SECTION – HERO : END ========================================= -->

                <!-- ============================================== INFO BOXES ============================================== -->
                <div class="info-boxes wow fadeInUp">
                    <div class="info-boxes-inner">
                        <div class="row">
                            <div class="col-md-6 col-sm-4 col-lg-4">
                                <div class="info-box">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4 class="info-box-heading green">@if(session()->get('language') == 'spanish') Devolución de dinero @else money back @endif</h4>
                                        </div>
                                    </div>
                                    <h6 class="text">@if(session()->get('language') == 'spanish')30 días de garantía de devolución de dinero @else 30 Days Money Back Guarantee @endif</h6>
                                </div>
                            </div>

                            <div class="hidden-md col-sm-4 col-lg-4">
                                <div class="info-box">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4 class="info-box-heading green">@if(session()->get('language') == 'spanish')Envío gratis @else Free Shipping @endif</h4>
                                        </div>
                                    </div>
                                    <h6 class="text">@if(session()->get('language') == 'spanish')Envío en pedidos superiores a $ 99 @else Shipping on orders over $99 @endif</h6>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-4 col-lg-4">
                                <div class="info-box">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4 class="info-box-heading green">@if(session()->get('language') == 'spanish')Venta especial @else Special sale @endif</h4>
                                        </div>
                                    </div>
                                    <h6 class="text">@if(session()->get('language') == 'spanish')$ 5 de descuento adicional en todos los artículos @else Extra $5 off on all items @endif</h6>
                                </div>
                            </div><!-- .col -->
                        </div><!-- /.row -->
                    </div><!-- /.info-boxes-inner -->
                </div>
                <!-- /.info-boxes -->
                <!-- ============================================== INFO BOXES : END ============================================== -->

                <!-- ============================================== CATEGORY PRODUCTS ============================================== -->
                <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
                    <div class="more-info-tab clearfix">
                        <h3 class="new-product-title pull-left">
                            @if(session()->get('language') == 'spanish') todos los productos @else Category Products @endif
                        </h3>
                        <ul class="nav nav-tabs nav-tab-line pull-right" id="new-products-1">
                            <li class="active"><a data-transition-type="backSlide" href="#all" data-toggle="tab">All</a></li>
                        @if(cache('categoryLevels')['categories'])
                        @foreach(cache('categoryLevels')['categories'] as $category)
                            <li>
                                <a data-transition-type="backSlide" href="#category{{ $category->id }}" data-toggle="tab">
                                    @if(session()->get('language') == 'spanish') {{ $category->category_name_es }} @else {{ $category->category_name_en }} @endif
                                </a>
                            </li>
                        @endforeach
                        @endif
                        </ul><!-- /.nav-tabs -->
                    </div>
                    <div class="tab-content outer-top-xs">
                        <div class="tab-pane in active" id="all">
                            <div class="product-slider">
                                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
                                @foreach(cache('categoryProducts')['products'] as $product)
                                    <div class="item item-carousel">
                                        <div class="products">
                                            <div class="product">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="{{ route('index.show', $product->product_slug_en) }}">
                                                        @if(!empty($product->product_thumbnail))
                                                            @if(file_exists('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail))
                                                                <img src="{{ asset('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail) }}"
                                                                     alt="product image"
                                                                     class="img-fluid">
                                                            @else
                                                                <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $product->id ."/" . $product->product_thumbnail)}}"
                                                                     alt="product image">
                                                            @endif
                                                        </a>
                                                    </div>
                                                    <!-- /.image -->
                                                    <div>
                                                        @if ($product->discount_percentage == 0)
                                                            <div class="tag new"><span>new</span></div>
                                                        @else
                                                            <div class="tag hot"><span>{{ $product->discount_percentage }}%<br></span></div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- /.product-image -->

                                                <div class="product-info text-left">
                                                    <h3 class="name">
                                                        <a href="{{ route('index.show', $product->product_slug_en) }}">
                                                            @if(session()->get('language') == 'spanish') {{ $product->product_name_es }} @else {{ $product->product_name_en  }} @endif
                                                        </a>
                                                    </h3>
                                                    @include('frontend.partials.reviews')
                                                    <div class="product-price">
                                                        @if ($product->discount_percentage == 0)
                                                        <span class="price">
                                                            ${{ $product->selling_price }}
                                                        </span>
                                                        @else
                                                        <span class="price">${{ $product->discount_price}}</span>
                                                        <span class="price-before-discount">${{ $product->selling_price }}</span>
                                                        @endif
                                                    </div><!-- /.product-price -->
                                                </div><!-- /.product-info -->
                                            @endif
                                                <div class="cart clearfix animate-effect">
                                                    <div class="action">
                                                   @include('frontend.partials.add_to_cart')
                                                    </div><!-- /.action -->
                                                </div><!-- /.cart -->
                                            </div><!-- /.product -->
                                        </div><!-- /.products -->
                                    </div><!-- /.item -->
                                @endforeach
                                </div><!-- /.home-owl-carousel -->
                            </div><!-- /.product-slider -->
                        </div><!-- /.tab-pane -->
                    @if(cache('categoryLevels')['categories'])
                    @foreach(cache('categoryLevels')['categories'] as $category)
                        <div class="tab-pane" id="category{{ $category->id }}">
                            <div class="product-slider">
                                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
                                @foreach($category->products as $product)
                                    @if($product->status == 1)
                                    <div class="item item-carousel">
                                        <div class="products">
                                            <div class="product">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="{{ route('index.show', $product->product_slug_en) }}">
                                                        @if(!empty($product->product_thumbnail))
                                                            @if(file_exists('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail))
                                                                <img src="{{ asset('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail) }}"
                                                                     alt="product image"
                                                            @else
                                                                <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $product->id ."/" . $product->product_thumbnail)}}"
                                                                     alt="product image">
                                                            @endif
                                                        </a>
                                                    </div><!-- /.image -->
                                                    <div>
                                                        @if ($product->discount_percentage == 0)
                                                            <div class="tag new"><span>new'</span></div>
                                                        @else
                                                            <div class="tag hot"><span>{{ $product->discount_percentage }}%</span></div>
                                                        @endif
                                                    </div>
                                                </div><!-- /.product-image -->

                                                <div class="product-info text-left">
                                                    <h3 class="name">
                                                        <a href="{{ route('index.show', $product->product_slug_en) }}">
                                                            @if(session()->get('language') == 'spanish') {{ $product->product_name_es }} @else {{ $product->product_name_en  }} @endif
                                                        </a>
                                                    </h3>
                                                    @include('frontend.partials.reviews')
                                                    <div class="description"></div>
                                                    <div class="product-price">
                                                        @if ($product->discount_percentage == 0)
                                                        <span class="price">${{ $product->selling_price }}</span>
                                                        @else
                                                            <span class="price">${{ $product->discount_price}}</span>
                                                            <span class="price-before-discount">${{ $product->selling_price }}</span>
                                                        @endif
                                                    </div><!-- /.product-price -->
                                                </div><!-- /.product-info -->
                                                @endif
                                            </div><!-- /.product -->
                                        </div><!-- /.products -->
                                    </div><!-- /.item -->
                                    @endif
                                    @endforeach
                                </div><!-- /.home-owl-carousel -->
                            </div><!-- /.product-slider -->
                        </div><!-- /.tab-pane -->
                    @endforeach
                    @endif
                    </div><!-- /.tab-content -->

                </div><!-- /.scroll-tabs -->
                <!-- ============================================== SCROLL TABS : END ============================================== -->

                <!-- ============================================== BANNER ============================================== -->
                <div class="wide-banners wow fadeInUp outer-bottom-xs">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="wide-banner cnt-strip">
                                <div class="image"> <img class="img-responsive" src="{{ asset('assets/images/women_clothing_sale.jpg')}}" alt=""> </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- ============================================== NEW PRODUCTS ============================================== -->
                @include('frontend.partials.new')
                <!-- ============================================== SECTION IMAGE ============================================== -->
                <div class="wide-banners wow fadeInUp outer-bottom-xs">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="wide-banner cnt-strip">
                                <div class="image"> <img class="img-responsive" src="{{ Storage::disk('s3')->url('images/banners/men_fashion_banner.jpg')}}" alt="men fashion banner"> </div>
                                <div class="strip strip-text">
                                    <div class="strip-inner">
                                        <h2 class="text-right">New Mens Fashion<br>
                                            <span class="shopping-needs">Save up to 40% off</span></h2>
                                    </div>
                                </div>
                                <div class="new-label">
                                    <div class="text">NEW</div>
                                </div><!-- /.new-label -->
                            </div><!-- /.wide-banner -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.wide-banners -->
                <!-- ============================================== BEST SELLER ============================================== -->
                <div class="wide-banners wow fadeInUp outer-bottom-xs">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="wide-banner cnt-strip">
                                @include('frontend.partials.best_seller')
                            </div><!-- /.wide-banner -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.wide-banners -->
            </div><!-- /.homebanner-holder -->
        </div><!-- /.row -->
        <!-- ============================================== BRANDS CAROUSEL ============================================== -->
       @include('frontend.body.brands')
    </div><!-- /.container -->
</div>
@endsection

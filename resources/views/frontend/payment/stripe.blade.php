@extends('layouts.frontend.master')
@section('title', "Stripe Payment")
@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Home</a></li>
                    <li><a href="{{ route('cart.index') }}">Cart</a></li>
                    <li><a href="{{ route('checkout.index') }}">Checkout</a></li>
                    <li class='active'>Stripe</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
            <div class="checkout-box">
                <div class="row">
                    <div class="col-md-6">
                        <!-- checkout-progress-sidebar -->
                        <div class="checkout-progress-sidebar ">
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="unicase-checkout-title">
                                            @if(session()->get('language') == 'spanish') Tus cosas IMPRESIONANTES @else Your AWESOME items @endif
                                        </h4>
                                    </div>
                                    <div class="">
                                        <ul class="nav nav-checkout-progress list-unstyled">
                                            <li>
                                                @if(Session::has('discount' . Auth::user()->id))

                                                    <strong>
                                                        @if(session()->get('language') == 'spanish')Total parcial @else SubTotal: @endif
                                                    </strong> ${{ $subTotal }} <hr>

                                                    <strong>
                                                        @if(session()->get('language') == 'spanish') Nombre del cupón @else Coupon Name: @endif
                                                    </strong> {{ session()->get('coupon_name' . Auth::user()->id)   }}
                                                    ( {{ session()->get('discount' . Auth::user()->id) }} % )
                                                    <hr>

                                                    <strong>
                                                        @if(session()->get('language') == 'spanish') Cupón de descuento @else Coupon Discount: @endif
                                                    </strong> ${{ $subTotal * session()->get('discount' . Auth::user()->id) / 100 }}
                                                    <hr>

                                                    <strong>
                                                        @if(session()->get('language') == 'spanish') Gran total @else Grand Total: @endif
                                                    </strong> ${{ $subTotal - $subTotal * session()->get('discount' . Auth::user()->id) / 100 }}
                                                    <hr>
                                                @else
                                                    <strong>
                                                        @if(session()->get('language') == 'spanish')Total parcial @else SubTotal: @endif
                                                    </strong> ${{ $subTotal }} <hr>

                                                    <strong>
                                                        @if(session()->get('language') == 'spanish') Gran total @else Grand Total: @endif
                                                    </strong> ${{ $subTotal }} <hr>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <!-- checkout-progress-sidebar -->
                        <div class="checkout-progress-sidebar ">
                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="unicase-checkout-title">
                                            @if(session()->get('language') == 'spanish') Gran total @else Select Payment Method @endif
                                        </h4>
                                    </div>
                                    <form action="{{ route('stripe.store') }} " method="post" id="payment-form">
                                        @csrf
                                        <div class="form-row">
                                            <label for="card-element">
                                                @if(session()->get('language') == 'spanish') Tarjeta de crédito o débito @else Credit card or debit @endif
                                            </label>
                                            <div id="card-element">
                                                <!-- A Stripe Element will be inserted here. -->
                                            </div>
                                            <!-- Used to display form errors. -->
                                            <div id="card-errors" role="alert"></div>
                                        </div>
                                        <br>
                                        <button class="btn btn-primary">
                                            @if(session()->get('language') == 'spanish') Enviar pago @else Submit Payment @endif
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.checkout-box -->
            <br><br><br><br>
        </div><!-- container-->
    </div><!-- body-content-->
    <br><br><br><br>
@endsection

@section('body-scripts')
    <script src="{{ asset('js/stripe.js') }}"></script>
@endsection

{{--<h1>{{  session('checkout1.shipping_name')}}</h1>--}}

@extends('layouts.frontend.master')
@section('title', "Checkout")
@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Home</a></li>
                    <li><a href="{{ route('cart.index') }}">Cart</a></li>
                    <li class='active'>Checkout</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
            <div class="checkout-box">
                <div class="row">
                    <form class="register-form" action="{{ route('checkout.store') }}" method="POST">
                        @csrf
                        <div class="col-md-8">
                            <div class="panel-group checkout-steps" id="accordion">
                                <!-- checkout-step-01  -->
                                <div class="panel panel-default checkout-step-01">
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <!-- panel-body  -->
                                        <div class="panel-body">
                                            <div class="row">
                                                <!-- guest-login -->
                                                <div class="col-md-6 col-sm-6 already-registered-login">
                                                    <h3 class="checkout-subtitle">
                                                        <strong>
                                                            @if(session()->get('language') == 'spanish') Dirección de Envío @else Shipping Address @endif
                                                        </strong>
                                                    </h3>

                                                    <div class="form-group">
                                                        <label class="info-title" for="shipping_name">
                                                            @if(session()->get('language') == 'spanish') Nombre de envío @else Shipping Name @endif
                                                        </label>
                                                        <input
                                                            name="shipping_name"
                                                            type="text"
                                                            class="form-control unicase-form-control text-input"
                                                            id="shipping_name"
                                                            required
                                                            value="{{ Auth::user()->name }}"
                                                            placeholder="Ebuyz Cartz...">
                                                        <strong class="text-danger">{{ $errors->first('shipping_name') }}</strong>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="info-title" for="shipping_email">
                                                            @if(session()->get('language') == 'spanish') Dirección de correo electrónico @else Email Address @endif
                                                        </label>
                                                        <input
                                                            name="shipping_email"
                                                            type="email"
                                                            class="form-control unicase-form-control text-input"
                                                            id="shipping_email"
                                                            value="{{ Auth::user()->email }}"
                                                            required
                                                            placeholder="ebuyz@gmail.com...">
                                                        <strong class="text-danger">{{ $errors->first('shipping_email') }}</strong>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="info-title" for="shipping_phone">
                                                            @if(session()->get('language') == 'spanish') Número de teléfono @else Phone Number @endif
                                                        </label>
                                                        <input
                                                            name="shipping_phone"
                                                            type="number"
                                                            class="form-control unicase-form-control text-input"
                                                            id="shipping_phone"
                                                            value="{{ Auth::user()->phone_number ?? old('shipping_phone') }}"
                                                            placeholder="4073336666...">
                                                        <strong class="text-danger">{{ $errors->first('shipping_phone') }}</strong>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="info-title" for="shipping_address">
                                                            @if(session()->get('language') == 'spanish') Dirección @else Address @endif
                                                        </label>
                                                        <input
                                                            name="shipping_address"
                                                            type="text"
                                                            class="form-control unicase-form-control text-input"
                                                            id="shipping_address"
                                                            value="{{ Auth::user()->address ?? old('shipping_address')  }}"
                                                            placeholder="123 Ebuyz St. ...">
                                                        <strong class="text-danger">{{ $errors->first('shipping_address') }}</strong>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="info-title" for="zip_code">
                                                            @if(session()->get('language') == 'spanish') Código postal @else Zip Code @endif
                                                        </label>
                                                        <input
                                                            name="zip_code"
                                                            required
                                                            type="number"
                                                            class="form-control unicase-form-control text-input"
                                                            id="zip_code"
                                                            value="{{ Auth::user()->zip_code ?? old('zip_code')  }}"
                                                            placeholder="32822...">
                                                        <strong class="text-danger">{{ $errors->first('zip_code') }}</strong>
                                                    </div>
                                                </div><!-- already-registered-login -->
                                                <!-- guest-login -->

                                                <!-- already-registered-login -->
                                                <div class="col-md-6 col-sm-6 already-registered-login">
                                                    <br><br><br>
                                                    <div class="form-group">
                                                        <label class="info-title" for="state">
                                                            @if(session()->get('language') == 'spanish') Estado @else State @endif
                                                        </label>
                                                        <input
                                                            name="state"
                                                            type="text"
                                                            class="form-control unicase-form-control text-input"
                                                            id="state"
                                                            value="{{ Auth::user()->state ?? old('state') }}"
                                                            placeholder="FL..."
                                                            required>
                                                        <strong class="text-danger">{{ $errors->first('state') }}</strong>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="info-title" for="city">
                                                            @if(session()->get('language') == 'spanish') Ciudad @else City @endif
                                                        </label>
                                                        <input
                                                            name="city"
                                                            type="text"
                                                            class="form-control unicase-form-control text-input"
                                                            id="city"
                                                            value="{{ Auth::user()->city ?? old('city') }}"
                                                            required
                                                            placeholder="Orlando...">
                                                        <strong class="text-danger">{{ $errors->first('city') }}</strong>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="info-title" for="notes">
                                                            @if(session()->get('language') == 'spanish') Notas @else Notes @endif
                                                        </label>
                                                        <textarea id="notes" class="form-control" cols="30" rows="5" placeholder="Notes" name="notes">
                                                            {{ old('notes') }}
                                                        </textarea>
                                                        <strong class="text-danger">{{ $errors->first('notes') }}</strong>
                                                    </div>  <!-- // end form group  -->
                                                </div><!-- already-registered-login -->
                                            </div>
                                        </div><!-- panel-body  -->
                                    </div><!-- row -->
                                </div><!-- checkout-step-01  -->
                            </div><!-- /.checkout-steps -->
                        </div>
                        <div class="col-md-4">
                            <!-- checkout-progress-sidebar -->
                            <div class="checkout-progress-sidebar ">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="unicase-checkout-title">
                                                @if(session()->get('language') == 'spanish') Tu progreso de pago @else Your Checkout Progress @endif
                                            </h4>
                                        </div>
                                        <div class="">
                                            <ul class="nav nav-checkout-progress list-unstyled">
                                            @foreach($cartItems as $cartItem)
                                                <li>
                                                    <strong>
                                                        @if(session()->get('language') == 'spanish') Imagen @else Image @endif
                                                    </strong>
                                                    @if(!empty($cartItem->product_thumbnail))
                                                        @if(file_exists('uploads/product_thumbnail/' . $cartItem->product_id ."/" . $cartItem->product_thumbnail))
                                                            <img src="{{ asset('uploads/product_thumbnail/' . $cartItem->product_id ."/" . $cartItem->product_thumbnail) }}"
                                                                 alt="{{ $cartItem->product_name_en }}"
                                                                 style="height: 40px; width: 40px;">
                                                        @else
                                                            <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $cartItem->product_id ."/" . $cartItem->product_thumbnail)}}"
                                                                 alt="{{ $cartItem->product_name_en }}"
                                                                 style="height: 40px; width: 40px;">
                                                        @endif
                                                    @endif
                                                </li>

                                                <li>
                                                    <strong>Qty: </strong>
                                                    ( {{ $cartItem->requested_quantity }} )
                                                    @if(empty(session()->get('colorsEN' . $cartItem->id)))
                                                    @else
                                                        <strong>Color: </strong>
                                                        @if(session()->get('language') == 'spanish') {{ $cartItem->product_color_es }} @else {{ $cartItem->product_color_en }}  @endif
                                                    @endif
                                                    @if(empty(session()->get('sizesEN' . $cartItem->id)))
                                                    @else
                                                    <strong>Size: </strong>
                                                        @if(session()->get('language') == 'spanish') {{ $cartItem->product_size_es }} @else {{ $cartItem->product_size_en }}  @endif
                                                    @endif
                                                </li>
                                            @endforeach
                                                <li>
                                                    @if(Session::has('discount' . Auth::user()->id))

                                                        <strong>
                                                            @if(session()->get('language') == 'spanish')Total parcial @else SubTotal: @endif
                                                        </strong> ${{ number_format($subTotal, 2, '.', '')  }} <hr>

                                                        <strong>
                                                            @if(session()->get('language') == 'spanish') Nombre del cupón @else Coupon Name: @endif
                                                        </strong> {{ session()->get('coupon_name' . Auth::user()->id)   }}
                                                        ( {{ session()->get('discount' . Auth::user()->id) }} % )
                                                        <hr>

                                                        <strong>
                                                            @if(session()->get('language') == 'spanish') Cupón de descuento @else Coupon Discount: @endif
                                                        </strong> ${{ number_format($subTotal * session()->get('discount' . Auth::user()->id) / 100 ,2, '.', '') }}
                                                        <hr>

                                                        <strong>
                                                            @if(session()->get('language') == 'spanish') Gran total @else Grand Total: @endif
                                                        </strong> ${{ number_format($subTotal - $subTotal * session()->get('discount' . Auth::user()->id) / 100 ,2, '.', '')  }}
                                                        <hr>
                                                    @else
                                                        <strong>
                                                            @if(session()->get('language') == 'spanish')Total parcial @else SubTotal: @endif
                                                        </strong> ${{ number_format($subTotal, 2, '.', '')  }}<hr>

                                                        <strong>
                                                            @if(session()->get('language') == 'spanish') Gran total @else Grand Total: @endif
                                                        </strong> ${{ number_format($subTotal, 2, '.', '')  }} <hr>
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkout-progress-sidebar ">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="unicase-checkout-title">
                                                @if(session()->get('language') == 'spanish') Seleccionar método de pago @else Select Payment Method @endif
                                            </h4>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Stripe</label>
                                                <input type="radio" name="payment_method" value="stripe">
                                                <img src="{{ asset('assets/images/payment/stripe.png') }}">
                                            </div>

                                            <div class="col-md-4">
                                                <label for="">
                                                    @if(session()->get('language') == 'spanish') Contra reembolso @else Cash On Delivery @endif
                                                </label>
                                                <input type="radio" name="payment_method" value="cash">
                                                <div class="img-fluid">
                                                    <img class="" src="{{ asset('assets/images/payment/cash-on-delivery.png') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('payment_method') }}</strong>
                                        <hr>
                                        <button type="submit" class="btn-upper btn btn-primary checkout-page-button">
                                            @if(session()->get('language') == 'spanish') Hacer el pago @else Make Payment @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.row -->
            </div><!-- /.checkout-box -->
        </div><!-- container-->
        <br><br><br><br>
    </div><!-- body-content-->
@endsection

@extends('layouts.frontend.master')
@section('title', "Search Products")
@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li class='active'>Search Products</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                <div class='col-md-3 sidebar'>
                    <div class="sidebar-module-container">
                        <div class="sidebar-filter">
                            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                            <div class="sidebar-widget wow fadeInUp">
                                <h3 class="section-title">shop by</h3>
                                <div class="widget-header">
                                    <h4 class="widget-title">Category</h4>
                                </div>
                                <div class="sidebar-widget-body">
                                    <div class="accordion">
                                        @foreach(cache('categoryLevels')['categories'] as $category)
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a href="#collapse{{ $category->id }}" data-toggle="collapse" class="accordion-toggle collapsed">
                                                        @if(session()->get('language') == 'spanish') {{ $category->category_name_es }} @else {{ $category->category_name_en }} @endif
                                                    </a>
                                                </div>
                                                <div class="accordion-body collapse" id="collapse{{ $category->id }}" style="height: 0px;">
                                                    <div class="accordion-inner">
                                                        @foreach(cache('categoryLevels')['subcategories'] as $subcategory)
                                                            @if($subcategory->category_id == $category->id)
                                                                <ul>
                                                                    <li>
                                                                        <a href="{{ url('subcategory/'. $subcategory->subcategory_name_en . '/'. $subcategory->subcategory_slug_en ) }}">
                                                                            @if(session()->get('language') == 'spanish') {{ $subcategory->subcategory_name_es }} @else {{ $subcategory->subcategory_name_en }} @endif
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            @endif
                                                        @endforeach
                                                    </div><!-- /.accordion-inner -->
                                                </div><!-- /.accordion-body -->
                                            </div><!-- /.accordion-group -->
                                        @endforeach
                                    </div><!-- /.accordion -->
                                </div><!-- /.sidebar-widget-body -->
                            </div><!-- /.sidebar-widget -->
                            <!-- ============================================== SIDEBAR CATEGORY : END ============================================== -->

                            <!-- ============================================== PRODUCT TAGS ============================================== -->
                        @include('frontend.partials.product_tags')
                        </div><!-- /.sidebar-filter -->
                    </div>
                    <!-- /.sidebar-module-container -->
                </div>
                <!-- /.sidebar -->
                <!-- ===============  SECTION – HERO ===================  -->
                <div class='col-md-9'>
                    <div id="category" class="category-carousel hidden-xs">
                        <div class="item">
                            <div class="image">
                                <img src="{{ asset('assets/images/half_off_sale.jpg') }}" alt="" class="img-responsive">
                            </div>
                            <div class="container-fluid">
                                <div class="caption vertical-top text-left">
{{--                                    <div class="big-text"> Big Sale </div>--}}
{{--                                    <div class="excerpt hidden-sm hidden-md"> Save up to 49% off </div>--}}
{{--                                    <div class="excerpt-normal hidden-sm hidden-md"> Lorem ipsum dolor sit amet, consectetur adipiscing elit </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="clearfix filters-container m-t-10">
                        <div class="row">
                            <div class="col col-sm-6 col-md-2">
                                <div class="filter-tabs">
                                    <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                                        <li class="active"> <a data-toggle="tab" href="#grid-container"><i class="icon fa fa-th-large"></i>Grid</a> </li>
                                        <li><a data-toggle="tab" href="#list-container"><i class="icon fa fa-th-list"></i>List</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <!-- PRODUCT GRID VIEW -->
                    <div class="search-result-container ">
                        <div id="myTabContent" class="tab-content category-list">
                            <div class="tab-pane active " id="grid-container">
                                <div class="category-product">
                                    <div class="row">
                                        @foreach($searchProducts as $product)
                                            <div class="col-sm-6 col-md-4 wow fadeInUp">
                                                <div class="products">
                                                    <div class="product">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="{{ route('index.show', $product->product_slug_en) }}">
                                                                    @if(!empty($product->product_thumbnail))
                                                                        @if(file_exists('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail))
                                                                            <img src="{{ asset('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail) }}"
                                                                                 alt="product image"
                                                                        @else
                                                                            <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $product->id ."/" . $product->product_thumbnail)}}"
                                                                                 alt="product image">
                                                                        @endif
                                                                    @endif
                                                                </a>
                                                            </div>

                                                            <div>
                                                                @if ($product->discount_percentage == 0)
                                                                    <div class="tag new"><span>new</span></div>
                                                                @else
                                                                    <div class="tag hot"><span>{{ $product->discount_percentage }}%</span></div>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="product-info text-left">
                                                            <h3 class="name">
                                                                <a href="{{ route('index.show', $product->product_slug_en) }}">
                                                                    @if(session()->get('language') == 'spanish') {{ $product->product_name_es }} @else {{ $product->product_name_en  }} @endif
                                                                </a>
                                                            </h3>
                                                            @include('frontend.partials.reviews')
                                                            <div class="description">
                                                                @if(session()->get('language') == 'spanish')
                                                                    {{ strip_tags($product->productExtension->short_description_es ) }}
                                                                @else
                                                                    {{ strip_tags($product->productExtension->short_description_en )  }}
                                                                @endif
                                                            </div>
                                                            <div class="product-price">
                                                                @if ($product->discount_percentage == 0)
                                                                    <span class="price">${{ $product->selling_price }}</span>
                                                                @else
                                                                    <span class="price">${{ $product->discount_price}}</span>
                                                                    <span class="price-before-discount">${{ $product->selling_price }}</span>
                                                                @endif
                                                            </div><!-- /.product-price -->
                                                        </div><!-- /.product-info -->
                                                    </div><!-- /.product -->
                                                </div><!-- /.products -->
                                            </div><!-- /.item -->
                                        @endforeach
                                    </div><!-- /.row -->
                                    {{ $searchProducts->links() }}
                                </div><!-- /.category-product -->
                            </div><!-- /.tab-pane -->

                            <!-- PRODUCT LIST VIEW -->
                            <div class="tab-pane "  id="list-container">
                                <div class="category-product">
                                    @foreach($searchProducts as $product)
                                        <div class="category-product-inner wow fadeInUp">
                                            <div class="products">
                                                <div class="product-list product">
                                                    <div class="row product-list-row">
                                                        <div class="col col-sm-4 col-lg-4">
                                                            <div class="product-image">
                                                                <div class="image">
                                                                    <a href="{{ route('index.show', $product->product_slug_en) }}">
                                                                        @if(!empty($product->product_thumbnail))
                                                                            @if(file_exists('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail))
                                                                                <img src="{{ asset('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail) }}"
                                                                                     alt="product image"
                                                                            @else
                                                                                <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $product->id ."/" . $product->product_thumbnail)}}"
                                                                                     alt="product image">
                                                                            @endif
                                                                        @endif
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <!-- /.product-image -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col col-sm-8 col-lg-8">
                                                            <div class="product-info">
                                                                <h3 class="name">
                                                                    <a href="{{ route('index.show', $product->product_slug_en) }}">
                                                                        @if(session()->get('language') == 'spanish') {{ $product->product_name_es }} @else {{ $product->product_name_en  }} @endif
                                                                    </a>
                                                                </h3>
                                                                @include('frontend.partials.reviews')
                                                                <div class="product-price">
                                                                    @if ($product->discount_percentage == 0)
                                                                        <span class="price">${{ $product->selling_price }}</span>
                                                                    @else
                                                                        <span class="price">${{ $product->discount_price}}</span>
                                                                        <span class="price-before-discount">${{ $product->selling_price }}</span>
                                                                    @endif
                                                                </div>
                                                                <!-- /.product-price -->
                                                                <div class="description m-t-10">
                                                                    @if(session()->get('language') == 'spanish')
                                                                        {{ strip_tags($product->productExtension->long_description_es ) }}
                                                                    @else
                                                                        {{ strip_tags($product->productExtension->long_description_en )  }}
                                                                    @endif
                                                                </div>
                                                            </div><!-- /.product-info -->
                                                        </div><!-- /.col -->
                                                    </div><!-- /.product-list-row -->
                                                    <div>
                                                        @if ($product->discount_percentage == 0)
                                                            <div class="tag new"><span>new</span></div>
                                                        @else
                                                            <div class="tag hot"><span>{{ $product->discount_percentage }}%</span></div>
                                                        @endif
                                                    </div>
                                                </div><!-- /.product-list -->
                                            </div><!-- /.products -->
                                        </div><!-- /.category-product-inner -->
                                    @endforeach
                                    {{ $searchProducts->links() }}
                                </div><!-- /.category-product -->
                            </div><!-- /.tab-pane #list-container -->
                        </div><!-- /.tab-content -->
                    </div><!-- /.search-result-container -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
        <br><br>
    </div><!-- /.body-content -->
@endsection


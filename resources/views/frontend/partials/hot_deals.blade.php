<div class="sidebar-widget hot-deals wow fadeInUp outer-bottom-xs">
    <h3 class="section-title">@if(session()->get('language') == 'spanish') 50 mejores ofertas @else Top 50 Hot Deals @endif</h3>
    <div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-ss">
        @if(cache('categoryProducts')['hotDeals'])
        @foreach(cache('categoryProducts')['hotDeals'] as $product)
            @if($product->hot_deals == 1 && $product->product->discount_percentage > 0 && $product->product->status == 1)
                <div class="item">
                    <div class="products">
                        <div class="hot-deal-wrapper">
                            <div class="image">
                                @if(!empty($product->product->product_thumbnail))
                                    @if(file_exists('uploads/product_thumbnail/' . $product->product->id ."/" . $product->product->product_thumbnail))
                                        <img src="{{ asset('uploads/product_thumbnail/' . $product->product->id ."/" . $product->product->product_thumbnail) }}"
                                             alt="product image">
                                    @else
                                        <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $product->product->id ."/" . $product->product->product_thumbnail)}}"
                                             alt="product image">
                                    @endif
                                @endif
                            </div>
                            @if($product->product->discount_percentage > 0)
                                <div class="sale-offer-tag">
                                    <span>{{ $product->product->discount_percentage }}%<br>off</span>
                                </div>
                            @endif
                        </div>

                        <div class="product-info text-left m-t-20">
                            <h3 class="name">
                                <a href="{{ route('index.show', $product->product->product_slug_en) }}">
                                    @if(session()->get('language') == 'spanish') {{ $product->product->product_name_es }} @else {{ $product->product->product_name_en  }} @endif
                                </a>
                            </h3>
                            @include('frontend.partials.product_extension_reviews')
                            <div class="product-price">
                                @if ($product->product->discount_percentage == 0)
                                    <span class="price">${{ $product->product->selling_price }}</span>
                                @else
                                    <span class="price">${{ $product->product->discount_price}}</span>
                                    <span class="price-before-discount">${{ $product->product->selling_price }}</span>
                                @endif
                            </div><!-- /.product-price -->
                        </div><!-- /.product-info -->
                    </div>
                </div>
            @endif
        @endforeach
        @endif
    </div><!-- /.sidebar-widget -->
</div>

<div class="featured-product product-tag wow fadeInUp">
    <h3 class="section-title">
        @if(session()->get('language') == 'spanish') Etiquetas de productos @else Product Tags @endif
    </h3>
    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
        <div class="tag-list">
            @foreach(cache('categoryProducts')['productTags'] as $productTag)
            @if(session()->get('language') == 'spanish')
                <a href="{{ route('index.product_tags', $productTag->product_tags_en) }}" title="{{ $productTag->product_tags_es }}" class="item active">
                    {{ str_replace(',',' ',$productTag->product_tags_es) }}
                </a>
            @else
                <a href="{{ route('index.product_tags', $productTag->product_tags_en) }}" title="{{ $productTag->product_tags_en }}" class='item active'>
                    {{ str_replace(',',' ',$productTag->product_tags_en) }}
                </a>
            @endif
            @endforeach
        </div>
    </div><!-- /.sidebar-widget-body -->
</div><!-- /.sidebar-widget -->

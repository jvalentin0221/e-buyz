<div class="sidebar-widget outer-bottom-small wow fadeInUp">
    <h3 class="section-title">
        @if(session()->get('language') == 'spanish')Ofertas especiales @else Special Offers @endif
    </h3>
    <div class="sidebar-widget-body outer-top-xs">
        <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
            <div class="item">
                <div class="products special-product">
                    @foreach(cache('categoryProducts')['specialOffers'] as $product)
                        @if($product->special_offer == 1)
                            <div class="product">
                                <div class="product-micro">
                                    <div class="row product-micro-row">
                                        <div class="col col-xs-5">
                                            <div class="product-image">
                                                <div class="image">
                                                    @if(!empty($product->product->product_thumbnail))
                                                        @if(file_exists('uploads/product_thumbnail/' . $product->product->id ."/" . $product->product->product_thumbnail))
                                                            <img src="{{ asset('uploads/product_thumbnail/' . $product->product->id ."/" . $product->product->product_thumbnail) }}"
                                                                 alt="product image">
                                                        @else
                                                            <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $product->product->id ."/" . $product->product->product_thumbnail)}}"
                                                                 alt="product image">
                                                        @endif
                                                    @endif
                                                </div><!-- /.image -->
                                            </div><!-- /.product-image -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col col-xs-7">
                                            <div class="product-info">
                                                <h3 class="name">
                                                    <a href="{{ route('index.show', $product->product->product_slug_en ) }}">
                                                        @if(session()->get('language') == 'spanish') {{ $product->product->product_name_es }} @else {{ $product->product->product_name_en  }} @endif
                                                    </a>
                                                </h3>
                                                @include('frontend.partials.product_extension_reviews')
                                                <div class="product-price">
                                                    @if ($product->product->discount_percentage == 0)
                                                        <span class="price">${{ $product->product->selling_price }}</span>
                                                    @else
                                                        <span class="price">${{ $product->product->discount_price}}</span>
                                                        <span class="price-before-discount">${{ $product->product->selling_price }}</span>
                                                    @endif
                                                </div><!-- /.product-price -->
                                            </div>
                                        </div><!-- /.col -->
                                    </div><!-- /.product-micro-row -->
                                </div><!-- /.product-micro -->
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div><!-- /.sidebar-widget-body -->
</div><!-- /.sidebar-widget -->

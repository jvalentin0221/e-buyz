<form method="POST" action="{{ route('cart.add_to_cart') }}">
    @csrf
    <ul class="list-unstyled">
        <li class="add-cart-button btn-group">
            <input type="hidden" name="product_id" value="{{ $product->id }}">
            <input type="hidden" name="quantity" value="{{ $product->product_qty }}">
            <input type="hidden" name="requested_quantity" value="1">
            <input type="hidden" name="product_colors" value={{ NULL }}>
            <input type="hidden" name="product_sizes" value={{ NULL }}>
            <input type="hidden" name="product_slug_en" value="{{ $product->product_slug_en }}">
            <input type="hidden" name="product_thumbnail" value="{{ $product->product_thumbnail }}">
            <input type="hidden" name="product_slug_en" value="{{ $product->product_slug_en }}">
            <input type="hidden" name="product_name_en" value="{{ $product->product_name_en }}">
            <input type="hidden" name="product_name_es" value="{{ $product->product_name_es }}">
            <input type="hidden" name="product_size_en" value="{{ $product->product_size_en }}">
            <input type="hidden" name="product_size_es" value="{{ $product->product_size_es }}">
            <input type="hidden" name="product_color_en" value="{{ $product->product_color_en }}">
            <input type="hidden" name="product_color_es" value="{{ $product->product_color_es }}">
            <input type="hidden" name="product_code" value="{{ $product->productExtension->product_code }}">
            <input type="hidden" name="product_price" value="{{ $product->discount_price }}">
            <button data-toggle="tooltip" class="btn btn-primary icon" type="submit" title="Add Cart">
                <i class="fa fa-shopping-cart"></i>
            </button>
        </li>
    </ul>
</form>

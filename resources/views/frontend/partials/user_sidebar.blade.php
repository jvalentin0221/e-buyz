<div class="col-md-2"><br>
    @if(!empty(Auth::user()->avatar))
        @if(file_exists('uploads/avatar/' . Auth::user()->id ."/" . Auth::user()->avatar))
            <img class="card-img-top"
                 style="border-radius: 50%"
                 height="100%"
                 width="100%"
                 src="{{ asset('uploads/avatar/' . Auth::user()->id ."/" . Auth::user()->avatar) }}"
                 alt="user profile image"><br><br>
        @else
            <img class="card-img-top rounded-circle"
                 src="{{ Storage::disk('s3')->url('avatar/' . Auth::user()->id ."/" . Auth::user()->avatar)  }}"
                 style="border-radius: 50%"
                 height="100%"
                 width="100%"
                 alt="user profile image"><br><br>
        @endif
    @else
        <img class="card-img-top rounded-circle"
             src="{{ asset('assets/images/user_placeholder.png') }}"
             style="border-radius: 50%"
             height="100%"
             width="100%"
             alt="user profile image"><br><br>
    @endif
    <div>
        <a href="{{ route('user.profile', Auth::user()->id) }}" class="btn btn-primary btn-small btn-block">
            @if(session()->get('language') == 'spanish')Hogar @else Home @endif
        </a>
        <a href="{{ route('user.profile.edit', Auth::user()->id) }}" class="btn btn-primary btn-small btn-block">
            @if(session()->get('language') == 'spanish')Actualización del perfil @else Update Profile @endif
        </a>
        <a href="{{ route('user.password.index', Auth::user()->id) }}" class="btn btn-primary btn-small btn-block">
            @if(session()->get('language') == 'spanish') Cambiar la contraseña @else Change Password @endif
        </a>
        <a href="{{ route('user.order.index') }}" class="btn btn-primary btn-small btn-block">
            @if(session()->get('language') == 'spanish')Mis ordenes @else My Orders @endif
        </a>
        <a href="{{ route('user.return.index') }}" class="btn btn-primary btn-small btn-block">
            @if(session()->get('language') == 'spanish') Órdenes de devolución @else Return Orders @endif
        </a>
        <a href="{{ route('user.cancel.index') }}" class="btn btn-primary btn-small btn-block">
            @if(session()->get('language') == 'spanish') Cancelar pedidos @else Cancel Orders @endif
        </a>
        <a href="{{ route('user.logout') }}" class="btn btn-danger btn-small btn-block">
            @if(session()->get('language') == 'spanish')Cerrar sesión @else Logout @endif
        </a><br>
    </div>
</div>


<div class="side-menu animate-dropdown outer-bottom-xs">
    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i>
        <span>@if(session()->get('language') == 'spanish') Categorias @else Categories @endif</span>
    </div>
    <nav class="yamm megamenu-horizontal">
        <ul class="nav">
            @if(cache('categoryLevels')['categories'])
                @foreach(cache('categoryLevels')['categories'] as $category)
                    <li class="dropdown menu-item"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="{{ $category->category_icon }}" aria-hidden="true"></i>
                            @if(session()->get('language') == 'spanish') {{ $category->category_name_es }} @else {{ $category->category_name_en }} @endif
                        </a>
                        <ul class="dropdown-menu mega-menu">
                            <li class="yamm-content">
                                <div class="row">
                                    @foreach(cache('categoryLevels')['subcategories'] as $subcategory)
                                        @if($subcategory->category_id == $category->id)
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li>
                                                    <a href="{{ url('subcategories/'.$subcategory->subcategory_name_en.'/'.$subcategory->subcategory_slug_en ) }}">
                                                        <h2 class="title">
                                                            @if(session()->get('language') == 'spanish') {{ $subcategory->subcategory_name_es }} @else {{ $subcategory->subcategory_name_en }} @endif
                                                        </h2>
                                                    </a>
                                                    @foreach(cache('categoryLevels')['subsubcategories']  as $subsubcategory)
                                                        @if($subsubcategory->sub_category_id == $subcategory->id)
                                                        <ul class="links font-weight-bold">
                                                            <li>
                                                                <a href="{{ url('subsubcategories/'.$subsubcategory->subsubcategory_name_en.'/'.$subsubcategory->subsubcategory_slug_en ) }}">
                                                                    @if(session()->get('language') == 'spanish') {{ $subsubcategory->subsubcategory_name_es }} @else {{ $subsubcategory->subsubcategory_name_en }} @endif
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        @endif
                                                    @endforeach
                                                </li>
                                            </ul>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                    </li>
                @endforeach <!-- End Category Foreach -->
            @endif
        </ul>
    </nav>
</div>

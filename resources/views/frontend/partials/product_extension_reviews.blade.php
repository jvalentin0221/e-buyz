<div class="rateit-selected m-t-20">
    @if(round($product->product->reviews->avg('rating')) == 0)
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
    @elseif(round($product->product->reviews->avg('rating')) == 1 || round($product->product->reviews->avg('rating')) < 2)
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
    @elseif(round($product->product->reviews->avg('rating')) == 2 || round($product->product->reviews->avg('rating')) < 3)
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>
    @elseif(round($product->product->reviews->avg('rating')) == 3 || round($product->product->reviews->avg('rating')) < 4)
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span>

    @elseif(round($product->product->reviews->avg('rating')) == 4 || round($product->product->reviews->avg('rating')) < 5)
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star"></span>
    @elseif(round($product->product->reviews->avg('rating')) == 5 || round($product->product->reviews->avg('rating')) < 5)
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
    @endif
</div><!-- /.rating-reviews -->

<section class="section featured-product wow fadeInUp">
    <h3 class="section-title">
        @if(session()->get('language') == 'spanish') Nuevos productos @else New Products @endif
    </h3>
    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
        @foreach(cache('categoryProducts')['newProducts'] as $product)
            @if($product->new == 1)
                <div class="item item-carousel">
                    <div class="products">
                        <div class="product">
                            <div class="product-image">
                                <div class="image">
                                    <a href="{{ route('index.show', $product->product->product_slug_en) }}">
                                    @if(!empty($product->product->product_thumbnail))
                                        @if(file_exists('uploads/product_thumbnail/' . $product->product->id ."/" . $product->product->product_thumbnail))
                                            <img src="{{ asset('uploads/product_thumbnail/' . $product->product->id ."/" . $product->product->product_thumbnail) }}"
                                                 alt="product image"
                                        @else
                                            <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $product->product->id ."/" . $product->product->product_thumbnail)}}"
                                                 alt="product image">
                                        @endif
                                    </a>
                                </div><!-- /.image -->
                                <div>
                                    @if ($product->product->discount_percentage == 0)
                                        <div class="tag new"><span>new</span></div>
                                    @else
                                        <div class="tag hot"><span>{{ $product->product->discount_percentage }}%</span></div>
                                    @endif
                                </div>
                            </div><!-- /.product-image -->

                            <div class="product-info text-left">
                                <h3 class="name">
                                    <a href="{{ route('index.show', $product->product->product_slug_en) }}">
                                        @if(session()->get('language') == 'spanish') {{ $product->product->product_name_es }} @else {{ $product->product->product_name_en  }} @endif
                                    </a>
                                </h3>

                                @include('frontend.partials.product_extension_reviews')

                                <div class="product-price">
                                    @if ($product->product->discount_percentage == 0)
                                        <span class="price">${{ $product->product->selling_price }}</span>
                                    @else
                                        <span class="price">${{ $product->product->discount_price }}</span>
                                        <span class="price-before-discount">${{ $product->product->selling_price }}</span>
                                    @endif
                                </div><!-- /.product-price -->
                            </div><!-- /.product-info -->
                            @endif
                            <div class="cart clearfix animate-effect">
                                <div class="action">
                                    @include('frontend.partials.additional_add_to_cart')
                                </div><!-- /.action -->
                            </div><!-- /.cart -->
                        </div><!-- /.product -->
                    </div><!-- /.products -->
                </div><!-- /.item -->
            @endif
        @endforeach
    </div><!-- /.home-owl-carousel -->
</section>

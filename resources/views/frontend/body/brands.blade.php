<div id="brands-carousel" class="logo-slider wow fadeInUp">
    <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
            @if(cache('brands')['brandCount'] >= 1)
                @foreach(cache('brands')['brands'] as $brand)
                <div class="item m-t-15">
                    <a href="{{ url('https://www.' . $brand->brand_url) }}" class="image">
                        @if(!empty($brand->brand_image))
                            @if(file_exists('uploads/brand_image/' . $brand->id ."/" . $brand->brand_image))
                                <img src="{{ asset('uploads/brand_image/' . $brand->id. "/" . $brand->brand_image) }}"
                                     style="width:100px; height: 100px;"
                                     alt="brand image">
                            @else
                                <img src="{{ Storage::disk('s3')->url('brand_image/' . $brand->id ."/" . $brand->brand_image)}}"
                                     style="width:100px; height: 100px;"
                                     alt="brand image">
                            @endif
                        @endif
                    </a>
               </div>
                @endforeach
            @else
                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="{{ Storage::disk('s3')->url('images/brands/brand3.png')}}" src="{{ Storage::disk('s3')->url('images/blank.gif')}}" alt="brand logo">
                    </a>
                </div>

                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="{{ Storage::disk('s3')->url('images/brands/brand5.png')}}" src="{{ Storage::disk('s3')->url('images/blank.gif')}}" alt="">
                    </a>
                </div>

                <div class="item"> <a href="#" class="image">
                        <img data-echo="{{ Storage::disk('s3')->url('images/brands/brand6.png')}}" src="{{ Storage::disk('s3')->url('images/blank.gif')}}" alt="">
                    </a>
                </div>

                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="{{ Storage::disk('s3')->url('images/brands/brand2.png')}}" src="{{ Storage::disk('s3')->url('images/blank.gif')}}" alt="">
                    </a>
                </div>

                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="{{ Storage::disk('s3')->url('images/brands/brand4.png')}}" src="{{ Storage::disk('s3')->url('images/blank.gif')}}" alt="">
                    </a>
                </div>

                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="{{ Storage::disk('s3')->url('images/brands/brand1.png')}}" src="{{ Storage::disk('s3')->url('images/blank.gif')}}" alt="">
                    </a>
                </div>
            @endif
        </div><!-- /.owl-carousel #logo-slider -->
    </div><!-- /.logo-slider-inner -->
</div>

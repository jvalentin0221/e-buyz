<header class="header-style-1">
    <!-- ============================================== TOP MENU ============================================== -->
    <div class="top-bar animate-dropdown">
        <div class="container">
            <div class="header-top-inner">
                <div class="cnt-account">
                    <ul class="list-unstyled">
                        <li><a href="{{ route('cart.index') }}"><i class="icon fa fa-shopping-cart"></i>
                                @if(session()->get('language') == 'spanish') Mi Carrito @else My Cart @endif
                            </a>
                        </li>
                        <li><a href="{{ route('checkout.index') }}"><i class="icon fa fa-check"></i>
                                @if(session()->get('language') == 'spanish') Verificar @else Checkout @endif
                            </a>
                        </li>

                        @guest
                            <li><a href="{{ route('login') }}"><i class="icon fa fa-lock"></i>
                                    @if(session()->get('language') == 'spanish') Iniciar Sesión / Registrarse @else Login/Register @endif
                                </a>
                            </li>
                        @endguest
                        @auth
                            <li><a href="" type="button" data-toggle="modal" data-target="#orderTrack"><i class="icon fa fa-truck"></i>
                                    @if(session()->get('language') == 'spanish') Rastreo De Orden @else Track Your Order @endif
                                </a>
                            </li>
                            @if(auth()->user()->role === 'admin')
                                <li><a href="{{ route('admin.dashboard') }}"><i class="icon fa fa-lock"></i>
                                        @if(session()->get('language') == 'spanish') Tablero @else Dashboard @endif
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->role === 'user')
                                <li><a href="{{ route('admin.dashboard', auth()->user()->id) }}"><i class="icon fa fa-user"></i>
                                        @if(session()->get('language') == 'spanish') Perfil @else Profile @endif
                                    </a>
                                </li>
                            @endif
                        @endauth
                    </ul>
                </div><!-- /.cnt-account -->

                <div class="cnt-block">
                    <ul class="list-unstyled list-inline">
                        <li class="dropdown dropdown-small">
                            <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                <span class="value">
                                    @if(session()->get('language') == 'spanish') Idioma @else Language @endif
                                </span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                @if(session()->get('language') == 'spanish')
                                    <li><a href="{{ route('language.english') }}">English</a></li>
                                @else
                                    <li><a href="{{ route('language.spanish') }}">Español</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul><!-- /.list-unstyled -->
                </div><!-- /.cnt-cart -->
                <div class="clearfix"></div>
            </div><!-- /.header-top-inner -->
        </div><!-- /.container -->
    </div><!-- /.header-top -->
    <!-- ============================================== TOP MENU : END ============================================== -->
    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
                    <!-- ============================================================= LOGO ============================================================= -->

                    <div class="logo">
                        <a href="{{ route('index') }}">
                        @if(isset(cache('sliderCompanyInfo')['companyInfo']))
                            @if(file_exists('uploads/logo/' . cache('sliderCompanyInfo')['companyInfo']['id'] ."/" . cache('sliderCompanyInfo')['companyInfo']['logo']))
                                <img src="{{ asset('uploads/logo/' .  cache('sliderCompanyInfo')['companyInfo']['id'] . '/' . cache('sliderCompanyInfo')['companyInfo']['logo']) }}"
                                     alt="logo"
                                     style="width:80px; height: 70px;">
                            @else
                                <img src="{{ asset('assets/images/logo/e-buyz-logo.png')}}"
                                     style="width:80px; height: 70px;"
                                     alt="logo">
                            @endif
                        @else
                            <img src="{{ asset('assets/images/logo/e-buyz-logo.png')}}"
                                 style="width:80px; height: 70px;"
                                 alt="logo">
                        @endif
                        </a>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
                    <!-- /.contact-row -->
                    <!-- ============================================================= SEARCH AREA ============================================================= -->
                    <div class="search-area">
                        <form method="GET" action="{{ route('search.searchProducts') }}">
                            @csrf
                            <div class="control-group">
                                <input class="search-field" name="search" placeholder= @if(session()->get('language') == 'spanish') "Busca aquí..." @else "Search here...." @endif />
                                <button class="search-button" type="submit" ></button>
                            </div>
                        </form>
                    </div>
                    <!-- /.search-area -->
                    <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
                <!-- /.top-search-holder -->

                <div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">
                    <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
                    <div class="dropdown-cart">
                        <a href="{{ route('cart.index') }}" class="lnk-cart">
                            <div class="items-cart-inner">
                                <div class="basket"> <i class="glyphicon glyphicon-shopping-cart"></i> </div>
                                <div class="basket-item-count">
                                    @if(!empty($userCartItems))
                                    <span class="count">{{ $userCartItems->count()  }}</span>
                                    @else
                                        <span class="count">0</span>
                                    @endif
                                </div>
                                <div class="total-price-basket">
                                    <span class="lbl"> @if(session()->get('language') == 'spanish') Carro @else cart - @endif</span>
                                    <span class="total-price">
                                     @if(!empty(auth()->user()->cart))
                                        <span class="sign">${{ $subTotal }}</span>
                                        <span class="value"></span>
                                    @else
                                        <span class="sign">$0</span>
                                        <span class="value"></span>
                                     @endif
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div><!-- /.dropdown-cart -->
                    <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= --> </div>
                <!-- /.top-cart-row -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.main-header -->

    <!-- ============================================== NAVBAR ============================================== -->
    <div class="header-nav animate-dropdown">
        <div class="container">
            <div class="yamm navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                </div>
                <div class="nav-bg-class">
                    <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                        <div class="nav-outer">
                            <ul class="nav navbar-nav">
                                <li class="active dropdown yamm-fw">
                                    <a href="{{ route('index') }}">
                                        @if(session()->get('language') == 'spanish') Hogar @else Home @endif
                                    </a>
                                </li>
                            @if(isset(cache('categoryLevels')['categories']))
                                @foreach(cache('categoryLevels')['categories']  as $category)
                                    <li class="dropdown yamm mega-menu">
                                        <a href="home.html" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">
                                            @if(session()->get('language') == 'spanish') {{ $category->category_name_es }} @else {{ $category->category_name_en }} @endif
                                        </a>
                                        <ul class="dropdown-menu container">
                                            <li>
                                                <div class="yamm-content">
                                                    <div class="row">
                                                        @foreach(cache('categoryLevels')['subcategories']  as $subcategory)
                                                            @if($subcategory->category_id == $category->id)
                                                            <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
                                                                <a href="{{ url('subcategories/'.$subcategory->subcategory_name_en.'/'.$subcategory->subcategory_slug_en ) }}">
                                                                    <h2 class="title">
                                                                        @if(session()->get('language') == 'spanish') {{ $subcategory->subcategory_name_es }} @else {{ $subcategory->subcategory_name_en }} @endif
                                                                    </h2>
                                                                </a>
                                                            @foreach(cache('categoryLevels')['subsubcategories']  as $subsubcategory)
                                                                @if($subsubcategory->sub_category_id == $subcategory->id)
                                                                <ul class="links">
                                                                    <li>
                                                                        <a href="{{ url('subsubcategories/'.$subsubcategory->subsubcategory_name_en.'/'.$subsubcategory->subsubcategory_slug_en ) }}">
                                                                            @if(session()->get('language') == 'spanish') {{ $subsubcategory->subsubcategory_name_es }} @else {{ $subsubcategory->subsubcategory_name_en }} @endif
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                @endif
                                                            @endforeach
                                                            </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                @endforeach
                            @endif
                            </ul>
                            <!-- /.navbar-nav -->
                            <div class="clearfix"></div>
                        </div><!-- /.nav-outer -->
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.nav-bg-class -->
            </div><!-- /.navbar-default -->
        </div><!-- /.container-class -->
    </div><!-- /.header-nav -->
    <!-- ============================================== NAVBAR : END ============================================== -->
    <!-- Order Tracking Modal -->
    <div class="modal fade" id="orderTrack" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="orderTrack">
                        Track Your Order
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('user.track.check_invoice_code') }}">
                        @csrf
                        <div class="modal-body">
                            <label>
                                @if(session()->get('language') == 'spanish') Código de factura @else Invoice Code @endif
                            </label>
                            <input type="text" name="invoice_code" class="form-control" placeholder=
                            @if(session()->get('language') == 'spanish')
                                'Su código de factura de pedido'
                            @else
                                "Your order Invoice Code"
                            @endif
                        </div>
                        <button class="btn btn-danger" type="submit" style="margin-left: 17px;">
                            @if(session()->get('language') == 'spanish') Seguimiento ahora @else Track Now @endif
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>

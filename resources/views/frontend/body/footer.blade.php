<footer id="footer" class="footer color-bg">
    <div class="footer-bottom">
        <div class="container">
            <div style="color: white">
                <h3>* THIS IS A DEMO PROJECT. NO REAL PRODUCTS CAN BE PURCHASED IN THIS WEBSITE. CREATED FOR EDUCATIONAL PURPOSES ONLY *</h3>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">@if(session()->get('language') == 'spanish') Contacta con nosotros @else Contact Us @endif</h4>
                    </div>

                    <div class="module-body">
                        <ul class="toggle-footer" style="">
                            <li class="media">
                                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i> </span> </div>
                                <div class="media-body">
                                    <p>{{ cache('sliderCompanyInfo')['companyInfo']['company_address'] ?? "" }}</p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-mobile fa-stack-1x fa-inverse"></i> </span> </div>
                                <div class="media-body">
                                    <p>{{ cache('sliderCompanyInfo')['companyInfo']['phone_number_one'] ?? "" }}
                                        <br>
                                       {{ cache('sliderCompanyInfo')['companyInfo']['phone_number_two'] ?? "" }}
                                    </p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span> </div>
                                <div class="media-body">
                                    <span>
                                        {{ cache('sliderCompanyInfo')['companyInfo']['email'] ?? "" }}
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">
                            @if(session()->get('language') == 'spanish') Servicio al Cliente @else Customer Service @endif
                        </h4>
                    </div>

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first">
                                <a href="#" title="Contact us">
                                    @if(session()->get('language') == 'spanish') Mi cuenta @else My Account @endif
                                </a>
                            </li>
                            <li><a href="#" title="About us">
                                    @if(session()->get('language') == 'spanish') Historial de pedidos @else Order History @endif
                                </a>
                            </li>
                            <li><a href="#" title="faq">FAQ</a></li>
                            <li><a href="#" title="Popular Searches">
                                    @if(session()->get('language') == 'spanish') Especiales @else Specials @endif
                                </a>
                            </li>
                            <li class="last">
                                <a href="#" title="Where is my order?">
                                    @if(session()->get('language') == 'spanish') Centro de ayuda @else Help Center @endif
                                </a>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">
                            @if(session()->get('language') == 'spanish')Corporación @else Corporation @endif
                        </h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first">
                                <a title="Your Account" href="#">
                                    @if(session()->get('language') == 'spanish') Sobre nosotros @else About Us @endif
                                </a>
                            </li>
                            <li><a title="Information" href="#">
                                    @if(session()->get('language') == 'spanish')Servicio al Cliente @else Customer Service @endif
                                </a>
                            </li>
                            <li><a title="Addresses" href="#">
                                    @if(session()->get('language') == 'spanish')Empresa @else Company @endif
                                </a>
                            </li>
                            <li>
                                <a title="Addresses" href="#">
                                    @if(session()->get('language') == 'spanish') Relaciones con inversionistas @else Investor Relations @endif
                                </a>
                            </li>
                            <li class="last">
                                <a title="Orders History" href="#">
                                    @if(session()->get('language') == 'spanish')Búsqueda Avanzada @else Advanced Search @endif
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">
                            @if(session()->get('language') == 'spanish') Por qué elegirnos @else Why Choose Us @endif
                        </h4>
                    </div>

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first">
                                <a href="#" title="About us">
                                    @if(session()->get('language') == 'spanish')Guía de compras @else Shopping Guide @endif
                                </a>
                            </li>
                            <li>
                                <a href="#" title="Blog">
                                    @if(session()->get('language') == 'spanish') Empresa @else Blog @endif
                                </a>
                            </li>
                            <li>
                                <a href="#" title="Company">
                                    @if(session()->get('language') == 'spanish') Empresa @else Company @endif
                                </a>
                            </li>
                            <li>
                                <a href="#" title="Investor Relations">
                                    @if(session()->get('language') == 'spanish') Relaciones con inversionistas @else Investor Relations @endif
                                </a>
                            </li>
                            <li class="last">
                                <a href="#">
                                    @if(session()->get('language') == 'spanish') Contacta con nosotros @else Contact Us @endif
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.module-body -->
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-padding social">
                <ul class="link">
                    <li class="fb pull-left"><a target="_blank" rel="nofollow" href="{{ cache('sliderCompanyInfo')['companyInfo']['facebook']?? url('https://www.facebook.com/') }}" title="Facebook"></a></li>
                    <li class="tw pull-left"><a target="_blank" rel="nofollow" href="{{ cache('sliderCompanyInfo')['companyInfo']['Twitter'] ?? url('https://twitter.com/?lang=en') }}" title="Twitter"></a></li>
                    <li class="linkedin pull-left"><a target="_blank" rel="nofollow" href="{{ cache('sliderCompanyInfo')['companyInfo']['linkedin'] ?? url('https://www.linkedin.com/') }}" title="Linkedin"></a></li>
                    <li class="youtube pull-left"><a target="_blank" rel="nofollow" href="{{ cache('sliderCompanyInfo')['companyInfo']['youtube'] ?? url('https://www.youtube.com/') }}" title="Youtube"></a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="clearfix payment-methods">
                    <ul>
                        <li><img src="{{ asset('assets/images/payment/stripe.png')}}" alt="stripe image"></li>
                        <li><img src="{{ Storage::disk('s3')->url('images/payments/2.png')}}" alt="discover iamge"></li>
                        <li><img src="{{ Storage::disk('s3')->url('images/payments/3.png')}}" alt="visa image"></li>
                    </ul>
                </div>
                <!-- /.payment-methods -->
            </div>
        </div>
    </div>
</footer>

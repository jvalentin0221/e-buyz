@extends('layouts.frontend.master')
@section('title', "Shopping Cart")
@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">Home</a></li>
                    <li class='active'>Shopping Cart</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content outer-top-xs">
        <div class="container">
            <div class="row">
                <div class="shopping-cart">
                    @foreach($cartItems as $cartItem)
                    <form method="POST" action="{{ route('cart.update')}}">
                        @method('PUT')
                        @csrf
                        <input type="hidden" name="cart_id" value="{{ $cartItem->id }}">
                        <div class="shopping-cart-table">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="cart-remove item">
                                                @if(session()->get('language') == 'spanish') Eliminar @else Remove @endif
                                            </th>
                                            <th class="cart-description item">
                                                @if(session()->get('language') == 'spanish') Imagen @else Image @endif
                                            </th>
                                            <th class="cart-product-name item">
                                                @if(session()->get('language') == 'spanish') Nombre Del Producto @else Product Name @endif
                                            </th>
                                            <th class="cart-qty item">
                                                @if(session()->get('language') == 'spanish') Cantidad @else Quantity @endif
                                            </th>
                                            <th class="cart-qty item">
                                                @if(session()->get('language') == 'spanish') Precio @else Price @endif
                                            </th>
                                            <th class="cart-sub-total item">
                                                @if(session()->get('language') == 'spanish') Total parcial @else Subtotal @endif
                                            </th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td colspan="7">
                                                <div class="shopping-cart-btn">
                                                    <span>
                                                        <button type="submit" class="btn btn-primary pull-right outer-right-xs">
                                                            @if(session()->get('language') == 'spanish') Actualizar carrito de compra @else Update Shopping Cart @endif
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <tr>
                                        <td class="remove-item">
                                            <button form="{{'button' . $cartItem->id }}" type="submit" class="icon" title="Delete Item"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                        <td class="cart-image">
                                            @if(!empty($cartItem->product_thumbnail))
                                                @if(file_exists('uploads/product_thumbnail/' . $cartItem->product_id ."/" . $cartItem->product_thumbnail))
                                                    <img src="{{ asset('uploads/product_thumbnail/' . $cartItem->product_id ."/" . $cartItem->product_thumbnail) }}"
                                                         alt="{{ $cartItem->product_name_en }}"
                                                @else
                                                    <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $cartItem->product_id ."/" . $cartItem->product_thumbnail)}}"
                                                         alt="{{ $cartItem->product_name_en }}">
                                                @endif
                                            @endif
                                        </td>
                                        <td class="cart-product-name-info">
                                            <h4 class='cart-product-description'>
                                                <a href="{{ route('index.show', $cartItem->product_slug_en) }}">
                                                    @if(session()->get('language') == 'spanish') {{ $cartItem->product_name_es }} @else {{ $cartItem->product_name_en  }} @endif
                                                </a>
                                            </h4>
                                            <div class="stock-container info-container m-t-10">
                                                <div class="stock-box">
                                                    @if($cartItem->quantity <= 10)
                                                        <span class="text-danger">
                                                            @if(session()->get('language') == 'spanish')
                                                                Solamente {{$cartItem->quantity}} quedaban en almacenaje
                                                            @else Only {{$cartItem->quantity}} left in stock
                                                            @endif
                                                        </span>
                                                    @else
                                                        <span class="text-success">
                                                            @if(session()->get('language') == 'spanish') En stock @else In Stock @endif
                                                        </span>
                                                    @endif
                                                </div>
                                                <br>
                                            </div>
                                            @if(empty(session()->get('colorsEN' . $cartItem->id )) )
                                            @else
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group cart-product-info">
                                                        @if(session()->get('language') == 'spanish')
                                                            <label class="">
                                                                Elegir color:
                                                                <span>{{ $cartItem->product_color_es }} </span>
                                                            </label>
                                                            <select name="product_color_es" class="form-control unicase-form-control selectpicker" style="display: none;">
                                                                @foreach(explode(',', session()->get('colorsES' . $cartItem->id)) as $color)
                                                                    <option name="product_color_es">{{ ucwords($color) }}</option>
                                                                @endforeach
                                                            </select>
                                                        @else
                                                            <label class="info-title control-label">
                                                                Choose Color:
                                                                <span>{{ $cartItem->product_color_en }} </span>
                                                            </label>
                                                            <select name="product_color_en" class="form-control unicase-form-control selectpicker" style="display: none;">
                                                                @foreach(explode(',', session()->get('colorsEN' . $cartItem->id)) as $color)
                                                                    <option name="product_color_en">{{ ucwords($color) }}</option>
                                                                @endforeach
                                                            </select>
                                                        @endif
                                                    </div> <!-- // end form group -->
                                                </div>
                                            @endif

                                            @if(empty(session()->get('sizesEN' . $cartItem->id)))
                                            @else
                                            <div class="col-sm-6">
                                                <div class="form group cart-product-info">
                                                    @if(session()->get('language') == 'spanish')
                                                        <label class="">
                                                            Elige el tamaño:
                                                            <span>{{ $cartItem->product_size_es }} </span>
                                                        </label>
                                                        <select name="product_size_es" class="form-control unicase-form-control selectpicker" style="display: none;">
                                                                @foreach(explode(',', session()->get('sizesES' . $cartItem->id)) as $color)
                                                                    <option name="product_size_es">{{ ucwords($color) }}</option>
                                                                @endforeach
                                                        </select>
                                                    @else
                                                        <label class="info-title control-label">
                                                              Choose Size:
                                                            <span>{{ $cartItem->product_size_en }} </span>
                                                        </label>
                                                        <select name="product_size_en" class="form-control unicase-form-control selectpicker" style="display: none;">
                                                            @foreach(explode(',', session()->get('sizesEN' . $cartItem->id)) as $size)
                                                                <option name="product_size_en">{{ ucwords($size) }}</option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </div> <!-- // end form group -->
                                                </div>
                                            </div>
                                            @endif
                                            </div>
                                        </td>

                                        <td class="cart-product-quantity">
                                            <div class="col-sm-2">
                                                <div class="cart-quantity">
                                                    <div class="quant-input">
                                                        <input min="1" max="{{ $cartItem->quantity }}" name="requested_quantity" value="{{ $cartItem->requested_quantity }}" type="number">
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="cart-product-sub-total"><span class="cart-sub-total-price">${{ $cartItem->product_price }}</span></td>
                                        <td class="cart-product-sub-total"><span class="cart-sub-total-price">${{ $cartItem->product_price * $cartItem->requested_quantity }}</span></td>
                                    </tr>
                                    </tbody><!-- /tbody -->
                                </table><!-- /table -->
                            </div>
                        </div><!-- /.shopping-cart-table -->
                    </form>
                    @endforeach
                    <!-- DELETE FORM -->
                        @foreach($cartItems as $cartItem)
                            <form class="" id="{{'button' . $cartItem->id }}" method="POST" action="{{ route('cart.delete') }}">
                                <input type="hidden" name="cart_id" value="{{ $cartItem->id }}">
                                @method('DELETE')
                                @csrf
                            </form>
                        @endforeach
                        <div class="col-md-6 col-sm-12 estimate-ship-tax">
                            <form method="POST" action="{{ route('coupon.add_coupon') }}">
                                @csrf
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <span class="estimate-title">
                                                     @if(session()->get('language') == 'spanish') Código de descuento @else Discount Code @endif
                                                </span>
                                                <p>
                                                    @if(session()->get('language') == 'spanish')
                                                        Ingrese su código de cupón si tiene uno
                                                    @else
                                                        Enter your coupon code if you have one...
                                                    @endif
                                                </p>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="coupon_code" class="form-control unicase-form-control text-input" placeholder="Your Coupon..">
                                                </div>
                                                <div class="clearfix pull-right">
                                                    <button type="submit" class="btn btn-primary">
                                                        @if(session()->get('language') == 'spanish') Aplicar cupón @else Apply Coupon @endif
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody><!-- /tbody -->
                                </table><!-- /table -->
                            </form>
                        </div>
                        <div class="col-md-6 col-sm-12 cart-shopping-total">
                            <form method="POST" action="{{ route('coupon.remove_coupon') }}">
                                @csrf
                                @method('DELETE')
                                <table class="table">
                                    <thead>
                                    @if(Session()->has('discount' . Auth::user()->id))
                                    <tr>
                                        <th>
                                            <div class="cart-grand-total">
                                                <button type="submit" href="{{ route('coupon.remove_coupon') }}" class="btn-sm btn-danger">X</button>
                                                {{ session()->get('discount' . Auth::user()->id) }}%
                                                @if(session()->get('language') == 'spanish') Descuento aplicado @else Discount Applied @endif
                                                <span class="inner-left-md">${{ number_format($subTotal * session()->get('discount' . Auth::user()->id) / 100 ,2, '.', '') }}</span>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            <div class="cart-grand-total">
                                                @if(session()->get('language') == 'spanish') Total parcial @else SubTotal @endif
                                                <span class="inner-left-md">${{ number_format($subTotal, 2, '.', '') }}</span>
                                            </div>
                                        </th>
                                    </tr>
                                    @endif
                                        <tr>
                                            <th>
                                                <div class="cart-grand-total">
                                                    @if(session()->get('language') == 'spanish') Gran total @else Grant Total @endif
                                                    <span class="inner-left-md">${{ number_format($subTotal - $subTotal * session()->get('discount' . Auth::user()->id) / 100 ,2, '.', '')  }}</span>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead><!-- /thead -->
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="cart-checkout-btn pull-right">
                                                    <div>
                                                        <a href="/" class="btn btn-primary">
                                                            @if(session()->get('language') == 'spanish') Seguir comprando @else Continue Shopping @endif
                                                        </a>
                                                    </div>

                                                    <a type="submit" href="{{ route('checkout.index') }}" class="btn btn-primary checkout-btn">
                                                        @if(session()->get('language') == 'spanish') Pasar por la caja @else Proceed To Checkout @endif
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody><!-- /tbody -->
                                </table><!-- /table -->
                            </form>
                        </div><!-- /.cart-shopping-total -->
                </div>
            </div>
        </div>
        <br><br><br><br>
    </div>
@endsection

@extends('layouts.frontend.master')
@section('title', "Tracking Order")
@section('content')
    <div class="container">
        <article class="order-tracker">
            <header class="order-tracker-header"><strong>My Orders / Tracking</strong></header>
            <div class="card-body">
                <div class="row" style="margin-left: 30px; margin-top: 20px;">
                    <div class="col-md-2">
                        <b> Invoice Number </b><br>
                        {{ $invoice->invoice_no }}
                    </div> <!-- // end col md 2 -->

                    <div class="col-md-2">
                        <b> Order Date </b><br>
                        {{ $invoice->order_date }}
                    </div> <!-- // end col md 2 -->

                    <div class="col-md-2">
                        <b> Shipping By - {{ $invoice->name }} </b><br>
                    </div> <!-- // end col md 2 -->

                    <div class="col-md-2">
                        <b> User Mobile Number </b><br>
                        {{ $invoice->phone }}
                    </div> <!-- // end col md 2 -->

                    <div class="col-md-2">
                        <b> Payment Method  </b><br>
                        {{ $invoice->payment_method  }}
                    </div> <!-- // end col md 2 -->

                    <div class="col-md-2">
                        <b> Total Amount  </b><br>
                        $ {{ $invoice->amount  }}
                    </div> <!-- // end col md 2 -->

                </div> <!-- // end row   -->

                <div class="track">
                    @if($invoice->status == 'pending')
                        <div class="step active"> <span class="icon"> <i class="fa fa-check"></i> </span> <span class="text">Order Pending</span> </div>
                        <div class="step"><span class="icon"> <i class="fa fa-check"></i></span><span class="text"> Order Confirmed</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Processing</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Picked</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Shipped</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Delivered</span></div>
                    @endif
                    @if($invoice->status == 'confirmed')
                        <div class="step active"> <span class="icon"> <i class="fa fa-check"></i> </span> <span class="text">Order Pending</span> </div>
                        <div class="step active"><span class="icon"> <i class="fa fa-check"></i></span><span class="text"> Order Confirmed</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Processing</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Picked</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Shipped</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Delivered</span></div>
                    @endif
                    @if($invoice->status == 'processing')
                        <div class="step active"> <span class="icon"> <i class="fa fa-check"></i> </span> <span class="text">Order Pending</span> </div>
                        <div class="step active"><span class="icon"> <i class="fa fa-check"></i></span><span class="text"> Order Confirmed</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Processing</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Picked</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Shipped</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Delivered</span></div>
                    @endif
                    @if($invoice->status == 'picked')
                        <div class="step active"> <span class="icon"> <i class="fa fa-check"></i> </span> <span class="text">Order Pending</span> </div>
                        <div class="step active"><span class="icon"> <i class="fa fa-check"></i></span><span class="text"> Order Confirmed</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Processing</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Picked</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Shipped</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Delivered</span></div>
                    @endif
                    @if($invoice->status == 'shipped')
                        <div class="step active"> <span class="icon"> <i class="fa fa-check"></i> </span> <span class="text">Order Pending</span> </div>
                        <div class="step active"><span class="icon"> <i class="fa fa-check"></i></span><span class="text"> Order Confirmed</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Processing</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Picked</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Shipped</span></div>
                        <div class="step"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Delivered</span></div>
                    @endif
                    @if($invoice->status == 'delivered')
                        <div class="step active"> <span class="icon"> <i class="fa fa-check"></i> </span> <span class="text">Order Pending</span> </div>
                        <div class="step active"><span class="icon"> <i class="fa fa-check"></i></span><span class="text"> Order Confirmed</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Processing</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Picked</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Order Shipped</span></div>
                        <div class="step active"><span class="icon"><i class="fa fa-check"></i></span><span class="text">Delivered</span></div>
                    @endif
                    @if($invoice->status == 'returned')
                        <div class="step active"> <span class="icon"> <i class="fa fa-check"></i> </span> <span class="text">Order Returned</span> </div>
                    @endif
                    @if($invoice->status == 'cancelled')
                        <div class="step active"> <span class="icon"> <i class="fa fa-check"></i> </span> <span class="text">Order Cancelled</span> </div>
                    @endif
            </div>
        </article>
    </div>
@endsection

@extends('layouts.frontend.master')
@section('title', "Return Orders")
@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                @include('frontend.partials.user_sidebar')
                <div class="col-md-8">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr style="background: #e2e2e2;">
                                <th class="col-md-1">
                                    @if(session()->get('language') == 'spanish') Fecha @else Date @endif
                                </th>
                                <th class="col-md-3">
                                    @if(session()->get('language') == 'spanish') Total @else Total @endif
                                </th>
                                <th class="col-md-3">
                                    @if(session()->get('language') == 'spanish') Pago @else Payment @endif
                                </th>
                                <th class="col-md-2">
                                    @if(session()->get('language') == 'spanish') Factura @else Invoice @endif
                                </th>
                                <th class="col-md-1">
                                    @if(session()->get('language') == 'spanish') Razón de la orden @else Order Reason @endif
                                </th>
                                <th class="col-md-2">
                                    @if(session()->get('language') == 'spanish') Estado de la orden @else Order Status @endif
                                </th>
                            </tr>
                            @foreach($orders as $order)
                                <tr>
                                    <td class="col-md-1">
                                       {{ $order->order_date }}
                                    </td>
                                    <td class="col-md-3">
                                       ${{ $order->amount }}
                                    </td>
                                    <td class="col-md-3">
                                        {{ $order->payment_method }}
                                    </td>
                                    <td class="col-md-2">
                                         {{ $order->invoice_no }}
                                    </td>
                                    <td class="col-md-1">
                                        {{ $order->return_reason }}
                                    </td>
                                    <td class="col-md-2">
                                        @if($order->status == 'returned')
                                            <span class="badge badge-pill badge-warning" style="background: #418DB9;">{{ $order->status }} </span>
                                        @else
                                            <span class="badge badge-pill badge-warning" style="background: #418DB9;">{{ $order->status }} </span>
                                            <span class="badge badge-pill badge-warning" style="background: red;">Return Requested </span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $orders->links() }}
                    </div>
                </div> <!-- / end col md 8 -->
            </div>
        </div>
        <br><br><br><br>
    </div>
@endsection


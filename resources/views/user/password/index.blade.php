@extends('layouts.frontend.master')
@section('title', "Change Password")
@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                @include('frontend.partials.user_sidebar')
                <div class="col-md-6">
                    <div class="card">
                        <h3 class="text-center">
                            @if(session()->get('language') == 'spanish') Cambia Tu Contraseña @else Change Your Password @endif
                        </h3>
                        <div class="card-body">
                            <form method="POST" action="{{ route('user.password.update', $user->id) }}">
                                @method('PUT')
                                @csrf

                                <div class="form-group">
                                    <label class="info-title" for="old_password">
                                        @if(session()->get('language') == 'spanish') Contraseña actual @else Current Password @endif
                                    </label>
                                    <input type="password" name="old_password" placeholder="Current Password" class="form-control" id="old_password" required>
                                    <strong class="text-danger">{{ $errors->first('old_password') }}</strong>
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="new_password">
                                        @if(session()->get('language') == 'spanish') Nueva contraseña @else New Password @endif
                                    </label>
                                    <input type="password" name="new_password" placeholder="New Password" class="form-control" id="new_password" required>
                                    <strong class="text-danger">{{ $errors->first('new_password') }}</strong>
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="password_confirmation">
                                        @if(session()->get('language') == 'spanish') Confirmar Contraseña @else Confirm Password @endif
                                    </label>
                                    <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control" id="password_confirmation" required>
                                    <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger">
                                        @if(session()->get('language') == 'spanish') Actualizar @else Update @endif
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end of col md 6 -->
            </div> <!-- end of row -->
        </div>
        <br><br><br><br>
    </div>
@endsection

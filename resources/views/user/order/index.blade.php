@extends('layouts.frontend.master')
@section('title', "Orders")
@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                @include('frontend.partials.user_sidebar')
                <div class="col-md-8">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr style="background: #e2e2e2;">
                                    <th class="col-md-1">
                                        @if(session()->get('language') == 'spanish') Fecha @else Date @endif
                                    </th>
                                    <th class="col-md-3">
                                        @if(session()->get('language') == 'spanish') Total @else Total @endif
                                    </th>
                                    <th class="col-md-3">
                                        @if(session()->get('language') == 'spanish') pago @else Payment @endif
                                    </th>
                                    <th class="col-md-2">
                                        @if(session()->get('language') == 'spanish') Factura @else Invoice @endif
                                    </th>
                                    <th class="col-md-2">
                                        @if(session()->get('language') == 'spanish') Pedido @else Order @endif
                                    </th>
                                    <th class="col-md-1">
                                        @if(session()->get('language') == 'spanish') Acción @else Action @endif
                                    </th>
                                </tr>
                            @foreach($orders as $order)
                                @if($order->status !== 'cancelled')
                                <tr>
                                    <td class="col-md-1">
                                        {{ $order->order_date }}
                                    </td>
                                    <td class="col-md-3">
                                       ${{ $order->amount }}
                                    </td>
                                    <td class="col-md-3">
                                       {{ $order->payment_method }}
                                    </td>
                                    <td class="col-md-2">
                                        {{ $order->invoice_no }}
                                    </td>
                                    <td class="col-md-2">
                                        @if($order->status == 'pending')
                                            <span class="badge badge-pill badge-warning" style="background: #800080;">Pending</span>
                                        @elseif($order->status == 'confirmed')
                                            <span class="badge badge-pill badge-warning" style="background: #0000FF;">Confirmed</span>

                                        @elseif($order->status == 'processing')
                                            <span class="badge badge-pill badge-warning" style="background: #FFA500;">Processing</span>

                                        @elseif($order->status == 'picked')
                                            <span class="badge badge-pill badge-warning" style="background: #808000;">Picked</span>

                                        @elseif($order->status == 'shipped')
                                            <span class="badge badge-pill badge-warning" style="background: #808080;">Shipped</span>

                                        @elseif($order->status == 'delivered')
                                            @if($order->return_date !== NULL)
                                                <span class="badge badge-pill badge-warning" style="background: #008000;">Delivered</span>
                                                <span class="badge badge-pill badge-warning" style="background: red;">Return Requested </span>
                                            @else
                                                <span class="badge badge-pill badge-warning" style="background: #008000;">Delivered</span>
                                            @endif

                                        @elseif($order->status == 'returned')
                                            <span class="badge badge-pill badge-warning" style="background: #0c85d0;">Returned</span>

                                        @elseif($order->status == 'cancelled')
                                            <span class="badge badge-pill badge-warning" style="background: #FF0000;">Cancelled</span>
                                        @endif
                                    </td>
                                    <td class="col-md-1">
                                        <a href="{{ route('user.order.show', $order->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i>
                                            @if(session()->get('language') == 'spanish') Vista @else View @endif
                                        </a>
                                        <a href="{{ route('user.order.create_invoice', $order->id) }}"
                                           style="margin-top: 5px;"
                                           class="btn btn-sm btn-danger">
                                            <i class="fa fa-download mt-5"style="color: white;"></i>
                                            @if(session()->get('language') == 'spanish') Factura @else Invoice @endif
                                        </a>
                                        @if($order->status == 'pending' || $order->status == 'confirmed')
                                            <form method="POST" action="{{ route('user.cancel.update', $order->id) }}">
                                                @csrf
                                                @method('PUT')
                                                <button class="btn btn-sm btn-warning" type="submit"><i class="fa fa-ban"></i>
                                                    @if(session()->get('language') == 'spanish') Cancelar @else Cancel @endif
                                                </button>
                                            </form>
                                            @else
                                        @endif
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        {{ $orders->links() }}
                    </div>
                </div> <!-- / end col md 8 -->
            </div>
        </div>
        <br><br><br><br>
    </div>
@endsection

@extends('layouts.frontend.master')
@section('title', "View Order")
@section('content')
    <div class="body-content">
        <div class="container-fluid">
            <div class="row">
                @include('frontend.partials.user_sidebar')
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                @if(session()->get('language') == 'spanish') Detalles de envío : @else Shipping Details @endif
                            </h4>
                        </div>
                        <hr>
                        <div class="card-body" style="background: #E9EBEC;">
                            <table class="table">
                                <tr>
                                    <th> @if(session()->get('language') == 'spanish') Nombre de envío : @else Shipping Name : @endif </th>
                                    <td class="text-bold"> {{ $order->name }} </td>
                                </tr>
                                <tr>
                                    <th>@if(session()->get('language') == 'spanish') Teléfono de envío : @else Shipping Phone : @endif</th>
                                    <th> {{ $order->phone }} </th>
                                </tr>
                                <tr>
                                    <th> @if(session()->get('language') == 'spanish') Correo electrónico de envío : @else Shipping Email : @endif </th>
                                    <th> {{ $order->email }} </th>
                                </tr>
                                <tr>
                                    <th>@if(session()->get('language') == 'spanish') Estado : @else State : @endif </th>
                                    <th>{{ $order->state }} </th>
                                </tr>
                                <tr>
                                    <th> @if(session()->get('language') == 'spanish') Código postal : @else Zip Code : @endif </th>
                                    <th> {{ $order->zip_code }} </th>
                                </tr>
                                <tr>
                                    <th> @if(session()->get('language') == 'spanish') Fecha de orden : @else Order Date : @endif </th>
                                    <th> {{ $order->order_date }} </th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div> <!-- // end col md -5 -->

                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header">
                            <h4>@if(session()->get('language') == 'spanish') Detalles del pedido  @else Order Details @endif
                                <span class="text-danger"> Invoice : {{ $order->invoice_no }}</span>
                            </h4>
                        </div>
                        <hr>
                        <div class="card-body" style="background: #E9EBEC;">
                            <table class="table">
                                <tr>
                                    <th>@if(session()->get('language') == 'spanish') nombre : @else Name : @endif</th>
                                    <th> {{ $order->name }} </th>
                                </tr>
                                <tr>
                                    <th> @if(session()->get('language') == 'spanish') Teléfono : @else Phone : @endif</th>
                                    <th> {{ $order->phone }} </th>
                                </tr>
                                <tr>
                                    <th> @if(session()->get('language') == 'spanish') Tipo de pago : @else Payment Type : @endif </th>
                                    <th> {{ $order->payment_method }} </th>
                                </tr>
                                <tr>
                                    <th> Tranx ID : </th>
                                    <th> {{ $order->transaction_id }} </th>
                                </tr>
                                <tr>
                                    <th> @if(session()->get('language') == 'spanish') Factura : @else Invoice : @endif </th>
                                    <th class="text-danger"> {{ $order->invoice_no }} </th>
                                </tr>
                                <tr>
                                    <th> @if(session()->get('language') == 'spanish') Total del pedido : @else Order Total : @endif </th>
                                    <th>${{ $order->amount }} </th>
                                </tr>
                                <tr>
                                    <th> @if(session()->get('language') == 'spanish') Pedido : @else Order : @endif </th>
                                    <th>
                                        <span class="badge badge-pill badge-warning" style="background: #418DB9;">{{ $order->status }} </span>
                                    </th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div> <!-- // 2ND end col md -5 -->
            </div> <!-- // End of row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr style="background: #e2e2e2;">
                                    <th>@if(session()->get('language') == 'spanish') Imagen @else Image @endif</th>
                                    <th>@if(session()->get('language') == 'spanish') Nombre del producto @else Product Name @endif</th>
                                    <th>@if(session()->get('language') == 'spanish') Código de producto @else Product Code @endif</th>
                                    <th>@if(session()->get('language') == 'spanish') Color @else Color @endif</th>
                                    <th>@if(session()->get('language') == 'spanish') Tamaño @else Size @endif</th>
                                    <th>@if(session()->get('language') == 'spanish') Cantidad @else Quantity @endif</th>
                                    <th>@if(session()->get('language') == 'spanish') precio @else Price @endif</th>
                                </tr>
                                @foreach($orderItems as $item)
                                    <tr>
                                        <td>
                                            @if(file_exists('uploads/product_thumbnail/' . $item->product->id ."/" . $item->product->product_thumbnail))
                                                <img src="{{ asset('uploads/product_thumbnail/' . $item->product->id ."/" . $item->product->product_thumbnail) }}"
                                                     style="width:50px; height: 50px;"
                                                     alt="product image">

                                            @else
                                                <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $item->product->id ."/" . $item->product->product_thumbnail)}}"
                                                     style="width:50px; height: 50px;"
                                                     alt="product image">
                                            @endif
                                        </td>
                                        <td>{{ $item->product->product_name_en }}</td>
                                        <td>{{ $item->productExtension->product_code }}</td>
                                        <td>
                                            @if($item->product->product_color_en == NULL)
                                                ----
                                            @else
                                                {{ $item->color }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->product->product_size_en == NULL)
                                                ----
                                            @else
                                                {{ $item->size  }}
                                            @endif
                                        </td>
                                        <td>{{ $item->qty }}</td>
                                        <td>${{ $item->price }}  ( $ {{ $item->price * $item->qty}} )</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div> <!-- / end col md 12 -->
            </div> <!-- // End of order item row -->
            @if($order->status !== "delivered")

            @else
                @if($order->return_date == NULL)
                    <form action="{{ route('user.return.update', $order->id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="label">Reason For Return:</label>
                            <textarea name="return_reason" id="" class="form-control" cols="30" rows="05">Reason for return...</textarea>
                        </div>
                        <button type="submit" class="btn btn-danger">Submit</button><br><br>
                    </form>
                @else
                    <span class="badge badge-pill badge-warning" style="background: red">You have sent a return request for this product</span>
                @endif
            @endif
        </div>
    </div>
@endsection

@extends('layouts.frontend.master')
@section('title', "User Profile")
@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                @include('frontend.partials.user_sidebar')
                <div class="col-md-6">
                    <div class="card">
                        <h3 class="text-center">
                            <span class="text-danger">
                                @if(session()->get('language') == 'spanish') Hola.... @else Hello.... @endif
                            </span>
                            <strong>{{ $user->name }}</strong>
                            @if(session()->get('language') == 'spanish') Actualiza tu perfil @else Update your Profile @endif
                        </h3>
                        <div class="card-body">
                            <form method="POST" action="{{ route('user.profile.update', $user->id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="info-title" for="name">
                                        @if(session()->get('language') == 'spanish') Nombre @else Name @endif
                                    </label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Ebuyz Cartz..." value="{{ $user->name }}">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="email">
                                        @if(session()->get('language') == 'spanish') Dirección de correo electrónico @else Email Address @endif
                                    </label>
                                    <input type="email" name="email" placeholder="ebuyz@gmail.com..." class="form-control" id="email" value="{{ $user->email }}">
                                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="phone_number">
                                        @if(session()->get('language') == 'spanish') Número de teléfono @else Phone Number @endif
                                    </label>
                                    <input placeholder="4073336666..." type="number" name="phone_number" class="form-control" id="phone_number" value="{{ $user->phone_number }}">
                                    <strong class="text-danger">{{ $errors->first('phone_number') }}</strong>
                                </div>


                                <div class="form-group">
                                    <label class="info-title" for="address">
                                        @if(session()->get('language') == 'spanish') Dirección @else Address @endif
                                    </label>
                                    <input type="text" placeholder="123 Ebuyz St. ..." name="address" class="form-control" id="address" value="{{ $user->address }}">
                                    <strong class="text-danger">{{ $errors->first('address') }}</strong>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="state">
                                        @if(session()->get('language') == 'spanish') Estado @else State @endif
                                    </label>
                                    <input type="text" name="state" placeholder="FL..." class="form-control" id="state" value="{{ $user->state }}"  autocomplete="state" autofocus>
                                    <strong class="text-danger">{{ $errors->first('state') }}</strong>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="city">
                                        @if(session()->get('language') == 'spanish') Ciudad @else City @endif
                                    </label>
                                    <input type="text" name="city" placeholder="Orlando..." class="form-control" id="city" value="{{ $user->city }}">
                                    <strong class="text-danger">{{ $errors->first('city') }}</strong>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="zip_code">
                                        @if(session()->get('language') == 'spanish') Código postal @else Zip Code @endif
                                    </label>
                                    <input placeholder="32822..." type="number" name="zip_code" class="form-control" id="zip_code" value="{{ $user->zip_code }}">
                                    <strong class="text-danger">{{ $errors->first('zip_code') }}</strong>
                                </div>


                                <div class="form-group">
                                    <label class="info-title" for="avatar">
                                        @if(session()->get('language') == 'spanish') Imagen de perfil de usuario @else User Profile Image @endif
                                    </label>
                                    <div class="controls">
                                        <input type="file" name="avatar" class="form-control">
                                    </div>
                                    <strong class="text-danger">{{ $errors->first('avatar') }}</strong>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger">
                                        @if(session()->get('language') == 'spanish') Actualizar @else Update @endif
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end of col md 6 -->
            </div> <!-- end of row -->
        </div>
        <br><br><br><br>
    </div>
@endsection

@extends('layouts.frontend.master')
@section('title', "User Profile")
@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                @include('frontend.partials.user_sidebar')
                <h4 class="text-center text-white bg-danger">{{ $message }}</h4>
                <div class="col-md-6">
                    <div class="card">
                        <h3 class="text-center">
                            <span class="text-danger">
                                @if(session()->get('language') == 'spanish') Hola.... @else Hello.... @endif
                            </span>
                            <strong>
                                {{ $user->name }}
                            </strong>  @if(session()->get('language') == 'spanish') Bienvenido a E-BUYZ!!! @else Welcome to E-BUYZ!!! @endif
                        </h3>
                    </div>
                </div><!-- end of col md 6 -->
            </div> <!-- end of row -->
        </div>
        <br><br><br><br>
    </div>
@endsection

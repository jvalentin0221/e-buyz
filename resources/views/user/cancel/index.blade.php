@extends('layouts.frontend.master')
@section('title', "Cancelled Orders")
@section('content')
    <div class="body-content">
        <div class="container">
            <div class="row">
                @include('frontend.partials.user_sidebar')
                <div class="col-md-8">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr style="background: #e2e2e2;">
                                <td class="col-md-1">
                                    <label for="">
                                        @if(session()->get('language') == 'spanish') Fecha @else Date @endif
                                    </label>
                                </td>
                                <td class="col-md-3">
                                    <label for="">
                                        @if(session()->get('language') == 'spanish') Total @else Total @endif
                                    </label>
                                </td>
                                <td class="col-md-3">
                                    <label for="">
                                        @if(session()->get('language') == 'spanish') pago @else Payment @endif
                                    </label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">
                                        @if(session()->get('language') == 'spanish') Factura @else Invoice @endif
                                    </label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">
                                        @if(session()->get('language') == 'spanish') Pedido @else Order @endif
                                    </label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">
                                        @if(session()->get('language') == 'spanish') fecha de cancelación @else Date of cancellation @endif
                                    </label>
                                </td>
                                <td class="col-md-1">
                                    <label for="">
                                        @if(session()->get('language') == 'spanish') Acción @else Action @endif
                                    </label>
                                </td>
                            </tr>
                            @foreach($orders as $order)
                                <tr>
                                    <td class="col-md-1">
                                        <label for=""> {{ $order->order_date }}</label>
                                    </td>
                                    <td class="col-md-3">
                                        <label for=""> ${{ $order->amount }}</label>
                                    </td>
                                    <td class="col-md-3">
                                        <label for=""> {{ $order->payment_method }}</label>
                                    </td>
                                    <td class="col-md-2">
                                        <label for=""> {{ $order->invoice_no }}</label>
                                    </td>
                                    <td class="col-md-2">
                                        <label for="">
                                            <span class="badge badge-pill badge-warning" style="background: #418DB9;">{{ $order->status }} </span>
                                        </label>
                                    </td>
                                    <td class="col-md-2">
                                        <label for=""> {{ \Carbon\Carbon::parse($order->cancel_date)->format('F j, Y') }}</label>
                                    </td>
                                    <td class="col-md-1">
                                        <a href="{{ route('user.order.show', $order->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a>
                                        <a href="{{ route('user.order.create_invoice', $order->id) }}"
                                           style="margin-top: 5px;"
                                           class="btn btn-sm btn-danger">
                                            <i class="fa fa-download mt-5"style="color: white;"></i>
                                            Invoice
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $orders->links() }}
                    </div>
                </div> <!-- / end col md 8 -->
            </div>
        </div>
        <br><br><br><br>
    </div>
@endsection

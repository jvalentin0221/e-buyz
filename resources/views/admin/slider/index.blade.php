@extends('layouts.admin.master')
@section('title', "Slider")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Slider List</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Slider Image</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sliders as $item)
                                        <tr>
                                            <td>
                                                @if(!empty($item->slider_img))
                                                    @if(file_exists('uploads/slider_img/' . $item->id ."/" . $item->slider_img))
                                                        <img src="{{ asset('uploads/slider_img/' . $item->id . "/" . $item->slider_img) }}"
                                                             alt="brand image"
                                                             style="width:80px; height: 70px;">
                                                    @else
                                                        <img src="{{ Storage::disk('s3')->url('slider_img/' . $item->id ."/" . $item->slider_img)}}"
                                                             style="width:80px; height: 70px;"
                                                             alt="brand image">
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->title == NULL)
                                                    <span class="badge badge-pill badge-danger"> No Title </span>
                                                @else
                                                    {{ $item->title }}
                                                @endif
                                            </td>

                                            <td>{{ $item->description }}</td>
                                            <td>
                                                @if($item->status == 1)
                                                    <span class="badge badge-pill badge-success">Active</span>
                                                @else
                                                    <span class="badge badge-pill badge-danger">InActive</span>
                                                @endif
                                            </td>
                                            <td>
                                                <form class="{{'button' . $item->id }}" method="POST" action="{{ route('sliders.destroy', $item->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a href="{{ route('sliders.edit', $item->id) }}" class="btn btn-info" title="Edit slider"><i class="fa fa-pencil"></i></a>
                                                    <button id="{{'button' . $item->id }}" type="submit" class="btn btn-danger delete" title="Delete slider"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $sliders->links() }}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
                <!-- Add slider column -->
                <div class="col-lg-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Slider</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <form method="POST" action="{{ route('sliders.store',auth()->user()->id) }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label class="text-bold" for="title">Slider Title<span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id=title type="text" name="title" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('title') }}</strong>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-bold" for="description">Slider Description<span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id=description type="text" name="description" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('description') }}</strong>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-bold" for="slider_img">Slider Image<span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id=slider_img type="file" name="slider_img" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('slider_img') }}</strong>
                                    </div>
                                    <div>
                                        <input type="submit" class="btn btn-rounded btn-primary" value="Add Slider">
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

@extends('layouts.admin.master')
@section('title', "Update Slider")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Edit Slider</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <form method="POST" action="{{ route('sliders.update', $slider->id) }}" enctype="multipart/form-data">
                                        @method('PUT')
                                        @csrf
                                        <div class="form-group">
                                            <h5 class="">Activate Slider</h5>
                                            <div class="controls">
                                                <fieldset class="mb-5">
                                                    <input type="hidden" name="status" value="0">
                                                    <input type="checkbox" id="status" name="status" value="1" {{ $slider->status == 1 ? 'checked': '' }}>
                                                    <label for="status">Active</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="text-bold" for="title">Slider Title<span class="text-danger"></span></label>
                                            <div class="controls">
                                                <input class="form-control" id="title" type="text" name="title" value="{{ $slider->title }}">
                                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="text-bold" for="description">Slider Description<span class="text-danger"></span></label>
                                            <div class="controls">
                                                <input class="form-control" id="description" type="text" name="description" value="{{ $slider->description }}">
                                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="text-bold" for="slider_img">Slider Image<span class="text-danger"></span></label>
                                            <div class="controls">
                                                <input class="form-control" id="slider_img" type="file" name="slider_img" value="{{ $slider->slider_img }}">
                                                <strong class="text-danger">{{ $errors->first('slider_img') }}</strong>
                                            </div>
                                        </div>
                                        <div>
                                            <input type="submit" class="btn btn-rounded btn-primary" value="Update Slider">
                                        </div>
                                    </form>
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

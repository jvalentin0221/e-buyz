<header class="main-header">
    <nav class="navbar navbar-static-top pl-30">
        <div>
            <ul class="nav">
                <li class="btn-group nav-item">
                    <a href="#" class="waves-effect waves-light nav-link rounded svg-bt-icon" data-toggle="push-menu" role="button">
                        <i class="nav-link-icon mdi mdi-menu"></i>
                    </a>
                </li>
                <li class="btn-group nav-item">
                    <a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link rounded svg-bt-icon" title="Full Screen">
                        <i class="nav-link-icon mdi mdi-crop-free"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="navbar-custom-menu r-side">
            <ul class="nav navbar-nav">
                <!-- User Account-->
                <li class="dropdown user user-menu">
                    <a href="#" class="waves-effect waves-light rounded dropdown-toggle p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropdownMenuButton"
                    data-target="#avatarDrop">
                        @if(!empty(auth()->user()->avatar))
                            @if(file_exists('uploads/avatar/' . auth()->user()->id ."/" . auth()->user()->avatar))
                                <img class="rounded-circle navbar__img d-xl-inline-block"
                                     src="{{ asset('uploads/avatar/' . auth()->user()->id ."/" . auth()->user()->avatar) }}"
                                     alt="admin image">
                            @else
                                <img class="rounded-circle navbar__img d-xl-inline-block"
                                     src="{{ Storage::disk('s3')->url('avatar/' . auth()->user()->id ."/" . auth()->user()->avatar ) }}"
                                     alt="admin image">
                            @endif
                        @else
                                <img class="rounded-circle navbar__img d-xl-inline-block"
                                 src="{{ asset('assets/images/user_placeholder.png') }}"
                                 alt="admin placeholder image">
                        @endif
                    </a>
                    <ul class="dropdown-menu animated flipInX" aria-labelledby="dropdownMenuButton" id="avatarDrop">
                        <li class="user-body">
                            <a class="dropdown-item" href="{{ route('index') }}"><i class="fa fa-home text-muted mr-2"></i> Home</a>
                            <a class="dropdown-item" href="{{ route('admin.profile', auth()->user()->id) }}"><i class="ti-user text-muted mr-2"></i> Profile</a>
                            <a class="dropdown-item" href="{{ route('admin.password.index', auth()->user()->id ) }}"><i class="ti-wallet text-muted mr-2"></i> Change Password
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('admin.logout') }}"><i class="ti-lock text-muted mr-2"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

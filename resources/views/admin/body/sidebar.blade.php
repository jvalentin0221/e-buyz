<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <div class="user-profile">
            <div class="ulogo">
                <a href="/">
                    <!-- logo for regular state and mobile devices -->
                    <div class="d-flex align-items-center justify-content-center">
                        @if(isset(cache('sliderCompanyInfo')['companyInfo']))
                            @if(file_exists('uploads/logo/' . cache('sliderCompanyInfo')['companyInfo']['id'] ."/" . cache('sliderCompanyInfo')['companyInfo']['logo']))
                                <img src="{{ asset('uploads/logo/' .  cache('sliderCompanyInfo')['companyInfo']['id'] . '/' . cache('sliderCompanyInfo')['companyInfo']['logo']) }}"
                                     alt="logo"
                                     style="width:80px; height: 70px;">
                            @else
                                <img src="{{ asset('assets/images/logo/e-buyz-logo.png')}}"
                                     alt="logo">
                            @endif
                        @else
                            <img src="{{ asset('assets/images/logo/e-buyz-logo.png')}}"
                                 alt="e-buyz logo">
                        @endif
                        <h3><b>E-Buyz</b> Admin</h3>
                    </div>
                </a>
            </div>
        </div>

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="{{ Route::current()->getName() == 'admin.dashboard' ? 'active' : '' }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i data-feather="pie-chart"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview {{ Route::current()->getName() == 'admin.brand.index' ? 'active' : '' }}">
                <a href="#">
                    <span>Brands</span>
                    <span class="pull-right-container">
                         <i class="fa fa-angle-right pull-right"></i>
                   </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('brands.index') }}"><i class="ti-more"></i>All Brands</a></li>
                </ul>
            </li>

            <li class="treeview {{ Route::current()->getName() == 'admin.category.index' ? 'active' : '' }}">
                <a href="#">
                    <span>Category</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('categories.index') }}"><i class="ti-more"></i>All Categories</a></li>
                    <li><a href="{{ route('subcategories.index') }}"><i class="ti-more"></i>All Sub Categories</a></li>
                    <li><a href="{{ route('subsubcategories.index') }}"><i class="ti-more"></i>All Sub->SubCategory</a></li>
                </ul>
            </li>

            <li class="treeview {{ Route::current()->getName() == 'admin.product.index' ? 'active' : '' }}">
                <a href="#">
                    <span>Products</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.product.index') }}"><i class="ti-more"></i>Add Product</a></li>
                    <li><a href="{{ route('manage-products.index') }}"><i class="ti-more"></i>Manage Products</a></li>
                </ul>
            </li>

            <li class="treeview {{ Route::current()->getName() == 'admin.slider.index' ? 'active' : '' }}">
                <a href="#">
                    <span>Slider</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('sliders.index') }}"><i class="ti-more"></i>Manage Slider</a></li>
                </ul>
            </li>

            <li class="treeview {{ Route::current()->getName() == 'admin.coupon.index' ? 'active' : '' }}">
                <a href="#">
                    <span>Coupon</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('coupons.index') }}"><i class="ti-more"></i>Manage Coupons</a></li>
                </ul>
            </li>

            <li class="treeview {{ Route::current()->getName() == 'admin.order.index' ? 'active' : '' }}">
                <a href="#">
                    <span>Orders</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.order.index') }}"><i class="ti-more"></i>Pending Orders</a></li>
                    <li><a href="{{ route('admin.order.confirmed_orders') }}"><i class="ti-more"></i>Confirmed Orders</a></li>
                    <li><a href="{{ route('admin.order.processing_orders') }}"><i class="ti-more"></i>Processing Orders</a></li>
                    <li><a href="{{ route('admin.order.picked_orders') }}"><i class="ti-more"></i>Picked Orders</a></li>
                    <li><a href="{{ route('admin.order.shipped_orders') }}"><i class="ti-more"></i>Shipped Orders</a></li>
                    <li><a href="{{ route('admin.order.delivered_orders') }}"><i class="ti-more"></i>Delivered Orders</a></li>
                    <li><a href="{{ route('admin.order.returned_orders') }}"><i class="ti-more"></i>Returned Orders</a></li>
                    <li><a href="{{ route('admin.order.cancel_orders') }}"><i class="ti-more"></i>Cancelled Orders</a></li>
                </ul>
            </li>

            <li class="treeview {{ Route::current()->getName() == 'admin.company_info.edit' ? 'active' : '' }}">
                <a href="#">
                    <span>Company Info</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.company_info.edit', 1) }}"><i class="ti-more"></i>Edit Company Info</a></li>
                </ul>
            </li>

            <li class="treeview {{ Route::current()->getName() == 'admin.review.index' ? 'active' : '' }}">
                <a href="#">
                    <span>Review</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.review.index') }}"><i class="ti-more"></i>Pending Reviews</a></li>
                    <li><a href="{{ route('admin.publish_review.index') }}"><i class="ti-more"></i>Published Reviews</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>

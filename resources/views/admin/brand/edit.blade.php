@extends('layouts.admin.master')
@section('title', "Update Brand")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="container">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Edit Brand</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <form method="POST" action="{{ route('brands.update', $brand->id) }}" enctype="multipart/form-data">
                                        @method('PUT')
                                        @csrf

                                        <div class="form-group">
                                            <h5>Brand Name English <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="brand_name_en" class="form-control" value="{{ $brand->brand_name_en }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('brand_name_en') }}</strong>
                                        </div>

                                        <div class="form-group">
                                            <h5>Brand Name Spanish <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="brand_name_es" class="form-control" value="{{ $brand->brand_name_es }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('brand_name_es') }}</strong>
                                        </div>

                                        <div class="form-group">
                                            <h5>Brand URL <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="brand_url" class="form-control" value="{{ $brand->brand_url }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('brand_url') }}</strong>
                                        </div>

                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <h5>Brand Image <span class="text-danger">*</span></h5>
                                                        <div class="controls">
                                                            <input id="image" type="file" name="brand_image" class="form-control">
                                                        </div>
                                                        <strong class="text-danger">{{ $errors->first('brand_image') }}</strong>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        @if(!empty($brand->brand_image))
                                                            @if(file_exists('uploads/brand_image/' . $brand->id ."/" . $brand->brand_image))
                                                                <img class="rounded-circle d-xl-inline-block"
                                                                     id="showImage"
                                                                     style="width: 100px; height: 100px"
                                                                     src="{{ asset('uploads/brand_image/' . $brand->id ."/" . $brand->brand_image) }}"
                                                                     alt="brand image">
                                                            @else
                                                                <img class="rounded-circle d-xl-inline-block"
                                                                     id="showImage"
                                                                     style="width: 100px; height: 100px"
                                                                     src="{{ Storage::disk('s3')->url('brand_image/' . $brand->id ."/" . $brand->brand_image)  }}"
                                                                     alt="brand image">
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="brand_slug_en" value="brand">
                                        <input type="hidden" name="brand_slug_es" value="brand">
                                        <div>
                                            <input type="submit" class="btn btn-rounded btn-primary" value="Edit Brand">
                                        </div>
                                    </form>
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

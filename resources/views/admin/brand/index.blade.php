@extends('layouts.admin.master')
@section('title', "Brands")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Brand List</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Brand En</th>
                                        <th>Brand ES</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($brands as $brand)
                                    <tr>
                                        <td>{{ $brand->brand_name_en }}</td>
                                        <td>{{ $brand->brand_name_es }}</td>
                                        <td>
                                            @if(!empty($brand->brand_image))
                                                @if(file_exists('uploads/brand_image/' . $brand->id ."/" . $brand->brand_image))
                                                    <img src="{{ asset('uploads/brand_image/' . $brand->id. "/" . $brand->brand_image) }}"
                                                         alt="brand image"
                                                         style="width:80px; height: 70px;">
                                                @else
                                                    <img src="{{ Storage::disk('s3')->url('brand_image/' . $brand->id ."/" . $brand->brand_image)}}"
                                                         style="width:80px; height: 70px;"
                                                         alt="brand image">
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <form class="{{'button' . $brand->id }}" method="POST" action="{{ route('brands.destroy', $brand->id) }}">
                                                @method('DELETE')
                                                @csrf
                                                <a href="{{ route('brands.edit', $brand->id) }}" class="btn btn-info" title="Edit Brand"><i class="fa fa-pencil"></i></a>
                                                <button id="{{'button' . $brand->id }}" type="submit" class="btn btn-danger delete" title="Delete Brand"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $brands->links() }}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
                <!-- Add brand column -->
                <div class="col-lg-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Brand</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <form method="POST" action="{{ route('brands.store',auth()->user()->id) }}" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group">
                                        <h5>Brand Name English <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="brand_name_en" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('brand_name_en') }}</strong>
                                    </div>

                                    <div class="form-group">
                                        <h5>Brand Name Spanish <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="brand_name_es" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('brand_name_es') }}</strong>
                                    </div>

                                    <div class="form-group">
                                        <h5>Brand URL <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="brand_url" class="form-control" placeholder="example.com">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('brand_url') }}</strong>
                                    </div>

                                    <div class="form-group">
                                        <h5>Brand Image <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="file" name="brand_image" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('brand_image') }}</strong>
                                    </div>
                                    <input type="hidden" name="brand_slug_en" value="brand">
                                    <input type="hidden" name="brand_slug_es" value="brand">
                                    <div>
                                        <input type="submit" class="btn btn-rounded btn-primary" value="Add Brand">
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

@extends('layouts.admin.master')
@section('title', "Categories")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Category List</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Category En</th>
                                        <th>Category ES</th>
                                        <th>Category Icon</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $item)
                                        <tr>
                                            <td>{{ $item->category_name_en }}</td>
                                            <td>{{ $item->category_name_es }}</td>
                                            {{--URL or class of icon--}}
                                            {{--Icons available at Font Awesome 4.--}}
                                            <td><span><i class="{{ $item->category_icon }} fa-3x"></i></span></td>
                                            <td>
                                                <form class="{{'button' . $item->id }}" method="POST" action="{{ route('categories.destroy', $item->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a href="{{ route('categories.edit', $item->id) }}" class="btn btn-info" title="Edit Category"><i class="fa fa-pencil"></i></a>
                                                    <button id="{{'button' . $item->id }}" type="submit" class="btn btn-danger delete" title="Delete Category"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $categories->links() }}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
                <!-- Add category column -->
                <div class="col-lg-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Category</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <form method="POST" action="{{ route('categories.store',auth()->user()->id) }}">
                                    @csrf

                                    <div class="form-group">
                                        <h5>Category Name English <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="category_name_en" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('category_name_en') }}</strong>
                                    </div>

                                    <div class="form-group">
                                        <h5>Category Name Spanish <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="category_name_es" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('category_name_es') }}</strong>
                                    </div>

                                    <div class="form-group">
                                    {{--URL or class of icon--}}
                                        <h5>Category Icon <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="category_icon" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('category_icon') }}</strong>
                                    </div>
                                    <input type="hidden" name="category_slug_en" value="category">
                                    <input type="hidden" name="category_slug_es" value="category">
                                    <div>
                                        <input type="submit" class="btn btn-rounded btn-primary" value="Add Category">
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

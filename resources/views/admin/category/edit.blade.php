@extends('layouts.admin.master')
@section('title', "Update Category")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="container">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Edit Category</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <form method="POST" action="{{ route('categories.update', $category->id) }}">
                                        @method('PUT')
                                        @csrf

                                        <div class="form-group">
                                            <h5>Category Icon <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input id="image" type="text" name="category_icon" class="form-control" value="{{ $category->category_icon }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('category_icon') }}</strong>
                                        </div>

                                        <div class="form-group">
                                            <h5>Category Name English <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="category_name_en" class="form-control" value="{{ $category->category_name_en }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('category_name_en') }}</strong>
                                        </div>

                                        <div class="form-group">
                                            <h5>Category Name Spanish <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="category_name_es" class="form-control" value="{{ $category->category_name_es }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('category_name_es') }}</strong>
                                        </div>
                                        <input type="hidden" name="category_slug_en" value="category">
                                        <input type="hidden" name="category_slug_es" value="category">
                                        <div>
                                            <input type="submit" class="btn btn-rounded btn-primary" value="Edit Category">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

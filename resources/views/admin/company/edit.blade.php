@extends('layouts.admin.master')
@section('title', "Edit Company Info")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Edit Company Info -->
                <div class="col-md-10">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Company Info</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <form method="POST" action="{{ route('admin.company_info.update', $company_info->id)}}" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold" for="logo">Logo</label>
                                            <div class="controls">
                                                <input
                                                    id="logo"
                                                    type="file"
                                                    name="logo"
                                                    class="form-control"
                                                    value="{{ $company_info->slider_img }}">
                                                <strong class="text-danger">{{ $errors->first('logo') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="email">Email</label>
                                            <div class="controls">
                                                <input
                                                    id="email"
                                                    type="email"
                                                    name="email"
                                                    class="form-control"
                                                    value="{{ $company_info->email }}">
                                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="company_name">Company Name</label>
                                            <div class="controls">
                                                <input
                                                    id="company_name"
                                                    type="text"
                                                    name="company_name"
                                                    class="form-control"
                                                    value="{{ $company_info->company_name }}">
                                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="company_name">Company Address</label>
                                            <div class="controls">
                                                <input
                                                    id="company_address"
                                                    type="text"
                                                    name="company_address"
                                                    class="form-control"
                                                    value="{{ $company_info->company_address }}">
                                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="phone_number_one">Phone Number One</label>
                                            <div class="controls">
                                                <input
                                                    id="phone_number_one"
                                                    type="number"
                                                    name="phone_number_one"
                                                    class="form-control"
                                                    value="{{ $company_info->phone_number_one }}">
                                                <strong class="text-danger">{{ $errors->first('phone_number_one') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="phone_number_two">Phone Number Two</label>
                                            <div class="controls">
                                                <input
                                                    id="phone_number_two"
                                                    type="number"
                                                    name="phone_number_two"
                                                    class="form-control"
                                                    value="{{ $company_info->phone_number_two }}">
                                                <strong class="text-danger">{{ $errors->first('phone_number_two') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="facebook">Facebook</label>
                                            <div class="controls">
                                                <input
                                                    id="facebook"
                                                    type="text"
                                                    name="facebook"
                                                    class="form-control"
                                                    value="{{ $company_info->facebook }}">
                                                <strong class="text-danger">{{ $errors->first('facebook') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="linkedin">LinkedIn</label>
                                            <div class="controls">
                                                <input
                                                    id="linkedin"
                                                    type="text"
                                                    name="linkedin"
                                                    class="form-control"
                                                    value="{{ $company_info->linkedin }}">
                                                <strong class="text-danger">{{ $errors->first('linkedin') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="twitter">Twitter</label>
                                            <div class="controls">
                                                <input
                                                    id="twitter"
                                                    type="text"
                                                    name="twitter"
                                                    class="form-control"
                                                    value="{{ $company_info->twitter }}">
                                                <strong class="text-danger">{{ $errors->first('twitter') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="youtube">Youtube</label>
                                            <div class="controls">
                                                <input
                                                    id="youtube"
                                                    type="text"
                                                    name="youtube"
                                                    class="form-control"
                                                    value="{{ $company_info->youtube }}">
                                                <strong class="text-danger">{{ $errors->first('youtube') }}</strong>
                                            </div>
                                        </div>
                                    </div> <!-- end col md 4 -->
                                    <div><input type="submit" class="btn btn-rounded btn-primary" value="Edit Company Info"></div>
                                </form>
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection


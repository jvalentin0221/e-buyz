@extends('layouts.admin.master')
@section('title', "Admin Dashboard")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="box box-widget widget-user">
                    <div class="widget-user-header bg-black">
                        <h3 class="widget-user-username">Admin Name: {{ $user->name }}</h3>
                        <a class="float-right" href="{{ route('admin.profile.edit', auth()->user()->id) }}">
                            <input type="submit" class="btn btn-success btn-rounded" value="Edit Profile">
                        </a>
                        <h6 class="widget-user-desc">Admin Email: {{ $user->email }}</h6>
                    </div>
                    <div class="widget-user-image">
                        @if(!empty($user->avatar))
                            @if(file_exists('uploads/avatar/' . $user->id ."/" . $user->avatar))
                                <img class="rounded-circle navbar__img d-xl-inline-block"
                                     src="{{ asset('uploads/avatar/' . $user->id ."/" . $user->avatar) }}"
                                     alt="admin image">
                            @else
                            <img class="rounded-circle navbar__img d-xl-inline-block"
                                 src="{{ Storage::disk('s3')->url('avatar/' . $user->id ."/" . $user->avatar) ?? asset('dashboard/images/avatar/user-placeholder.jpg') }}"
                                 alt="admin image">
                            @endif
                        @else
                            <img class="rounded-circle navbar__img d-xl-inline-block"
                                 src="{{ asset('assets/images/user_placeholder.png') }}"
                                 alt="admin placeholder image">
                        @endif
                    </div>
                    <div class="box-footer"></div>
                </div>
            </div>
        </section>
    </div>
@endsection

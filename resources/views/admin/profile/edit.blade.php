@extends('layouts.admin.master')
@section('title', "Admin Dashboard")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Admin Profile Edit</h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form method="POST" action="{{ route('admin.profile.update',auth()->user()->id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>Admin User Name <span class="text-danger">*</span></h5>
                                                    <div class="controls">
                                                        <input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
                                                    </div>
                                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                                </div>
                                            </div> <!-- end of col-md-6 -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>Admin Email <span class="text-danger">*</span></h5>
                                                    <div class="controls">
                                                        <input type="email" name="email" class="form-control" value="{{ $user->email }}" required>
                                                    </div>
                                                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                                </div>
                                            </div> <!-- end of col-md-6 -->
                                        </div> <!-- end of row -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>Profile Image <span class="text-danger">*</span></h5>
                                                    <div class="controls">
                                                        <input id="image" type="file" name="avatar" class="form-control">
                                                    </div>
                                                    <strong class="text-danger">{{ $errors->first('avatar') }}</strong>
                                                </div>
                                            </div> <!-- end of col-md-6 -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    @if(!empty($user->avatar))
                                                        @if(file_exists('uploads/avatar/' . $user->id ."/" . $user->avatar))
                                                            <img class="rounded-circle d-xl-inline-block"
                                                                 id="showImage"
                                                                 style="width: 100px; height: 100px"
                                                                 src="{{ asset('uploads/avatar/' . $user->id ."/" . $user->avatar) }}"
                                                                 alt="admin image">
                                                        @else
                                                            <img class="rounded-circle d-xl-inline-block"
                                                                 id="showImage"
                                                                 style="width: 100px; height: 100px"
                                                                 src="{{ Storage::disk('s3')->url('avatar/' . $user->id ."/" . $user->avatar)  }}"
                                                                 alt="admin image">
                                                        @endif
                                                    @else
                                                        <img class="rounded-circle image-fluid d-xl-inline-block"
                                                             id="showImage"
                                                             style="width: 100px; height: 100px"
                                                             src="{{ asset('assets/images/user_placeholder.png') }}"
                                                             alt="admin image">
                                                    @endif
                                                </div>
                                            </div> <!-- end of col-md-6 -->
                                        </div> <!-- end of row -->
                                    </div>
                                </div>
                                <div class="text-xs-right">
                                    <input type="submit" class="btn btn-rounded btn-primary" value="Update">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

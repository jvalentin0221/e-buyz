@extends('layouts.admin.master')
@section('title', "Manage Products")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Product List</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Product Name En</th>
                                        <th>Product Name Es</th>
                                        <th>Product Price</th>
                                        <th>Quantity</th>
                                        <th>Discount Percentage</th>
                                        <th>Discount_Price </th>
                                        <th>Status </th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>
                                                @if(file_exists('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail))
                                                    <img src="{{ asset('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail) }}"
                                                         alt="product image"
                                                         style="width:60px; height: 50px;">
                                                @else
                                                    <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $product->id ."/" . $product->product_thumbnail)}}"
                                                         style="width:80px; height: 70px;"
                                                         alt="product image">
                                                @endif
                                            </td>
                                            <td>{{ $product->product_name_en }}</td>
                                            <td>{{ $product->product_name_es }}</td>
                                            <td>{{ $product->selling_price }}</td>
                                            <td>{{ $product->product_qty }}</td>
                                            <td>
                                                <span class="badge badge-pill badge-danger">{{ $product->discount_percentage }} %</span>
                                            </td>
                                            <td>{{ $product->discount_price }}</td>
                                            <td>
                                                @if($product->status == 1)
                                                    <span class="badge badge-pill badge-success"> Active </span>
                                                @else
                                                    <span class="badge badge-pill badge-danger"> InActive </span>
                                                @endif
                                            </td>
                                            <td>
                                                <form class="{{'button' . $product->id }}" method="POST" action="{{ route('manage-products.destroy', $product->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a href="{{ route('manage-products.edit', $product->id) }}" class="btn btn-info" title="Edit Category"><i class="fa fa-pencil"></i></a>
                                                    <button id="{{'button' . $product->id }}" type="submit" class="btn btn-danger delete" title="Delete Category"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $products->links() }}
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

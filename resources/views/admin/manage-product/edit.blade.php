@extends('layouts.admin.master')
@section('title', "Edit Product")
@section('admin')
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">
            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Edit Product</h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form method="POST" action="{{ route('manage-products.update', $product->id) }}" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="">Activate Product</h5>
                                            <div class="controls">
                                                <fieldset class="mb-5">
                                                    <input type="hidden" name="status" value="0">
                                                    <input type="checkbox" id="status" name="status" value="1" {{ $product->status == 1 ? 'checked': '' }}>
                                                    <label for="status">Active</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="category_title">Category Select<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <select id="category_title" name="category_title" class="form-control" required>
                                                            <option value="" selected="" disabled="">Select Category</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category->category_name_en }}" {{ $category->category_name_en == $product->category_title ? 'selected': '' }} >{{ $category->category_name_en }}</option>
                                                            @endforeach
                                                        </select>
                                                        <strong class="text-danger">{{ $errors->first('category_title') }}</strong>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="subcategory_title">Subcategory Select<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <select id="subcategory_title" name="subcategory_title" class="form-control" required>
                                                            @foreach($subcategories as $subcategory)
                                                                <option value="{{ $subcategory->subcategory_name_en }}" {{ $subcategory->subcategory_name_en == $product->subcategory_title ? 'selected': '' }} >{{ $subcategory->subcategory_name_en }}</option>
                                                            @endforeach
                                                        </select>
                                                        <strong class="text-danger">{{ $errors->first('subcategory_title') }}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="subsubcategory_title">Sub-Subcategory Select<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <select id="subsubcategory_title" name="subsubcategory_title" class="form-control" required>
                                                            <option selected="" disabled="">Select SubSubCategory</option>
                                                            @foreach($subsubcategories as $subsubcategory)
                                                                <option value="{{ $subsubcategory->subsubcategory_name_en }}" {{ $subsubcategory->subsubcategory_name_en == $product->subsubcategory_title ? 'selected': '' }} >{{ $subsubcategory->subsubcategory_name_en }}</option>
                                                            @endforeach
                                                        </select>
                                                        <strong class="text-danger">{{ $errors->first('subsubcategory_title') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_name_en">Product Name En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_name_en"
                                                            type="text"
                                                            name="product_name_en"
                                                            class="form-control"
                                                            required
                                                            value="{{ $product->product_name_en }}">
                                                        <strong class="text-danger">{{ $errors->first('product_name_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_name_es">Product Name Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_name_es"
                                                            type="text"
                                                            name="product_name_es"
                                                            class="form-control"
                                                            required
                                                            value="{{ $product->product_name_es }}">
                                                        <strong class="text-danger">{{ $errors->first('product_name_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                        </div> <!-- end of 2ND row -->

                                        <div class="row"> <!-- start 3RD row  -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_code">Product Code<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_code"
                                                            type="text"
                                                            name="product_code"
                                                            class="form-control"
                                                            required
                                                            value="{{ $product->productExtension->product_code }}">
                                                        <strong class="text-danger">{{ $errors->first('product_code') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_qty">Product Qty<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_qty"
                                                            type="number"
                                                            name="product_qty"
                                                            class="form-control"
                                                            required
                                                            value="{{ $product->product_qty }}">
                                                        <strong class="text-danger">{{ $errors->first('product_qty') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_tags_en">Product Tags En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_tags_en"
                                                            type="text"
                                                            name="product_tags_en"
                                                            class="form-control"
                                                            required
                                                            value="{{ $product->product_tags_en }}">
                                                        <strong class="text-danger">{{ $errors->first('product_tags_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                        </div> <!-- end 3RD row  -->

                                        <div class="row"> <!-- start 4th row  -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_tags_es">Product Tags Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_tags_es"
                                                            type="text"
                                                            name="product_tags_es"
                                                            class="form-control"
                                                            required
                                                            value="{{ $product->product_tags_es }}">
                                                        <strong class="text-danger">{{ $errors->first('product_tags_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_name_en">Product Size En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input id="product_size_en"
                                                               type="text"
                                                               name="product_size_en"
                                                               value="{{ $product->product_size_en }}"
                                                               class="form-control">
                                                        <strong class="text-danger">{{ $errors->first('product_size_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_size_es">Product Size Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input id="product_size_es"
                                                               type="text"
                                                               name="product_size_es"
                                                               value="{{ $product->product_color_es }}"
                                                               class="form-control">
                                                        <strong class="text-danger">{{ $errors->first('product_size_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                        </div> <!-- end 4th row  -->

                                        <div class="row"> <!-- start 5th row  -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_color_en">Product Color En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input id="product_color_en"
                                                               type="text"
                                                               name="product_color_en"
                                                               value="{{ $product->product_color_en }}"
                                                               class="form-control">
                                                        <strong class="text-danger">{{ $errors->first('product_color_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_color_es">Product Color Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input id="product_color_es"
                                                               type="text"
                                                               name="product_color_es"
                                                               value="{{ $product->product_color_es }}"
                                                               class="form-control">
                                                        <strong class="text-danger">{{ $errors->first('product_color_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->
                                        </div> <!-- end 5th row  -->

                                        <div class="row"> <!-- start 6th row  -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="discount_price">Product Discount Price<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="discount_price"
                                                            type="number"
                                                            step="0.01"
                                                            name="discount_price"
                                                            class="form-control"
                                                            value="{{ $product->discount_price }}">
                                                        <strong class="text-danger">{{ $errors->first('discount_price') }}</strong>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="discount_percentage" value="1">
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="selling_price">Product Selling Price<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="selling_price"
                                                            type="number"
                                                            step="0.01"
                                                            name="selling_price"
                                                            class="form-control"
                                                            required
                                                            value="{{ $product->selling_price }}">
                                                        <strong class="text-danger">{{ $errors->first('selling_price') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->
                                        </div> <!-- End of 6th row -->


                                        <div class="row"> <!-- start 7th row  -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="short_description_en">Short Description En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <textarea
                                                            name="short_description_en"
                                                            id="short_description_en"
                                                            class="form-control"
                                                            required
                                                            placeholder="short description">
                                                            {{ $product->productExtension->short_description_en }}
                                                        </textarea>
                                                        <strong class="text-danger">{{ $errors->first('short_description_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="short_description_es">Short Description Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <textarea
                                                            name="short_description_es"
                                                            id="short_description_es"
                                                            class="form-control"
                                                            required
                                                            placeholder="short description">
                                                            {{ $product->productExtension->short_description_es }}
                                                        </textarea>
                                                        <strong class="text-danger">{{ $errors->first('short_description_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->
                                        </div> <!-- end 7th row  -->

                                        <div class="row"> <!-- start 8th row  -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="long_description_en">long Description En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <textarea
                                                            name="long_description_en"
                                                            id="long_description_en"
                                                            rows="10"
                                                            cols="80"
                                                            required>
                                                            {{ $product->productExtension->long_description_en }}
                                                        </textarea>
                                                        <strong class="text-danger">{{ $errors->first('long_description_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="long_description_es">long Description Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <textarea
                                                            name="long_description_es"
                                                            id="long_description_es"
                                                            rows="10"
                                                            cols="80"
                                                            required>
                                                            {{ $product->productExtension->long_description_es }}
                                                        </textarea>
                                                        <strong class="text-danger">{{ $errors->first('long_description_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->
                                        </div><!-- end of 8TH row -->

                                        <div class="row"> <!-- start of 9TH row -->
                                            <div class="col-md-6">
                                                <div class="controls">
                                                    <fieldset>
                                                        <input type="hidden" name="hot_deals" value="0">
                                                        <input type="checkbox" id="checkbox_2" name="hot_deals" value="1" {{ $product->productExtension->hot_deals == 1 ? 'checked': '' }}>
                                                        <label for="checkbox_2">Hot Deals</label>
                                                    </fieldset>
                                                    <fieldset>
                                                        <input type="hidden" name="new" value="0">
                                                        <input type="checkbox" id="checkbox_3" name="new" value="1" {{ $product->productExtension->new == 1 ? 'checked': '' }}>
                                                        <label for="checkbox_3">New</label>
                                                    </fieldset>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <fieldset>
                                                    <input type="hidden" name="special_offer" value="0">
                                                    <input type="checkbox" id="checkbox_4" name="special_offer" value="1" {{ $product->productExtension->special_offer == 1 ? 'checked': '' }}>
                                                    <label for="checkbox_4">Special Offer</label>
                                                </fieldset>
                                                <fieldset>
                                                    <input type="hidden" name="special_deals" value="0">
                                                    <input type="checkbox" id="checkbox_5" name="special_deals" value="1" {{ $product->productExtension->special_deals == 1 ? 'checked': '' }}>
                                                    <label for="checkbox_5">Special Deals</label>
                                                </fieldset>
                                            </div>
                                        </div><!-- end of 9TH row -->
                                        <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Edit Product">
                                    </div>
                                </div>
                            </form>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div> <!-- /.box -->
        </section> <!-- /.content -->

        <!--Product multi image-->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box bt-3 border-info">
                        <div class="box-header">
                            <h4 class="box-title">Product Multiple Image <strong>Update</strong></h4>
                        </div>
                        <form method="POST" action="{{ route('admin.manage-product.update-multi-img', $product->id) }}" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="row row-sm">
                                @foreach($product->multiimgs as $img)
                                    <div class="col-md-3">
                                        <div class="card">
                                            @if(file_exists('uploads/multi_img/' . $product->id ."/" . $img->photo_name))
                                                <img src="{{ asset('uploads/multi_img/' . $product->id ."/" . $img->photo_name) }}"
                                                     alt="multi image product"
                                                     style="width:200px; height: 200px;">
                                            @else
                                                <img src="{{ Storage::disk('s3')->url('multi_img/' . $product->id ."/" . $img->photo_name)}}"
                                                     style="width:280px; height: 130px;"
                                                     alt="multi image product">
                                            @endif
                                            <div class="card-body">
                                                <button id="{{'button' . $img->id }}" form="{{'button' . $img->id }}" type="submit" class="btn btn-danger delete" title="Delete image"><i class="fa fa-trash"></i></button>
                                                <p class="card-text">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Change Image <span class="tx-danger">*</span></label>
                                                        <input class="form-control" type="file" name="multi_img[{{ $img->id }}]">
                                                    </div>
                                                </p>
                                                <strong class="text-danger">{{ $errors->first('multi_img[]') }}</strong>
                                            </div>
                                        </div>
                                    </div><!--  end col md 3-->
                                @endforeach
                            </div>
                            <div class="text-xs-right">
                                <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Update Images">
                            </div>
                            <br><br>
                        </form>
                        @foreach($product->multiimgs as $img)
                            <form class="{{'button' . $img->id }}" id="{{'button' . $img->id }}" method="POST" action="{{ route('admin.manage-product.delete-multi-img', $img->id) }}">
                                @method('DELETE')
                                @csrf
                            </form>
                        @endforeach
                    </div>
                </div>
            </div> <!-- // end row  -->
        </section>

        <!-- Product thumbnail -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box bt-3 border-info">
                        <div class="box-header">
                            <h4 class="box-title">Product Thumbnail Image <strong>Update</strong></h4>
                        </div>
                        <form method="POST" action="{{ route('admin.manage-product.update-thumbnail', $product->id) }}" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="row row-sm">
                                <div class="col-md-3">
                                    <div class="card">
                                        @if(file_exists('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail))
                                            <img src="{{ asset('uploads/product_thumbnail/' . $product->id ."/" . $product->product_thumbnail) }}"
                                                 alt="product_thumbnail"
                                                 style="width:280px; height: 130px;">
                                        @else
                                            <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $product->id ."/" . $product->product_thumbnail)}}"
                                                 style="width:280px; height: 130px;"
                                                 alt="product_thumbnail">
                                        @endif
                                        <div class="card-body">
                                            <p class="card-text">
                                                <div class="form-group">
                                                    <label for="thumbnail" class="form-control-label">Change Image <span class="tx-danger">*</span></label>
                                                    <input
                                                        id="thumbnail"
                                                        type="file"
                                                        name="product_thumbnail"
                                                        value="{{ $product->thumbnail }}"
                                                        class="form-control">
                                                    <strong class="text-danger">{{ $errors->first('product_thumbnail') }}</strong>
                                                    <img src="" id="mainThumbNail">
                                                </div>
                                            </p>
                                        </div>
                                    </div>
                                </div><!--  end col md 3		 -->
                            </div>
                            <div class="text-xs-right">
                                <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Update Image">
                            </div>
                            <br><br>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

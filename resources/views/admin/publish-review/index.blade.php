@extends('layouts.admin.master')
@section('title', "Published Reviews")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Published Reviews</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Summary</th>
                                        <th>Comment</th>
                                        <th>User</th>
                                        <th>Product</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($reviews as $review)
                                        <tr>
                                            <td>{{ $review->summary }}</td>
                                            <td>{{ $review->comment }}</td>
                                            <td>{{ $review->user->name }}</td>
                                            <td>{{ $review->product->product_name_en }}</td>
                                            <td>
                                                <span class="badge badge-pill badge-success">Publish</span>
                                            </td>
                                            <td>
                                                <form method="POST" action="{{ route('admin.publish_review.destroy', $review->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button  type="submit" class="btn btn-danger" title="Delete review">
                                                        <span>
                                                            &cross;
                                                        </span>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $reviews->links() }}
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

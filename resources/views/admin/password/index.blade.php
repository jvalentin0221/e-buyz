@extends('layouts.admin.master')
@section('title', "Admin Dashboard")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Change Password</h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form method="POST" action="{{ route('admin.password.update',auth()->user()->id) }}">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>Current Password <span class="text-danger">*</span></h5>
                                                    <div class="controls">
                                                        <input type="password" name="old_password" class="form-control" required>
                                                    </div>
                                                    <strong class="text-danger">{{ $errors->first('old_password') }}</strong>
                                                </div>
                                            </div> <!-- end of col-md-6 -->
                                        </div> <!-- end of row -->
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>New Password <span class="text-danger">*</span></h5>
                                                    <div class="controls">
                                                        <input type="password" name="new_password" class="form-control" value="" required>
                                                    </div>
                                                    <strong class="text-danger">{{ $errors->first('new_password') }}</strong>
                                                </div>
                                            </div> <!-- end of col-md-6 -->
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Confirm New Password <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="password" name="password_confirmation" class="form-control" value="" required>
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                                    </div>
                                    <div class="text-xs-right">
                                        <input type="submit" class="btn btn-rounded btn-primary" value="Update">
                                    </div>
                                </div> <!-- end of col-md-6 -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@extends('layouts.admin.master')
@section('title', "Sub Categories")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sub Category List</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Category </th>
                                        <th>SubCategory En</th>
                                        <th>SubCategory Es </th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subcategories as $item)
                                        <tr>
                                            <td> {{ $item->category_title }}  </td>
                                            <td>{{ $item->subcategory_name_en }}</td>
                                            <td>{{ $item->subcategory_name_es }}</td>
                                            <td>
                                                <form class="{{'button' . $item->id }}" method="POST" action="{{ route('subcategories.destroy', $item->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a href="{{ route('subcategories.edit', $item->id) }}" class="btn btn-info" title="Edit Category"><i class="fa fa-pencil"></i></a>
                                                    <button id="{{'button' . $item->id }}" type="submit" class="btn btn-danger delete" title="Delete Category"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $subcategories }}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
                <!-- Add category column -->
                <div class="col-lg-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Sub Category</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <form method="POST" action="{{ route('subcategories.store',auth()->user()->id) }}">
                                    @csrf
                                    <label class="text-bold" for="select">Category Select <span class="text-danger">*</span></label>
                                    <div class="form-group">
                                        <div class="controls">
                                            <select name="category_title" id="select"  class="form-control">
                                                <option value="">Select Your Category</option>
                                                @foreach ($category as $categories)
                                                    <option value="{{ $categories->category_name_en }}">{{ $categories->category_name_en }}</option>
                                                @endforeach
                                            </select>
                                            <strong class="text-danger">{{ $errors->first('category_title') }}</strong>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-bold" for="subcategory_name_en">Sub Category Name English <span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id=subcategory_name_en type="text" name="subcategory_name_en" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('subcategory_name_en') }}</strong>
                                    </div>

                                    <div class="form-group">
                                        <label class="text-bold" for="subcategory_name_es">Sub Category Name Spanish <span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id="subcategory_name_es" type="text" name="subcategory_name_es" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('subcategory_name_es') }}</strong>
                                    </div>
                                    <input type="hidden" name="subcategory_slug_en" value="subcategory">
                                    <input type="hidden" name="subcategory_slug_es" value="subcategory">

                                    <div>
                                        <input type="submit" class="btn btn-rounded btn-primary" value="Add SubCategory">
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

@extends('layouts.admin.master')
@section('title', "Update Sub Category")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="container">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Edit Sub Category</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <form method="POST" action="{{ route('subcategories.update', $subcategory->id) }}">
                                        @method('PUT')
                                        @csrf

                                        <div class="form-group">
                                            <label  class="text-bold" for="title">Category Title<span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <select name="category_title" id="title"  class="form-control">
                                                    <option value="">Select Your Category</option>
                                                    @foreach ($category as $categories)
                                                        <option
                                                            value="{{$categories->category_name_en }}"
                                                            {{ $categories->category_name_en == $subcategory->category_title ? 'selected': ''}}>
                                                            {{ $categories->category_name_en }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <strong class="text-danger">{{ $errors->first('category_title') }}</strong>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="text-bold" for="name">Sub Category Name English <span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <input id="name" type="text" name="subcategory_name_en" class="form-control" value="{{ $subcategory->subcategory_name_en }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('subcategory_name_en') }}</strong>
                                        </div>

                                        <div class="form-group">
                                            <label  class="text-bold" for="name_es">Sub Category Name Spanish <span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <input id="name_es" type="text" name="subcategory_name_es" class="form-control" value="{{ $subcategory->subcategory_name_es }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('subcategory_name_es') }}</strong>
                                        </div>
                                        <input type="hidden" name="subcategory_slug_en" value="subcategory">
                                        <input type="hidden" name="subcategory_slug_es" value="subcategory">
                                        <div>
                                            <input type="submit" class="btn btn-rounded btn-primary" value="Edit Sub Category">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

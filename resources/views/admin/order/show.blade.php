@extends('layouts.admin.master')
@section('title', "Show Order")
@section('admin')
    <div class="container-full">
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="page-title">Order Details</h3>
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                <li class="breadcrumb-item" aria-current="page">Order Details</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="box box-bordered border-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title"><strong>Shipping Details</strong> </h4>
                        </div>
                        <table class="table">
                            <tr>
                                <th> Shipping Name : </th>
                                <th> {{ $order->name }} </th>
                            </tr>
                            <tr>
                                <th> Shipping Phone : </th>
                                <th> {{ $order->phone }} </th>
                            </tr>
                            <tr>
                                <th> Shipping Email : </th>
                                <th> {{ $order->email }} </th>
                            </tr>
                            <tr>
                                <th> Address : </th>
                                <th>{{ $order->address }}</th>
                            </tr>
                            <tr>
                                <th> State : </th>
                                <th>{{ $order->state }}</th>
                            </tr>
                            <tr>
                                <th> Zip Code : </th>
                                <th> {{ $order->zip_code }} </th>
                            </tr>
                            <tr>
                                <th> Order Date : </th>
                                <th> {{ $order->order_date }} </th>
                            </tr>
                        </table>
                    </div>
                </div> <!--  // cod md -6 -->

                <div class="col-md-6 col-12">
                    <div class="box box-bordered border-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title"><strong>Order Details</strong>
                                <span class="text-danger"> Invoice : {{ $order->invoice_no }}</span>
                            </h4>
                        </div>

                        <table class="table">
                            <tr>
                                <th>  Name : </th>
                                <th> {{ $order->name }} </th>
                            </tr>
                            <tr>
                                <th>  Phone : </th>
                                <th> {{ $order->phone }} </th>
                            </tr>
                            <tr>
                                <th> Payment Type : </th>
                                <th> {{ $order->payment_method }} </th>
                            </tr>
                            <tr>
                                <th> Transaction ID : </th>
                                <th> {{ $order->transaction_id }} </th>
                            </tr>
                            <tr>
                                <th> Invoice  : </th>
                                <th class="text-danger"> {{ $order->invoice_no }} </th>
                            </tr>
                            <tr>
                                <th> Order Total : </th>
                                <th>${{ $order->amount }} </th>
                            </tr>
                            <tr>
                                <th> Order : </th>
                                <th>
                                    <span class="badge badge-pill badge-warning" style="background: #418DB9;">{{ $order->status }}</span>
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    @if($order->status == 'pending')
                                    <form class="{{'button' . $order->id }}" method="POST" action="{{ route('admin.order_status.pending_to_confirmed', $order->id) }}">
                                        @method('PUT')
                                        @csrf
                                        <button id="{{'button' . $order->id }}" type="submit" class="btn btn-block btn-success confirmed" title="Confirmed Order">Confirm Order</button>
                                    </form>
                                    @elseif($order->status == 'confirmed')
                                        <form class="{{'button' . $order->id }}" method="POST" action="{{ route('admin.order_status.confirmed_to_processing', $order->id) }}">
                                            @method('PUT')
                                            @csrf
                                            <button id="{{'button' . $order->id }}" type="submit" class="btn btn-block btn-success processing" title="Confirmed Order">Process Order</button>
                                        </form>
                                    @elseif($order->status == 'processing')
                                        <form class="{{'button' . $order->id }}" method="POST" action="{{ route('admin.order_status.processing_to_picked', $order->id) }}">
                                            @method('PUT')
                                            @csrf
                                            <button id="{{'button' . $order->id }}" type="submit" class="btn btn-block btn-success picked" title="Confirmed Order">Picked Order</button>
                                        </form>
                                    @elseif($order->status == 'picked')
                                        <form class="{{'button' . $order->id }}" method="POST" action="{{ route('admin.order_status.picked_to_shipped', $order->id) }}">
                                            @method('PUT')
                                            @csrf
                                            <button id="{{'button' . $order->id }}" type="submit" class="btn btn-block btn-success picked" title="Confirmed Order">Shipped Order</button>
                                        </form>
                                    @elseif($order->status == 'shipped')
                                        <form class="{{'button' . $order->id }}" method="POST" action="{{ route('admin.order_status.shipped_to_delivered', $order->id) }}">
                                            @method('PUT')
                                            @csrf
                                            <button id="{{'button' . $order->id }}" type="submit" class="btn btn-block btn-success delivered" title="Confirmed Order">Delivered Order</button>
                                        </form>
                                    @endif
                                </th>
                            </tr>
                        </table>
                    </div>
                </div> <!--  // cod md -6 -->

                <div class="col-md-12 col-12">
                    <div class="box box-bordered border-primary">
                        <div class="box-header with-border">

                        </div>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td width="10%">
                                    <label for=""> Image</label>
                                </td>
                                <td width="20%">
                                    <label for=""> Product Name </label>
                                </td>
                                <td width="10%">
                                    <label for=""> Product Code</label>
                                </td>
                                <td width="10%">
                                    <label for=""> Color </label>
                                </td>
                                <td width="10%">
                                    <label for=""> Size </label>
                                </td>
                                <td width="10%">
                                    <label for=""> Quantity </label>
                                </td>
                                <td width="10%">
                                    <label for=""> Price </label>
                                </td>
                            </tr>
                            @foreach($orderItems as $item)
                                <tr>
                                    <td width="10%">
                                        @if(file_exists('uploads/product_thumbnail/' . $item->product->id ."/" . $item->product->product_thumbnail))
                                            <img src="{{ asset('uploads/product_thumbnail/' . $item->product->id ."/" . $item->product->product_thumbnail) }}"
                                                 style="width:50px; height: 50px;"
                                                 alt="product image">

                                        @else
                                            <img src="{{ Storage::disk('s3')->url('product_thumbnail/' . $item->product->id ."/" . $item->product->product_thumbnail)}}"
                                                 style="width:50px; height: 50px;"
                                                 alt="product image">
                                        @endif
                                    </td>
                                    <td width="20%">
                                        {{ $item->product->product_name_en }}
                                    </td>
                                    <td width="10%">
                                         {{ $item->product->product_code }}
                                    </td>
                                    <td width="10%">
                                         {{ $item->color }}
                                    </td>
                                    <td width="10%">
                                        {{ $item->size }}
                                    </td>
                                    <td width="10%">
                                        {{ $item->qty }}
                                    </td>
                                    <td width="10%">
                                        ${{ $item->price }}  ( $ {{ $item->price * $item->qty}} )
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>  <!-- col-md-12 -->
            </div><!-- /. end row -->
        </section><!-- /.content -->
    </div>
@endsection

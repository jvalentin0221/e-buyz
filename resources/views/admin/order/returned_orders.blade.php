@extends('layouts.admin.master')
@section('title', "Returned Orders")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Returned Orders</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Date </th>
                                        <th>Invoice </th>
                                        <th>Amount </th>
                                        <th>Payment </th>
                                        <th>Status </th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td> {{ $order->order_date }}  </td>
                                            <td> {{ $order->invoice_no }}  </td>
                                            <td> ${{ $order->amount }}  </td>

                                            <td> {{ $order->payment_method }}  </td>
                                            <td>
                                                @if($order->status == 'returned')
                                                    <span class="badge badge-pill badge-warning" style="background: #418DB9;">{{ $order->status }} </span>
                                                @else
                                                    <span class="badge badge-pill badge-warning" style="background: red;">Return Requested </span>
                                                @endif
                                            </td>
                                            <td>
                                                <form class="{{'button' . $order->id }}" method="POST" action="{{ route('admin.order_status.returned', $order->id) }}">
                                                    @method('PUT')
                                                    @csrf
                                                    <button type="submit" class="btn btn-success" title="Return Item">Return Item</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $orders->links() }}
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

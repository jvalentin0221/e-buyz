@extends('layouts.admin.master')
@section('title', "Picked Orders")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Picked Orders</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Date </th>
                                        <th>Invoice </th>
                                        <th>Amount </th>
                                        <th>Payment </th>
                                        <th>Status </th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td> {{ $order->order_date }}  </td>
                                            <td> {{ $order->invoice_no }}  </td>
                                            <td> ${{ $order->amount }}  </td>

                                            <td> {{ $order->payment_method }}  </td>
                                            <td> <span class="badge badge-pill badge-primary">{{ $order->status }} </span>  </td>
                                            <td>
                                                <form class="{{'button' . $order->id }}" method="POST" action="{{ route('coupons.destroy', $order->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a href="{{ route('admin.order.show', $order->id) }}" class="btn btn-info" title="View Order"><i class="fa fa-eye"></i></a>
                                                    <button id="{{'button' . $order->id }}" type="submit" class="btn btn-danger delete" title="Delete Order"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $orders->links() }}
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection





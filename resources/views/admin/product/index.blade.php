@extends('layouts.admin.master')
@section('title', "Products")
@section('admin')
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">
            <!-- Basic Forms -->
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Add Product </h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form method="POST" action="{{ route('admin.product.store',auth()->user()->id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row"> <!-- start 1st row  -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="category_title">Category Select<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <select id="category_title" name="category_title" class="form-control" required>
                                                            <option value="{{ old('category_title') }}" selected="" disabled="">Select Category</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category->category_name_en }}">{{ $category->category_name_en }}</option>
                                                            @endforeach
                                                        </select>
                                                        <strong class="text-danger">{{ $errors->first('category_title') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="subcategory_title">Subcategory Select<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <select id="subcategory_title" name="subcategory_title" class="form-control" required>
                                                            <option value="{{ old('subcategory_title') }}" selected="" disabled="">Select SubCategory</option>
                                                        </select>
                                                        <strong class="text-danger">{{ $errors->first('subcategory_title') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                        </div> <!-- end 1st row  -->

                                        <div class="row"> <!-- start 2nd row  -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="subsubcategory_title">Sub-Subcategory Select<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <select id="subsubcategory_title" name="subsubcategory_title" class="form-control" required>
                                                            <option value="value="{{ old('subsubcategory_title') }} selected="" disabled="">Select SubSubCategory</option>
                                                        </select>
                                                        <strong class="text-danger">{{ $errors->first('subsubcategory_title') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_name_en">Product Name En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_name_en"
                                                            type="text"
                                                            name="product_name_en"
                                                            class="form-control"
                                                            required
                                                            value="{{ old('product_name_en') }}">
                                                        <strong class="text-danger">{{ $errors->first('product_name_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_name_es">Product Name Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_name_es"
                                                            type="text"
                                                            name="product_name_es"
                                                            class="form-control"
                                                            required
                                                            value="{{ old('product_name_es') }}">
                                                        <strong class="text-danger">{{ $errors->first('product_name_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                        </div> <!-- end of 2ND row -->

                                        <div class="row"> <!-- start 3RD row  -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_code">Product Code<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_code"
                                                            type="text"
                                                            name="product_code"
                                                            class="form-control"
                                                            required
                                                            value="{{ old('product_code') }}">
                                                        <strong class="text-danger">{{ $errors->first('product_code') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_qty">Product Qty<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_qty"
                                                            type="text"
                                                            name="product_qty"
                                                            class="form-control"
                                                            required
                                                            value="{{ old('product_qty') }}">
                                                        <strong class="text-danger">{{ $errors->first('product_qty') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_tags_en">Product Tags En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_name_en"
                                                            type="text"
                                                            name="product_tags_en"
                                                            class="form-control"
                                                            required
                                                            value="{{ old('product_tags_en') }}">
                                                        <strong class="text-danger">{{ $errors->first('product_tags_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                        </div> <!-- end 3RD row  -->

                                        <div class="row"> <!-- start 4th row  -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_tags_es">Product Tags Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="product_tags_es"
                                                            type="text"
                                                            name="product_tags_es"
                                                            class="form-control"
                                                            required
                                                            value="{{ old('product_name_es') }}">
                                                        <strong class="text-danger">{{ $errors->first('product_tags_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_name_en">Product Size En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input id="product_name_en"
                                                               type="text"
                                                               name="product_size_en"
                                                               value="{{ old('product_size_en') }}"
                                                               class="form-control">
                                                        <strong class="text-danger">{{ $errors->first('product_size_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_name_es">Product Size Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input id="product_name_es"
                                                               type="text"
                                                               name="product_size_es"
                                                               value="{{ old('product_color_es') }}"
                                                               class="form-control">
                                                        <strong class="text-danger">{{ $errors->first('product_size_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                        </div> <!-- end 4th row  -->

                                        <div class="row"> <!-- start 5th row  -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_color_en">Product Color En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input id="product_color_en"
                                                               type="text"
                                                               name="product_color_en"
                                                               value="{{ old('product_color_en') }}"
                                                               class="form-control">
                                                        <strong class="text-danger">{{ $errors->first('product_color_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_color_es">Product Color Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input id="product_color_es"
                                                               type="text"
                                                               name="product_color_es"
                                                               value="{{ old('product_color_es') }}"
                                                               class="form-control">
                                                        <strong class="text-danger">{{ $errors->first('product_color_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="selling_price">Product Selling Price<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="selling_price"
                                                            type="number"
                                                            step="0.01"
                                                            name="selling_price"
                                                            class="form-control"
                                                            required
                                                            value="{{ old('selling_price') }}">
                                                        <strong class="text-danger">{{ $errors->first('selling_price') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                        </div> <!-- end 5th row  -->

                                        <div class="row"> <!-- start 6th row  -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="discount_price">Product Discount Price<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="discount_price"
                                                            type="number"
                                                            step="0.01"
                                                            name="discount_price"
                                                            class="form-control"
                                                            required
                                                            value="{{ old('discount_price') }}">
                                                        <strong class="text-danger">{{ $errors->first('discount_price') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="product_thumbnail">Main Thumbnail<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="thumbnail"
                                                            type="file"
                                                            name="product_thumbnail"
                                                            value="{{ old('product_thumbnail') }}"
                                                            required
                                                            class="form-control">
                                                        <strong class="text-danger">{{ $errors->first('product_thumbnail') }}</strong>
                                                        <img src="" id="mainThumbNail">
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-bold" for="multi_img">Multiple Images<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <input
                                                            id="multiImg"
                                                            type="file"
                                                            name="multi_img[]"
                                                            class="form-control"
                                                            value="{{ old('multi_img') }}"
                                                            required
                                                            multiple>
                                                        <strong class="text-danger">{{ $errors->first('multi_img') }}</strong>
                                                        <div class="row" id="preview_img"></div>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 4 -->
                                        </div> <!-- end 6th row  -->

                                        <div class="row"> <!-- start 7th row  -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="short_description_en">Short Description En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <textarea
                                                            name="short_description_en"
                                                            id="short_description_en"
                                                            class="form-control"
                                                            required
                                                            placeholder="short description">
                                                            {{ old('short_description_en') }}
                                                        </textarea>
                                                        <strong class="text-danger">{{ $errors->first('short_description_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="short_description_es">Short Description Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <textarea
                                                            name="short_description_es"
                                                            id="short_description_es"
                                                            class="form-control"
                                                            required
                                                            placeholder="short description">
                                                            {{ old('short_description_es') }}
                                                        </textarea>
                                                        <strong class="text-danger">{{ $errors->first('short_description_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->
                                        </div> <!-- end 7th row  -->

                                        <div class="row"> <!-- start 8th row  -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="long_description_en">long Description En<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <textarea name="long_description_en" id="long_description_en" rows="10" cols="80" required>
                                                            {{ old('long_description_en') }}
                                                        </textarea>
                                                        <strong class="text-danger">{{ $errors->first('long_description_en') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-bold" for="long_description_es">long Description Es<span class="text-danger">*</span></label>
                                                    <div class="controls">
                                                        <textarea name="long_description_es" id="long_description_es" rows="10" cols="80" required>
                                                            {{ old('long_description_es') }}
                                                        </textarea>
                                                        <strong class="text-danger">{{ $errors->first('long_description_es') }}</strong>
                                                    </div>
                                                </div>
                                            </div> <!-- end col md 6 -->
                                        </div><!-- end of 8TH row -->

                                        <div class="row"> <!-- start of 9TH row -->
                                            <div class="col-md-6">
                                                <div class="controls">
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_2" name="hot_deals" value="1">
                                                        <label for="checkbox_2">Hot Deals</label>
                                                    </fieldset>
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_3" name="new" value="1">
                                                        <label for="checkbox_3">New</label>
                                                    </fieldset>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <fieldset>
                                                    <input type="checkbox" id="checkbox_4" name="special_offer" value="1">
                                                    <label for="checkbox_4">Special Offer</label>
                                                </fieldset>
                                                <fieldset>
                                                    <input type="checkbox" id="checkbox_5" name="special_deals" value="1">
                                                    <label for="checkbox_5">Special Deals</label>
                                                </fieldset>
                                            </div>
                                        </div><!-- end of 9TH row -->
                                    <input type="submit" class="btn btn-rounded btn-primary mb-5" value="Add Product">
                                </div>
                                    <input type="hidden" name="discount_percentage" value="1">
                                </div>
                            </form>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div> <!-- /.box -->
        </section> <!-- /.content -->
    </div>
@endsection

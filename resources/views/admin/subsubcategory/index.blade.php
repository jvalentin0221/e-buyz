@extends('layouts.admin.master')
@section('title', "Sub->SubCategory")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sub->SubCategory List</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Subcategory</th>
                                        <th>Sub->SubCategory En</th>
                                        <th>Sub->SubCategory Es</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subSubCategories as $item)
                                        <tr>
                                            <td>{{ $item->category_title }}</td>
                                            <td>{{ $item->subcategory_title }}</td>
                                            <td>{{ $item->subsubcategory_name_en }}</td>
                                            <td>{{ $item->subsubcategory_name_es }}</td>
                                            <td>
                                                <form class="{{'button' . $item->id }}" method="POST" action="{{ route('subsubcategories.destroy', $item->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a href="{{ route('subsubcategories.edit', $item->id) }}" class="btn btn-info" title="Edit Category"><i class="fa fa-pencil"></i></a>
                                                    <button id="{{'button' . $item->id }}" type="submit" class="btn btn-danger delete" title="Delete Category"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $subSubCategories->links() }}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
                <!-- Add category column -->
                <div class="col-lg-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Sub-Subcategory</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <form method="POST" action="{{ route('subsubcategories.store',auth()->user()->id) }}">
                                    @csrf
                                    <label class="text-bold" for="category_title">Category Select <span class="text-danger">*</span></label>
                                    <div class="form-group">
                                        <div class="controls">
                                            <select name="category_title" id="category_title"  class="form-control">
                                                <option value="">Select Your Category</option>
                                                @foreach ($category as $categories)
                                                    <option value="{{ $categories->category_name_en }}">{{ $categories->category_name_en }}</option>
                                                @endforeach
                                            </select>
                                            <strong class="text-danger">{{ $errors->first('category_title') }}</strong>
                                        </div>
                                    </div>
                                    <label class="text-bold" for="subcategory_title">Subcategory Select<span class="text-danger">*</span></label>
                                    <div class="form-group">
                                        <div class="controls">
                                            <select name="subcategory_title" id="subcategory_title"  class="form-control">
                                                <option value="">Select Your Subcategory</option>
                                            </select>
                                            <strong class="text-danger">{{ $errors->first('subcategory_title') }}</strong>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-bold" for="subsubcategory_name_en">Sub-Subcategory English<span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id=subsubcategory_name_en type="text" name="subsubcategory_name_en" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('subsubcategory_name_en') }}</strong>
                                    </div>

                                    <div class="form-group">
                                        <label class="text-bold" for="subsubcategory_name_es">Sub-Subcategory Spanish <span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id="subsubcategory_name_es" type="text" name="subsubcategory_name_es" class="form-control">
                                        </div>
                                        <strong class="text-danger">{{ $errors->first('subsubcategory_name_es') }}</strong>
                                    </div>
                                    <input type="hidden" name="subsubcategory_slug_en" value="subcategory">
                                    <input type="hidden" name="subsubcategory_slug_es" value="subcategory">
                                    <div>
                                        <input type="submit" class="btn btn-rounded btn-primary" value="Add Sub-SubCategory">
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
    <script src="{{ asset('js/populator.js') }}"></script>
@endsection

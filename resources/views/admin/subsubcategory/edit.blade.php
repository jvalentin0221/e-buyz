@extends('layouts.admin.master')
@section('title', "Update Sub-Subcategory")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="container">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Edit Sub->Subcategory</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <form method="POST" action="{{ route('subsubcategories.update', $subsubcategory->id) }}">
                                        @method('PUT')
                                        @csrf

                                        <div class="form-group">
                                            <label  class="text-bold" for="title">Category Title<span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <select name="category_title" id="title"  class="form-control">
                                                    <option value="">Select Your Category</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->category_name_en }}" {{ $category->category_name_en == $subsubcategory->category_title ? 'selected':'' }} >{{ $category->category_name_en }}</option>
                                                    @endforeach
                                                </select>
                                                <strong class="text-danger">{{ $errors->first('category_title') }}</strong>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="text-bold" for="subcategory_title">Subcategory Title<span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <select name="subcategory_title" class="form-control" id="subcategory_title">
                                                    <option value="" selected="" disabled="">Select SubCategory</option>
                                                    @foreach($subcategories as $subs)
                                                        <option value="{{ $subs->subcategory_name_en }}" {{ $subs->subcategory_name_en == $subsubcategory->subcategory_title ? 'selected':'' }} >{{ $subs->subcategory_name_en }}</option>
                                                    @endforeach
                                                </select>
                                                <strong class="text-danger">{{ $errors->first('subcategory_title') }}</strong>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="text-bold" for="subsubcategory_name_en">Sub-Subcategory Name English <span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <input id="subsubcategory_name_en" type="text" name="subsubcategory_name_en" class="form-control" value="{{ $subsubcategory->subsubcategory_name_en }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('subsubcategory_name_en') }}</strong>
                                        </div>

                                        <div class="form-group">
                                            <label  class="text-bold" for="name_es">Sub-Subcategory Name Spanish <span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <input id="name_es" type="text" name="subsubcategory_name_es" class="form-control" value="{{ $subsubcategory->subsubcategory_name_es }}">
                                            </div>
                                            <strong class="text-danger">{{ $errors->first('subsubcategory_name_es') }}</strong>
                                        </div>
                                        <input type="hidden" name="subsubcategory_slug_en" value="subcategory">
                                        <input type="hidden" name="subsubcategory_slug_es" value="subcategory">
                                        <div>
                                            <input type="submit" class="btn btn-rounded btn-primary" value="Edit Sub->Subcategory">
                                        </div>
                                    </form>
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

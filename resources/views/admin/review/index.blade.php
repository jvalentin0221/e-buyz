@extends('layouts.admin.master')
@section('title', "Pending Reviews")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pending Reviews</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Summary</th>
                                        <th>Comment</th>
                                        <th>User</th>
                                        <th>Product</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($reviews as $review)
                                        <tr>
                                            <td>{{ $review->summary }}</td>
                                            <td>{{ $review->comment }}</td>
                                            <td>{{ $review->user->name }}</td>
                                            <td>{{ $review->product->product_name_en }}</td>
                                            <td>
                                                @if($review->status == 0)
                                                    <span class="badge badge-pill badge-primary">Pending </span>
                                                @elseif($review->status == 1)
                                                    <span class="badge badge-pill badge-success">Publish </span>
                                                @endif
                                            </td>
                                            <td>
                                                <form method="POST" action="{{ route('admin.review.update', $review->id) }}">
                                                    @method('PUT')
                                                    @csrf
                                                    <button type="submit" class="btn btn-success" title="Approve review">
                                                        <span>
                                                            &#10003;
                                                        </span>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $reviews->links() }}
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

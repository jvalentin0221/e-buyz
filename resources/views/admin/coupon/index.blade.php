@extends('layouts.admin.master')
@section('title', "Coupons")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Coupon List</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Coupon Name </th>
                                        <th>Coupon Discount</th>
                                        <th>Validity </th>
                                        <th>Status </th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($coupons as $coupon)
                                        <tr>
                                            <td> {{ $coupon->coupon_name }}  </td>
                                            <td> {{ $coupon->coupon_discount }}% </td>
                                            <td> {{ $coupon->coupon_validity }}</td>
                                            <td>
                                                @if($coupon->coupon_validity >= Carbon\Carbon::now()->format('m/d/y'))
                                                    <span class="badge badge-pill badge-success"> Valid </span>
                                                @else
                                                    <span class="badge badge-pill badge-danger"> Invalid </span>
                                                @endif
                                            </td>
                                            <td>
                                                <form class="{{'button' . $coupon->id }}" method="POST" action="{{ route('coupons.destroy', $coupon->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <a href="{{ route('coupons.edit', $coupon->id) }}" class="btn btn-info" title="Edit Category"><i class="fa fa-pencil"></i></a>
                                                    <button id="{{'button' . $coupon->id }}" type="submit" class="btn btn-danger delete" title="Delete Category"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $coupons->links() }}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
                <!-- Add category column -->
                <div class="col-lg-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Coupon</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <form method="POST" action="{{ route('coupons.store',auth()->user()->id) }}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="text-bold" for="coupon_name">Coupon Name <span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id="coupon_name" type="text" name="coupon_name" class="form-control">
                                            <strong class="text-danger">{{ $errors->first('coupon_name') }}</strong>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="text-bold" for="coupon_discount">Coupon Discount(%) <span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id="coupon_discount" type="text" name="coupon_discount" class="form-control">
                                            <strong class="text-danger">{{ $errors->first('coupon_discount') }}</strong>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="text-bold" for="coupon_validity">Coupon Validity Date <span class="text-danger">*</span></label>
                                        <div class="controls">
                                            <input id="coupon_validity" type="date" name="coupon_validity" class="form-control" min="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                                            <strong class="text-danger">{{ $errors->first('coupon_validity') }}</strong>
                                        </div>
                                    </div>
                                    <div><input type="submit" class="btn btn-rounded btn-primary" value="Add Coupon"></div>
                                </form>
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

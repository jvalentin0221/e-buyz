@extends('layouts.admin.master')
@section('title', "Update Coupon")
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Edit Coupon</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <form method="POST" action="{{ route('coupons.update', $coupon->id) }}">
                                        @method('PUT')
                                        @csrf
                                        <div class="form-group">
                                            <label  class="text-bold" for="coupon_name">Coupon Name<span class="text-danger"></span></label>
                                            <div class="controls">
                                                <input class="form-control" id="coupon_name" type="text" name="coupon_name" value="{{ $coupon->coupon_name }}">
                                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-bold" for="coupon_discount">Coupon Discount(%) <span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <input id="coupon_discount" type="text" name="coupon_discount" class="form-control" value="{{ $coupon->coupon_discount }}">
                                                <strong class="text-danger">{{ $errors->first('coupon_discount') }}</strong>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-bold" for="coupon_validity">Coupon Validity Date <span class="text-danger">*</span></label>
                                            <div class="controls">
                                                <input
                                                    id="coupon_validity"
                                                    type="date"
                                                    name="coupon_validity"
                                                    class="form-control"
                                                    min="{{ Carbon\Carbon::now()->format('Y-m-d') }}"
                                                    value="{{ $coupon->coupon_validity }}">
                                                <strong class="text-danger">{{ $errors->first('coupon_validity') }}</strong>
                                            </div>
                                        </div>
                                        <div>
                                            <input type="submit" class="btn btn-rounded btn-primary" value="Update Coupon">
                                        </div>
                                    </form>
                                </div>
                            </div><!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
@endsection

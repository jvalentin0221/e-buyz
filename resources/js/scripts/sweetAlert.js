//Delete Alert
$(document).on("click", ".delete", function(e){
    e.preventDefault();
    let id = $(this).attr("id");

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    });

    swalWithBootstrapButtons.fire({
        title: 'Are you sure you want to delete your brand?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            setTimeout(function(){
                $('.' + id ).submit();
                }, 1500);
            swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            );
        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                ' Your brand is still active',
                'error'
            );
        }
    });
});

//Confirm Alert
$(function(){
    $(document).on('click','.confirmed',function(e){
        e.preventDefault();
        // let link = $(this).attr("href");
        let id = $(this).attr("id");

        Swal.fire({
            title: 'Are you sure you want to Confirm?',
            text: "Once Confirmed, You will no longer be able to use 'pending' status for the duration of the order. ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Confirm!'
        }).then((result) => {
            if (result.isConfirmed) {
                // window.location.href = link
                setTimeout(function(){
                    $('.' + id ).submit();
                }, 1500);
                Swal.fire(
                    'Your Order has been Confirmed!',
                    'Confirm Changes',
                    'success'
                )
            }
        })
    });
});

//Processing Alert
$(function(){
    $(document).on('click','.processing',function(e){
        e.preventDefault();
        // let link = $(this).attr("href");
        let id = $(this).attr("id");

        Swal.fire({
            title: 'Are you sure you want to process this order?',
            text: "Once Processed, You will no longer be able to use previous status for the duration of the order. ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Confirm!'
        }).then((result) => {
            if (result.isConfirmed) {
                // window.location.href = link
                setTimeout(function(){
                    $('.' + id ).submit();
                }, 1500);
                Swal.fire(
                    'Your Order has been Confirmed!',
                    'Confirm Changes',
                    'success'
                )
            }
        })
    });
});

//Picked Alert
$(function(){
    $(document).on('click','.picked',function(e){
        e.preventDefault();
        // let link = $(this).attr("href");
        let id = $(this).attr("id");

        Swal.fire({
            title: "Are you sure you want to change status to 'picked'?",
            text: "Once Confirmed, You will no longer be able to use previous status for the duration of the order. ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Confirm!'
        }).then((result) => {
            if (result.isConfirmed) {
                // window.location.href = link
                setTimeout(function(){
                    $('.' + id ).submit();
                }, 1500);
                Swal.fire(
                    'Your Order has been Confirmed!',
                    'Confirm Changes',
                    'success'
                )
            }
        })
    });
});

//Shipped Alert
$(function(){
    $(document).on('click','.shipped',function(e){
        e.preventDefault();
        // let link = $(this).attr("href");
        let id = $(this).attr("id");

        Swal.fire({
            title: "Are you sure you want to change status to 'shipped'?",
            text: "Once Confirmed, You will no longer be able to use previous status for the duration of the order. ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Confirm!'
        }).then((result) => {
            if (result.isConfirmed) {
                // window.location.href = link
                setTimeout(function(){
                    $('.' + id ).submit();
                }, 1500);
                Swal.fire(
                    'Your Order has been Confirmed!',
                    'Confirm Changes',
                    'success'
                )
            }
        })
    });
});

//Delivered Alert
$(function(){
    $(document).on('click','.delivered',function(e){
        e.preventDefault();
        // let link = $(this).attr("href");
        let id = $(this).attr("id");

        Swal.fire({
            title: "Are you sure you want to change status to 'delivered'?",
            text: "Once Confirmed, You will no longer be able to use previous status for the duration of the order. ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Confirm!'
        }).then((result) => {
            if (result.isConfirmed) {
                // window.location.href = link
                setTimeout(function(){
                    $('.' + id ).submit();
                }, 1500);
                Swal.fire(
                    'Your Order has been Confirmed!',
                    'Confirm Changes',
                    'success'
                )
            }
        })
    });
});

//Adds subcategory items to dropdown box
$(document).ready(function() {
    $('select[name="category_title"]').on('change', function(){
        let category_title = $(this).val();
        if(category_title) {
            $.ajax({
                url: "/admins/subcategory/ajax/"+category_title,
                type:"GET",
                dataType:"json",
                success:function(data) {
                    let d =$('select[name="subcategory_title"]').empty();
                    $.each(data, function(key, value){
                        $('select[name="subcategory_title"]').append('<option value="'+ value.subcategory_name_en +'">' + value.subcategory_name_en + '</option>');
                    });
                },
            });
        } else {
            alert('We cannot find your associated subcategories at this time.');
        }
    });

//Adds subsubcategory items to dropdown box
    $('select[name="subcategory_title"]').on('change', function(){
        let subcategory_title = $(this).val();
        if(subcategory_title) {
            $.ajax({
                url: "/admins/subsubcategory/ajax/"+subcategory_title,
                type:"GET",
                dataType:"json",
                success:function(data) {
                    $('select[name="subsubcategory_title"]').html('');
                    let d =$('select[name="subsubcategory_title"]').empty();
                    $.each(data, function(key, value){
                        $('select[name="subsubcategory_title"]').append('<option value="'+ value.subsubcategory_name_en +'">' + value.subsubcategory_name_en + '</option>');
                    });
                },
            });
        } else {
            alert('We cannot find your associated Sub-Subcategories at this time.');
        }
    });
});

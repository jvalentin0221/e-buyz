// Profile image change
function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            $('#showImage').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$('#image').change(function(){
    readURL(this);
});

//Product image change
function viewMainThumbNail(input){
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function(e){
            $('#mainThumbNail').attr('src',e.target.result).width(80).height(80);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$('#thumbnail').change(function(){
    viewMainThumbNail(this);
});

//Multiple Image load for products
$(document).ready(function(){
    $('#multiImg').on('change', function(){
        if (window.File && window.FileReader && window.FileList && window.Blob)
        {
            var data = $(this)[0].files; //this file data

            $.each(data, function(index, file){
                if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
                    var fRead = new FileReader();
                    fRead.onload = (function(file){
                        return function(e) {
                            var img = $('<img/>').addClass('thumb').attr('src', e.target.result) .width(80)
                                .height(80); //create image element
                            $('#preview_img').append(img);
                        };
                    })(file);
                    fRead.readAsDataURL(file);
                }
            });
        }else{
            alert("Your browser doesn't support File API!");
        }
    });
});

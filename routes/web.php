<?php

use App\Http\Controllers\AJAXController;
use App\Http\Controllers\Brand\BrandController;
use App\Http\Controllers\Cart\CartController;
use App\Http\Controllers\Checkout\CheckoutController;
use App\Http\Controllers\Company\AdminCompanyInfoController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\UserTrackOrderController;
use App\Http\Controllers\Cancel\CancelController;
use App\Http\Controllers\Category\CategoryController;
use App\Http\Controllers\Coupon\CouponController;
use App\Http\Controllers\Dashboard\AdminPasswordController;
use App\Http\Controllers\Dashboard\AdminProfileController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\ManageProduct\ManageProductController;
use App\Http\Controllers\Product\AdminProductController;
use App\Http\Controllers\Payment\CashOnDeliveryController;
use App\Http\Controllers\Payment\StripeController;
use App\Http\Controllers\Order\AdminOrderController;
use App\Http\Controllers\Order\OrderStatusController;
use App\Http\Controllers\Order\UserOrderController;
use App\Http\Controllers\Profile\UserController;
use App\Http\Controllers\Profile\UserPasswordController;
use App\Http\Controllers\Profile\UserProfileController;
use App\Http\Controllers\PublishReview\PublishReviewController;
use App\Http\Controllers\Return\ReturnController;
use App\Http\Controllers\Review\ReviewController;
use App\Http\Controllers\Slider\SliderController;
use App\Http\Controllers\SubCategory\SubCategoryController;
use App\Http\Controllers\SubSubCategory\SubSubCategoryController;
use App\Services\CartService;
use App\Services\CouponService;
use App\Services\OrderService;
use App\Services\ManageProductService;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Dashboard\AdminController;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/',[IndexController::class, 'index'])->name('index');
Route::get('/products/{slug}',[IndexController::class, 'show'])->name('index.show');
Route::post('/products/{slug}',[ReviewController::class, 'store'])->name('review.store')->middleware('IsUser');
Route::get('/products/tags/{tag}',[IndexController::class, 'showProductTags'])->name('index.product_tags');
Route::get('/subcategories/{name}/{slug}',[IndexController::class, 'showSubCategories'])->name('index.subcategories');
Route::get('/subsubcategories/{name}/{slug}',[IndexController::class, 'showSubSubCategories'])->name('index.sub_subcategories');
Route::get('/search',[SearchController::class, 'searchProducts'])->name('search.searchProducts');
//User Verification
Route::group(['middleware' => 'auth'], function(){
    Route::get('/email/verify', function () {
        return view('auth.verify');
    })->name('verification.notice');

    Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
        $request->fulfill();
        return redirect('/');
    })->middleware('signed')->name('verification.verify');

    Route::post('/email/verification-notification', function (\Illuminate\Http\Request $request) {
        $request->user()->sendEmailVerificationNotification();
        return back()->with('message', 'Verification link sent!');
    })->middleware( 'throttle:6,1')->name('verification.send');
});


Route::group(['middleware' => ['auth', 'IsUser', 'verified']], function(){
    Route::get('/cart',[CartController::class, 'index'])->name('cart.index');
    Route::post('/cart/add-to-cart',[CartService::class, 'addToCart'])->name('cart.add_to_cart');
    Route::put('/cart',[CartController::class, 'update'])->name('cart.update');
    Route::delete('/cart',[CartController::class, 'delete'])->name('cart.delete');
    Route::post('cart/add-coupon',[CouponService::class, 'addCoupon'])->name('coupon.add_coupon');
    Route::delete('cart/remove-coupon',[CouponService::class, 'removeCoupon'])->name('coupon.remove_coupon');
    Route::get('/checkout',[CheckoutController::class, 'index'])->name('checkout.index');
    Route::post('/checkout',[CheckoutController::class, 'store'])->name('checkout.store');
    Route::get('/checkout/stripe',[StripeController::class, 'index'])->name('stripe.index');
    Route::post('/checkout/stripe',[StripeController::class, 'store'])->name('stripe.store');
    Route::get('/checkout/cash-on-delivery',[CashOnDeliveryController::class, 'index'])->name('cod.index');
    Route::post('/checkout/cash-on-delivery',[CashOnDeliveryController::class, 'store'])->name('cod.store');
});

Route::group(['prefix' => 'admins', 'middleware' => ['auth', 'IsAdmin','verified']], function(){
    Route::get('dashboard',[AdminController::class, 'index'])->name('admin.dashboard');

    Route::group(['middleware' => 'OnlyViewMyProfile'], function(){
        //Profile routes
        Route::get('profiles/{user}',[AdminProfileController::class, 'index'])->name('admin.profile');
        Route::get('profiles/{user}/edit',[AdminProfileController::class, 'edit'])->name('admin.profile.edit');
        Route::post('profiles/{user}',[AdminProfileController::class, 'update'])->name('admin.profile.update');
        //Password routes
        Route::get('passwords/{user}',[AdminPasswordController::class, 'index'])->name('admin.password.index');
        Route::put('passwords/{user}',[AdminPasswordController::class, 'update'])->name('admin.password.update');
    });
    //Brand routes
    Route::resource('brands', BrandController::class)->except(['create', 'show']);
    //Category routes
    Route::resource('categories', CategoryController::class)->except(['create', 'show']);
    //Subcategory routes
    Route::resource('subcategories', SubCategoryController::class)->except(['create', 'show']);
    Route::get('subcategory/ajax/{category_title}', [AJAXController::class, 'getSubCategory'])->name('admin.ajax.get_sub_category');
    //Sub_subcategory routes
    Route::resource('subsubcategories', SubSubCategoryController::class)->except(['create', 'show']);
    Route::get('subsubcategory/ajax/{category_title}', [AJAXController::class, 'getSubSubCategory'])->name('admin.ajax.get_sub_sub_category');
    //Product routes
    Route::get('products',[AdminProductController::class, 'index'])->name('admin.product.index');
    Route::post('products',[AdminProductController::class, 'store'])->name('admin.product.store');
    //Manage product routes
    Route::resource('manage-products', ManageProductController::class)->except(['create', 'show','store']);
    //Manage product Service
    Route::put('manage-products/update-multi-img/{product}',
        [ManageProductService::class, 'updateMultiImg'])
        ->name('admin.manage-product.update-multi-img');
    Route::put('manage-products/update-img/{product}',
        [ManageProductService::class, 'updateProductThumbnail'])
        ->name('admin.manage-product.update-thumbnail');
    Route::delete('manage-products/update-img/delete/{id}',
        [ManageProductService::class, 'deleteMultiImg'])
        ->name('admin.manage-product.delete-multi-img');
    //Slider routes
    Route::resource('sliders', SliderController::class)->except(['create', 'show']);
    //Coupon routes
    Route::resource('coupons', CouponController::class)->except(['create', 'show']);
    //Order routes
    Route::get('orders',[AdminOrderController::class, 'index'])->name('admin.order.index');
    Route::get('orders/{order}',[AdminOrderController::class, 'show'])->name('admin.order.show');
    Route::get('orders/confirmed/orders',[OrderService::class, 'confirmedOrders'])->name('admin.order.confirmed_orders');
    Route::get('orders/processing/orders',[OrderService::class, 'processingOrders'])->name('admin.order.processing_orders');
    Route::get('orders/picked/orders',[OrderService::class, 'pickedOrders'])->name('admin.order.picked_orders');
    Route::get('orders/shipped/orders',[OrderService::class, 'shippedOrders'])->name('admin.order.shipped_orders');
    Route::get('orders/delivered/orders',[OrderService::class, 'deliveredOrders'])->name('admin.order.delivered_orders');
    Route::get('orders/returned/orders',[OrderService::class, 'returnedOrders'])->name('admin.order.returned_orders');
    Route::get('orders/cancelled/orders',[OrderService::class, 'cancelledOrders'])->name('admin.order.cancel_orders');
    //Order Status routes
    Route::put('orders/pending/confirmed/{order}',[OrderStatusController::class, 'pendingToConfirmed'])->name('admin.order_status.pending_to_confirmed');
    Route::put('orders/confirmed/processing/{order}',[OrderStatusController::class, 'confirmedToProcessing'])->name('admin.order_status.confirmed_to_processing');
    Route::put('orders/processing/picked/{order}',[OrderStatusController::class, 'processingToPicked'])->name('admin.order_status.processing_to_picked');
    Route::put('orders/picked/shipped/{order}',[OrderStatusController::class, 'pickedToShipped'])->name('admin.order_status.picked_to_shipped');
    Route::put('orders/shipped/delivered/{order}',[OrderStatusController::class, 'shippedToDelivered'])->name('admin.order_status.shipped_to_delivered');
    Route::put('orders/return-request/returned/{order}',[OrderStatusController::class, 'returnRequestToReturned'])->name('admin.order_status.returned');
    //Company info routes
    Route::get('company-info/{company_info}/edit',[AdminCompanyInfoController::class, 'edit'])->name('admin.company_info.edit');
    Route::put('company-info/{company_info}',[AdminCompanyInfoController::class, 'update'])->name('admin.company_info.update');
    //Review routes
    Route::get('reviews',[ReviewController::class, 'index'])->name('admin.review.index');
    Route::put('reviews/{review}',[ReviewController::class, 'update'])->name('admin.review.update');
    Route::get('published-reviews',[PublishReviewController::class, 'index'])->name('admin.publish_review.index');
    Route::delete('published-reviews/{review}',[PublishReviewController::class, 'destroy'])->name('admin.publish_review.destroy');

    Route::get('logout', function(){
        Session::flush();
        Auth::logout();
        return Redirect::to("/");
    })->name('admin.logout');
});

Route::group(['prefix' => 'users', 'middleware' => ['auth', 'IsUser','verified','OnlyViewMyProfile']], function(){
    //Profile Routes
    Route::get('profiles/{user}',[UserController::class, 'index'])->name('user.profile');
    Route::get('profiles/{user}/edit',[UserProfileController::class, 'index'])->name('user.profile.edit');
    Route::post('profiles/{user}',[UserProfileController::class, 'update'])->name('user.profile.update');
    Route::get('profiles/password/{user}',[UserPasswordController::class, 'index'])->name('user.password.index');
    Route::put('profiles/password/{user}',[UserPasswordController::class, 'update'])->name('user.password.update');
});

Route::group(['prefix' => 'users', 'middleware' => ['auth', 'IsUser','verified']], function(){
    Route::get('orders',[UserOrderController::class, 'index'])->name('user.order.index');
    Route::get('cancel/orders',[CancelController::class, 'index'])->name('user.cancel.index');
    Route::get('tracking',[UserTrackOrderController::class, 'index'])->name('user.track.index');
    Route::post('tracking',[UserTrackOrderController::class, 'checkInvoiceCode'])->name('user.track.check_invoice_code');
    Route::get('returns/orders/',[ReturnController::class, 'index'])->name('user.return.index');
});
Route::group(['prefix' => 'users', 'middleware' => ['auth', 'IsUser','verified','OnlyViewMyOrders']], function(){
    Route::get('orders/{order}',[UserOrderController::class, 'show'])->name('user.order.show');
    Route::get('orders/invoice/{order}',[UserOrderController::class, 'createInvoice'])->name('user.order.create_invoice');
    Route::post('returns/orders/{order}',[ReturnController::class, 'update'])->name('user.return.update');
    Route::put('cancel/orders/{order}',[CancelController::class, 'update'])->name('user.cancel.update');
});

Route::get('logout', function(){
    Session::flush();
    Auth::logout();
    return Redirect::to("/");
})->name('user.logout');

//Multi-language routes
Route::get('language/english',[LanguageController::class, 'english'])->name('language.english');
Route::get('language/spanish',[LanguageController::class, 'spanish'])->name('language.spanish');

<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    public function test_Login_Redirects_To_User_Profile()
    {
       $user = User::factory()->create([
            'email' => 'user@gmail.com',
            'password' => bcrypt('user1234'),
            'role' => 'user'
        ]);

        $response = $this->post('/login', ['email' => 'user@gmail.com', 'password' => 'user1234']);

        $response->assertRedirect('/users/profiles/' . $user->id);
    }

    public function test_Registration_Redirects_To_User_Profile()
    {
        $user = [
            'name' => 'user',
            'email' => 'user@gmail.com',
            'password' => 'user1234',
            'password_confirmation' => 'user1234',
            'role' => 'user'
        ];

        $response = $this->post('/register', $user);

        $response->assertStatus(302);
        $response->assertRedirect('/users/profiles/1');
    }

    public function test_Not_Logged_In_User_Cant_Access_Admin_Dashboard()
    {
        $response = $this->get('/admins/dashboard/');

        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    public function test_Not_Logged_In_User_Cant_Access_User_Profile()
    {
        $user = User::factory()->create([]);

        $response = $this->get('/users/profiles/' . $user->id);

        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }
}

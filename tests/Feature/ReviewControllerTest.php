<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Product;
use App\Models\Review;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ReviewControllerTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $review;
    public object $product;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        Category::factory()->create();
        $this->product = Product::factory()->create();

        $this->review = Review::factory()->create();
    }

    public function test_User_Can_View_Pending_Reviews()
    {
        $response =  $this->actingAs($this->user)->get('/admins/reviews');

        $response->assertStatus(200);
    }

    public function test_User_Can_Store_Reviews()
    {
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);

        $response =  $this->actingAs($this->user)->post('/products/' . $this->product->product_slug_en,$this->review->toArray() + [
                'product_slug_en' => $this->product->product_slug_en
            ]);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseCount('reviews',2 );
        $response->assertRedirect('/products/' . $this->product->product_slug_en);
    }

    public function test_Admin_Can_Approve_Review()
    {
        $response =  $this->actingAs($this->user)->put('admins/reviews/' . $this->review->id, [
            'status' => 1
        ]);

        $this->assertDatabaseHas('reviews',['status' => 1]);
        $response->assertRedirect('admins/reviews');
    }
}

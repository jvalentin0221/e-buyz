<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductExtension;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AdminProductTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $category;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->category = Category::factory()->create();
    }

    public function test_Admin_Can_Access_Add_Product_Page()
    {
        $response = $this->actingAs($this->user)->get('/admins/products/');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Store_Product()
    {
        $file = UploadedFile::fake()->image('test.jpg');
        Storage::fake('public');

        $product = Product::factory()->create([
            'product_thumbnail' => $file
        ]);
        $productExtension = ProductExtension::factory()->create([
            'product_id' => $product->id
        ]);

        $response =  $this->actingAs($this->user)->post('/admins/products/',
            $product->toArray() + $productExtension->toArray() + [
                'multi_img' => [$file,$file]
            ]);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('products',$product->toArray());
        $this->assertDatabaseHas('product_extensions',$productExtension->toArray());
        $this->assertDatabaseHas('multi_imgs',[
            'photo_name' => [$file,$file],
        ]);
        $response->assertRedirect('admins/products/');
    }
}

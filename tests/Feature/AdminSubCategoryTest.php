<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\SubCategory;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AdminSubCategoryTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $category;
    public object $subcategory;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->category = Category::factory()->create();
        $this->subcategory = SubCategory::factory()->create();

    }

    public function test_Admin_Can_Access_Subcategory_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/subcategories/');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Subcategory_Edit_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/subcategories/' . $this->subcategory->id . '/edit');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Store_New_Subcategory()
    {
        $response =  $this->actingAs($this->user)->post('/admins/subcategories/', $this->subcategory->toArray());

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('sub_categories', $this->subcategory->toArray());
        $response->assertRedirect('admins/subcategories/');
    }

    public function test_Admin_Can_Edit_Existing_Subcategory()
    {
        $response =  $this->actingAs($this->user)->put('admins/subcategories/' . $this->subcategory->id, $this->subcategory->toArray());

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('sub_categories',$this->subcategory->toArray());
        $response->assertRedirect('admins/subcategories/');
    }

    public function test_Admin_Can_Delete_Existing_Subcategory()
    {
        $response =  $this->actingAs($this->user)->delete('admins/subcategories/' . $this->subcategory->id);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseCount('sub_categories', '0');
        $response->assertRedirect('admins/subcategories/');
    }
}

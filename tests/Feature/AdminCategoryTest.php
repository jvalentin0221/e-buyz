<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminCategoryTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $category;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->category = Category::factory()->create();
    }

    public $input = [
        'category_name_en' => 'toys',
        'category_name_es' => 'juguetes',
        'category_slug_en' => 'toys',
        'category_slug_es' => 'juguetes',
        'category_icon' => 'fa fa-car'
    ];

    public function test_Admin_Can_Access_Category_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/categories/');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Category_Edit_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/categories/' . $this->category->id . '/edit');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Store_New_Category()
    {
        $response =  $this->actingAs($this->user)->post('/admins/categories/', $this->input);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('categories', $this->input);
        $this->assertDatabaseCount('categories', 2);
        $response->assertRedirect('admins/categories/');
    }

    public function test_Admin_Can_Update_Existing_Category()
    {
        $response =  $this->actingAs($this->user)->put('admins/categories/' . $this->category->id, $this->input);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('categories', $this->input);
        $response->assertRedirect('admins/categories/');
    }

    public function test_Admin_Can_Delete_Existing_Category()
    {
        $response =  $this->actingAs($this->user)->delete('admins/categories/' . $this->category->id);

        $this->assertDatabaseCount('categories', '0');
        $response->assertRedirect('admins/categories/');
    }
}

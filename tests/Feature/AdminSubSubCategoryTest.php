<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AdminSubSubCategoryTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $category;
    public object $subcategory;
    public object $subSubcategory;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->category = Category::factory()->create();
        $this->subcategory = SubCategory::factory()->create();
        $this->subSubcategory  = SubSubCategory::factory()->create();
    }

    public function test_Admin_Can_Access_SubSubcategory_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/subsubcategories/');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_SubSubcategory_Edit_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/subsubcategories/' . $this->subSubcategory->id . '/edit');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Store_New_SubSubcategory()
    {
        $response =  $this->actingAs($this->user)->post('/admins/subsubcategories/', $this->subSubcategory->toArray());

        $this->assertDatabaseHas('sub_sub_categories', $this->subSubcategory->toArray());
        $response->assertRedirect('admins/subsubcategories/');
    }

    public function test_Admin_Can_Edit_Existing_SubSubcategory()
    {
        $response =  $this->actingAs($this->user)->put('admins/subsubcategories/' . $this->subSubcategory->id, $this->subSubcategory->toArray());

        $this->assertDatabaseHas('sub_sub_categories', $this->subSubcategory->toArray());
        $response->assertRedirect('admins/subsubcategories/');
    }

    public function test_Admin_Can_Delete_Existing_Subcategory()
    {
        $response =  $this->actingAs($this->user)->delete('admins/subsubcategories/' . $this->subSubcategory->id);

        $this->assertDatabaseCount('sub_sub_categories', 0);
        $response->assertRedirect('admins/subsubcategories/');
    }
}

<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use Tests\TestCase;

class AdminProfileTest extends TestCase
{
    use DatabaseMigrations;

    public function test_Admin_Can_Access_Profile_Page()
    {
        $user = User::factory()->create([
            'role' => 'admin'
        ]);

        $response =  $this->actingAs($user)->get('/admins/profiles/' . $user->id);

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Edit_Profile_Page()
    {
        $user = User::factory()->create([
            'role' => 'admin'
        ]);

        $response =  $this->actingAs($user)->get('/admins/profiles/' . $user->id . '/edit');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Update_Profile()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create([
            'name' => 'Joel',
            'role' => 'admin'
        ]);

        $userInput = [
            'name' => 'John',
            'email' => 'admin@test.com'
        ];

        $response =  $this->actingAs($user)->post('admins/profiles/' . $user->id, $userInput);


        $response->assertStatus(302);
        $this->assertDatabaseHas('users', [
            'name' => 'John',
            'email' => 'admin@test.com'
        ]);
        $response->assertRedirect('/admins/profiles/' . $user->id);
    }

    public function test_Name_Field_Is_Required_To_Edit_Profile()
    {
        $user = User::factory()->create([
            'name' => 'admin',
            'role' => 'admin'
        ]);

        $userInput = [
            'name' => '',
            'email' => 'admin@test.com'
        ];

        $response =  $this->actingAs($user)->post('/admins/profiles/' . $user->id, $userInput);

        $response->assertSessionHasErrors(['name']);
    }

    public function test_Name_Field_Max_Characters_Is_100()
    {
        $user = User::factory()->create([
            'name' => 'admin',
            'role' => 'admin'
        ]);

        $userInput = [
            'name' => Str::random(101),
            'email' => 'admin@test.com'
        ];

        $response =  $this->actingAs($user)->post('/admins/profiles/' . $user->id, $userInput);

        $response->assertSessionHasErrors(['name']);
    }

    public function test_Email_Field_Is_Required_To_Edit_Profile()
    {
        $user = User::factory()->create([
            'name' => 'admin',
            'role' => 'admin'
        ]);

        $userInput = [
            'name' => 'admin',
            'email' => ''
        ];

        $response =  $this->actingAs($user)->post('/admins/profiles/' . $user->id, $userInput);

        $response->assertSessionHasErrors(['email']);
    }

    public function test_Email_Field_Max_Characters_Is_100()
    {
        $user = User::factory()->create([
            'name' => 'admin',
            'role' => 'admin'
        ]);

        $userInput = [
            'name' => 'admin',
            'email' => Str::random(101)
        ];

        $response =  $this->actingAs($user)->post('/admins/profiles/' . $user->id, $userInput);

        $response->assertSessionHasErrors(['email']);
    }
}

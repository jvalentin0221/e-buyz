<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CancelControllerTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $order;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);
        $this->order = Order::factory()->create();
    }

    public function test_User_Can_View_Cancelled_Orders()
    {
        $response =  $this->actingAs($this->user)->get('/users/cancel/orders');

        $response->assertStatus(200);
    }
}

<?php

namespace Tests\Feature;

use App\Models\Coupon;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AdminCouponTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $coupon;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->coupon = Coupon::factory()->create();
    }

    public function test_Admin_Can_Access_Coupon_Page()
    {
        $response = $this->actingAs($this->user)->get('/admins/coupons/');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Store_Coupon_Page()
    {
        $coupon = Coupon::factory()->make();
        $response = $this->actingAs($this->user)->post('/admins/coupons/', $coupon->toArray());

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('coupons',$coupon->toArray());
        $this->assertDatabaseCount('coupons', 2);
        $response->assertRedirect('admins/coupons/');
    }

    public function test_Admin_Can_Access_Edit_Coupon_Page()
    {
        $response = $this->actingAs($this->user)->get('/admins/coupons/' . $this->coupon->id . '/edit');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Update_Coupon_Page()
    {
        $response = $this->actingAs($this->user)->put('/admins/coupons/' . $this->coupon->id, $this->coupon->toArray());

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('coupons',$this->coupon->toArray());
        $response->assertRedirect('admins/coupons/');
    }

    public function test_Admin_Can_Delete_Coupon()
    {
        $response = $this->actingAs($this->user)->delete('/admins/coupons/' . $this->coupon->id);

        $this->assertDatabaseCount('coupons', 0);
        $response->assertRedirect('admins/coupons/');
    }
}

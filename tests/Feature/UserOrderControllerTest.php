<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UserOrderControllerTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $order;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);
        $this->order = Order::factory()->create();

    }

    public function test_User_Can_Access_User_Orders_Page()
    {
        $response =  $this->actingAs($this->user)->get('/users/orders/');
        $response->assertStatus(200);
    }

    public function test_User_Can_Access_View_Order_Page()
    {
        $response =  $this->actingAs($this->user)->get('/users/orders/' . $this->order->id);
        $response->assertStatus(200);
    }
}

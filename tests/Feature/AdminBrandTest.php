<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

class AdminBrandTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->brand = Brand::factory()->create();
    }

    public function test_Admin_Can_Access_Brand_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/brands/');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Brand_Edit_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/brands/' . $this->brand->id . '/edit');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Store_New_Brand()
    {
        $file = UploadedFile::fake()->image('test.jpg');
        Storage::fake('public');

        $input = [
            'brand_name_en' => 'Apple',
            'brand_name_es' => 'Apple',
            'brand_slug_en' => 'Apple test',
            'brand_slug_es' => 'Apple test',
            'brand_url' => 'facebook.com',
            'brand_image' => $file
        ];

        $response =  $this->actingAs($this->user)->post('/admins/brands/',$input);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('brands', [
            'brand_name_en' => 'Apple',
            'brand_name_es' => 'Apple',
            'brand_slug_en' => 'apple',
            'brand_slug_es' => 'apple',
            'brand_image' => $file
        ]);

        $response->assertRedirect('/admins/brands/');
    }

    public function test_Admin_Can_Update_Existing_Brand()
    {
        $input = [
            'brand_name_en' => 'test',
            'brand_name_es' => 'test',
        ];

        $response =  $this->actingAs($this->user)->put('/admins/brands/' . $this->brand->id,$input);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('brands', [
            'brand_name_en' => 'test',
            'brand_name_es' => 'test',
        ]);
        $response->assertRedirect('admins/brands/');
    }

    public function test_Admin_Can_Delete_Existing_Brand()
    {

        $response =  $this->actingAs($this->user)->delete('admins/brands/' . $this->brand->id);

        $this->assertDatabaseCount('brands', '0');
        $response->assertRedirect('admins/brands/');
    }
}

<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserProfileTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);
    }


    public function test_User_Can_Access_User_Profile_Page()
    {

        $response =  $this->actingAs($this->user)->get('/users/profiles/' . $this->user->id);
        $response->assertStatus(200);
    }

    public function test_User_Can_Access_Profile_Edit_Page()
    {


        $response =  $this->actingAs($this->user)->get('/users/profiles/' . $this->user->id . '/edit');

        $response->assertStatus(200);
    }

    public function test_User_Can_Update_Profile()
    {
        $response =  $this->actingAs($this->user)->post('/users/profiles/' . $this->user->id, $this->user->toArray());

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('users', $this->user->toArray());
        $response->assertRedirect('/users/profiles/' . $this->user->id);
    }

    public function test_Name_Field_Is_Required_To_Edit_User_Profile()
    {
        $userInput = [
            'name' => '',
            'email' => 'admin@test.com'
        ];

        $response =  $this->actingAs($this->user)->post('/users/profiles/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['name']);
    }

    public function test_Name_Field_Max_Characters_User_Profile_Is_100()
    {
        $userInput = [
            'name' => Str::random(101),
            'email' => 'user@test.com'
        ];

        $response =  $this->actingAs($this->user)->post('/users/profiles/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['name']);
    }

    public function test_Email_Field_Is_Required_To_Edit_User_Profile()
    {
        $userInput = [
            'name' => 'admin',
            'email' => ''
        ];

        $response =  $this->actingAs($this->user)->post('/users/profiles/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['email']);
    }

    public function test_Email_Field_For_User_Profile_Max_Characters_Is_100()
    {
        $userInput = [
            'name' => 'admin',
            'email' => Str::random(101)
        ];

        $response =  $this->actingAs($this->user)->post('/users/profiles/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['email']);
    }
}

<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AdminOrderControllerTest extends TestCase
{
    use DatabaseMigrations;

    public $user;
    public $order;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->order = Order::factory()->create();
    }

    public function test_Admin_Can_Access_Pending_Orders()
    {
        $response = $this->actingAs($this->user)->get('/admins/orders/');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Confirmed_orders()
    {
        $response = $this->actingAs($this->user)->get('/admins/orders/confirmed/orders');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Processing_orders()
    {
        $response = $this->actingAs($this->user)->get('/admins/orders/processing/orders');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Picked_Orders()
    {
        $response = $this->actingAs($this->user)->get('/admins/orders/picked/orders');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Delivered_Orders()
    {
        $response = $this->actingAs($this->user)->get('/admins/orders/delivered/orders');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Shipped_Orders()
    {
        $response = $this->actingAs($this->user)->get('/admins/orders/shipped/orders');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Cancelled_orders()
    {
        $response = $this->actingAs($this->user)->get('/admins/orders/cancelled/orders');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Returned_Orders()
    {
        $response = $this->actingAs($this->user)->get('/admins/orders/returned/orders');

        $response->assertStatus(200);
    }
}

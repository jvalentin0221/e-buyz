<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\MultiImg;
use App\Models\Product;
use App\Models\ProductExtension;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AdminManageProductTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $category;
    public object $product;
    public object $productExtension;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->category = Category::factory()->create();
        $this->product = Product::factory()->create();
        $this->productExtension = ProductExtension::factory()->create([
            'product_id' => $this->product->id
        ]);
    }

    public function test_Admin_Can_Access_Manage_Product_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/manage-products/');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Access_Manage_Product_Edit_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/manage-products/' . $this->product->id . '/edit');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Update_Product()
    {
        $response =  $this->actingAs($this->user)
            ->put('/admins/manage-products/' . $this->product->id, $this->product->toArray() + $this->productExtension->toArray()
        );

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('products',[
            'product_name_en' => $this->product->product_name_en,
            'product_qty' => $this->product->product_qty,
        ]);
        $this->assertDatabaseHas('product_extensions',[
            'product_code' => $this->productExtension->product_code,
            'short_description_en' => $this->productExtension->short_description_en
        ]);
        $response->assertRedirect('admins/manage-products/');
    }

    public function test_Admin_Can_Delete_Product()
    {
        $multiImg = MultiImg::factory()->create([
            'product_id' => $this->product->id
        ]);

        $response =  $this->actingAs($this->user)
            ->delete('/admins/manage-products/' . $this->product->id);


        $response->assertSessionHasNoErrors();
        $this->assertDatabaseCount('product_extensions', 0);
        $this->assertDatabaseCount('multi_imgs', 0);
        $response->assertRedirect('admins/manage-products/');
    }
}

<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AdminPasswordTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin',
            'password' => bcrypt('admin1234'),
        ]);
    }

    public function test_Admin_Can_Access_Change_Password_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/passwords/' . $this->user->id);
        $response->assertStatus(200);
    }

    public function test_Admin_Can_Update_Password()
    {
        $userInput = [
            'old_password' => 'admin1234',
            'new_password' => 'admin12345',
            'password_confirmation' => 'admin12345'
        ];

        $response =  $this->actingAs($this->user)->put('/admins/passwords/' . $this->user->id, $userInput);

        $response->assertStatus(302);
        $response->assertRedirect('/admins/dashboard/');

    }

    public function test_Admin_Old_Password_Field_Is_Required()
    {
        $userInput = [
            'old_password' => '',
        ];

        $response =  $this->actingAs($this->user)->put('/admins/passwords/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['old_password']);
    }

    public function test_Admin_New_Password_Field_Is_Required()
    {
        $userInput = [
            'old_password' => 'admin1234',
            'new_password' => ''
        ];

        $response =  $this->actingAs($this->user)->put('/admins/passwords/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['new_password']);
    }

    public function test_Admin_Password_Confirmation_Has_To_Have_The_Same_Exact_String()
    {
        $userInput = [
            'old_password' => 'admin1234',
            'new_password' => 'admin12345',
            'password_confirmation' => 'error1234'
        ];

        $response =  $this->actingAs($this->user)->put('/admins/passwords/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['password_confirmation']);
    }

    public function test_Admin_New_Password_And_Password_Confirmation_Has_A_Minimum_of_8_Characters()
    {
        $userInput = [
            'old_password' => 'admin1234',
            'new_password' => 'admin',
            'password_confirmation' => 'admin'
        ];

        $response =  $this->actingAs($this->user)->put('/admins/passwords/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['password_confirmation']);
    }
}

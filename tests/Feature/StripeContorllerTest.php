<?php

namespace Tests\Feature;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductExtension;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class StripeControllerTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $cart;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productExtension = ProductExtension::factory()->create([
            'product_id' => $product->id
        ]);
        $this->cart = Cart::factory()->create();

    }

    public function test_User_Can_Access_Stripe_page()
    {
        $response = $this->actingAs($this->user)->get('/checkout/stripe');

        $response->assertStatus(200);
    }
}

<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UserReturnControllerTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $order;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);
        $this->order = Order::factory()->create();
    }

    public function test_User_Can_View_Return_Orders()
    {
        $response =  $this->actingAs($this->user)->get('/users/returns/orders');

        $response->assertStatus(200);
    }

    public function test_User_Can_Update_Return_Order()
    {
        $response =  $this->actingAs($this->user)->post('/users/returns/orders/' . $this->order->id, [
            'return_reason' => 'Wrong product',
        ]);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('orders', [
            'return_date' => Carbon::now()->format('F d Y'),
            'return_reason' => 'Wrong product',
        ]);
        $response->assertRedirect('users/returns/orders/');
    }
}

<?php

namespace Tests\Feature;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductExtension;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CartTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $cart;


    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productExtension = ProductExtension::factory()->create([
            'product_id' => $product->id
        ]);
        $this->cart = Cart::factory()->create();
    }

    public function test_User_Can_Access_My_Cart()
    {
        $response = $this->actingAs($this->user)->get('/cart');

        $response->assertStatus(200);
    }

    public function test_User_Can_Add_To_My_Cart()
    {
        $cart = Cart::factory()->create();
        $response = $this->actingAs($this->user)->post('/cart/add-to-cart', $cart->toArray());

        $this->assertDatabaseHas('carts',$cart->toArray());
        $response->assertSessionHasNoErrors();
        $response->assertRedirect('/');
    }

    public function test_User_Can_Update_On_Add_To_Cart_Page()
    {
        $cartId = $this->cart->id;
        $response = $this->actingAs($this->user)->put('/cart', $this->cart->toArray() + [
            'cart_id' => $cartId
        ]);

        $this->assertDatabaseHas('carts',$this->cart->toArray());
        $response->assertSessionHasNoErrors();
        $response->assertRedirect('/cart');
    }

    public function test_User_Can_Delete_On_Add_To_Cart_Page()
    {
        $cartId = $this->cart->id;
        $response = $this->actingAs($this->user)->delete('/cart',[
                'cart_id' => $cartId
            ]);

        $this->assertDatabaseCount('carts', '0');
        $response->assertRedirect('/cart');
    }
}

<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Product;
use App\Models\Review;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PublishReviewControllerTest extends TestCase
{
    use DatabaseMigrations;

    public $user;
    public $review;
    public $product;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        Category::factory()->create();
        $this->product = Product::factory()->create();

        $this->review = Review::factory()->create();
    }

    public function test_User_Can_View_Published_Reviews()
    {
        $response =  $this->actingAs($this->user)->get('/admins/published-reviews');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Delete_Published_Review()
    {
        $response =  $this->actingAs($this->user)->delete('admins/published-reviews/' . $this->review->id);

        $this->assertDatabaseCount('reviews', 0);
        $response->assertRedirect('admins/published-reviews');
    }
}

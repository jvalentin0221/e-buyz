<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class OrderStatusControllerTest extends TestCase
{
    use DatabaseMigrations;

    public $user;
    public $order;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->order = Order::factory()->create();
    }

    public function test_Admin_Can_Change_Order_Status_To_Confirmed()
    {
        $response = $this->actingAs($this->user)->put('/admins/orders/pending/confirmed/' . $this->order->id);

        $this->assertDatabaseHas('orders',[
            'status' => 'confirmed'
        ]);
        $response->assertRedirect('admins/orders/');
    }

    public function test_Admin_Can_Change_Order_Status_To_Processing()
    {
        $response = $this->actingAs($this->user)->put('/admins/orders/confirmed/processing/' . $this->order->id);

        $this->assertDatabaseHas('orders',[
            'status' => 'processing'
        ]);
        $response->assertRedirect('admins/orders/confirmed/orders');
    }

    public function test_Admin_Can_Change_Order_Status_To_Picked()
    {
        $response = $this->actingAs($this->user)->put('/admins/orders/processing/picked/' . $this->order->id);

        $this->assertDatabaseHas('orders',[
            'status' => 'picked'
        ]);
        $response->assertRedirect('admins/orders/processing/orders');
    }

    public function test_Admin_Can_Change_Order_Status_To_Shipped()
    {
        $response = $this->actingAs($this->user)->put('/admins/orders/picked/shipped/' . $this->order->id);

        $this->assertDatabaseHas('orders',[
            'status' => 'shipped'
        ]);
        $response->assertRedirect('admins/orders/picked/orders');
    }

    public function test_Admin_Can_Change_Order_Status_To_Delivered()
    {
        $response = $this->actingAs($this->user)->put('/admins/orders/shipped/delivered/' . $this->order->id);

        $this->assertDatabaseHas('orders',[
            'status' => 'delivered'
        ]);
        $response->assertRedirect('admins/orders/picked/orders');
    }

    public function test_Admin_Can_Change_Order_Status_To_Returned()
    {
        $response = $this->actingAs($this->user)->put('/admins/orders/return-request/returned/' . $this->order->id);

        $this->assertDatabaseHas('orders',[
            'status' => 'returned'
        ]);
        $response->assertRedirect('admins/orders/returned/orders');
    }
}

<?php

namespace Tests\Feature;

use App\Models\CompanyInfo;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AdminCompanyInfoTest extends TestCase
{
    use DatabaseMigrations;

    public $user;


    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
    }

    public function test_Admin_Can_Access_Edit_Company_Info_Page()
    {
        $company = CompanyInfo::factory()->create();

        $response = $this->actingAs($this->user)->get('/admins/company-info/' . $company->id . '/edit');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Update_Company_Info()
    {
        $file = UploadedFile::fake()->image('test.jpg');
        Storage::fake('public');

        $company = CompanyInfo::factory()->create([
            'logo' => $file,
            'company_name' => 'E-buyz'
        ]);

        $response = $this->actingAs($this->user)->put('/admins/company-info/' . $company->id, $company->toArray());

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('company_infos',[
            'company_name' => 'E-buyz'
        ]);
        $response->assertRedirect('admins/dashboard/');
    }
}

<?php

namespace Tests\Feature;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductExtension;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CashOnDeliveryTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $cart;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productExtension = ProductExtension::factory()->create([
            'product_id' => $product->id
        ]);
        $this->cart = Cart::factory()->create();

    }

    public function test_User_Can_Place_Order_With_Cash_On_Delivery()
    {
        $this->withoutExceptionHandling();
        $order = Order::factory()->create();
        $orderItem = OrderItem::factory()->create();

        $response = $this->actingAs($this->user)
            ->post('/checkout/cash-on-delivery', $order->toArray() + $orderItem->toArray());

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseCount('orders', 2);
        $this->assertDatabaseCount('order_items',2);
        $response->assertRedirect('/');
    }
}

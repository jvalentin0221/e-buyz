<?php

namespace Tests\Feature;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductExtension;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CheckoutTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $cart;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productExtension = ProductExtension::factory()->create([
            'product_id' => $product->id
        ]);
        $this->cart = Cart::factory()->create();
    }

    public function test_User_Can_Checkout()
    {
        $response = $this->actingAs($this->user)->get('/checkout');

        $response->assertStatus(200);
    }

    public function test_User_Can_Checkout_To_Stripe()
    {
        $response = $this->actingAs($this->user)->post('/checkout', [
            'payment_method' => 'stripe',
            'shipping_name' => $this->user->name,
            'shipping_address' => $this->user->address,
            'shipping_email' => $this->user->email,
            'shipping_phone' => $this->user->phone_number,
            'zip_code' => $this->user->zip_code,
            'state' => $this->user->state,
            'city' => $this->user->city,
            'notes' => 'Ring door bell',
        ]);

        $response->assertSessionHasNoErrors();
        $response->assertRedirect('/checkout/stripe');
    }
}

<?php

namespace Tests\Feature;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use App\Models\User;
use App\Models\ProductExtension;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class IndexControllerTest extends TestCase
{
    use DatabaseMigrations;

    public object $user, $cart,$category,$subcategory,$product,$productExtension,$order,$orderItem,$subsubcategory;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);
        $this->category = Category::factory()->create();
        $this->subcategory = SubCategory::factory()->create();
        $this->subsubcategory = SubSubCategory::factory()->create();
        $this->product = Product::factory()->create();
        $this->productExtension = ProductExtension::factory()->create([
            'product_id' => $this->product->id
        ]);
        $this->order = Order::factory()->create();
        $this->orderItem = OrderItem::factory()->create();
        $this->cart = Cart::factory()->create();
    }

//    public function test_User_Can_Access_Search_Page()
//    {
//        $this->withoutExceptionHandling();
//        $response = $this->get('/search');
//
//        $response->assertStatus(200);
//    }

    public function test_User_Can_Access_Product_Show_page()
    {
        $response = $this->get('/products/' . $this->product->product_slug_en);

        $response->assertStatus(200);
    }

    public function test_User_Can_Access_Product_Tags_page()
    {
        $response = $this->get('/products/tags' . $this->product->product_tags_en);

        $response->assertStatus(200);
    }

    public function test_User_Can_Access_Subcategories_page()
    {
        $this->withoutExceptionHandling();
        $response = $this->get('/subcategories/' . $this->product->product_name_en . '/' . $this->product->product_slug_en);

        $response->assertStatus(200);
    }

    public function test_User_Can_Access_SubSubcategories_page()
    {
        $response = $this->get('/subsubcategories/' . $this->product->product_name_en . '/' . $this->product->product_slug_en);

        $response->assertStatus(200);
    }
}

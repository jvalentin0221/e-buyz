<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UserPasswordTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;


    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user',
            'password' => bcrypt('user1234'),
        ]);

    }

    public function test_User_Can_Access_Change_Password_Page()
    {
        $response =  $this->actingAs($this->user)->get('/users/profiles/password/' . $this->user->id);

        $response->assertStatus(200);
    }

    public function test_User_Can_Update_Password()
    {

        $userInput = [
            'old_password' => 'user1234',
            'new_password' => 'user12345',
            'password_confirmation' => 'user12345'
        ];

        $response =  $this->actingAs($this->user)->put('/users/profiles/password/' . $this->user->id, $userInput);

        $response->assertStatus(302);
        $response->assertRedirect('/users/profiles/' . $this->user->id);

    }

    public function test_User_Old_Password_Field_Is_Required()
    {
        $userInput = [
            'old_password' => '',
        ];

        $response =  $this->actingAs($this->user)->put('/users/profiles/password/' . $this->user->id, $userInput);
        $response->assertSessionHasErrors(['old_password']);
    }

    public function test_User_New_Password_Field_Is_Required()
    {
        $userInput = [
            'old_password' => 'user1234',
            'new_password' => ''
        ];

        $response =  $this->actingAs($this->user)->put('/users/profiles/password/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['new_password']);
    }

    public function test_User_Password_Confirmation_Has_To_Have_The_Same_Exact_String()
    {
        $userInput = [
            'old_password' => 'user1234',
            'new_password' => 'user12345',
            'password_confirmation' => 'error1234'
        ];

        $response =  $this->actingAs($this->user)->put('/users/profiles/password/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['password_confirmation']);
    }

    public function test_User_New_Password_And_Password_Confirmation_Has_A_Minimum_of_8_Characters()
    {
        $userInput = [
            'old_password' => 'user1234',
            'new_password' => 'user',
            'password_confirmation' => 'user'
        ];

        $response =  $this->actingAs($this->user)->put('/users/profiles/password/' . $this->user->id, $userInput);

        $response->assertSessionHasErrors(['password_confirmation']);
    }
}

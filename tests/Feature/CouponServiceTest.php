<?php

namespace Tests\Feature;

use App\Models\Coupon;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CouponServiceTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $coupon;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'user'
        ]);
        $this->coupon = Coupon::factory()->create();
    }

    public function test_User_Can_Add_Coupon()
    {
        $response = $this->actingAs($this->user)->post('/cart/add-coupon/',[
            $this->coupon->coupon_name
        ]);

        $response->assertSessionHasNoErrors();
        $response->assertRedirect('cart');
    }
}

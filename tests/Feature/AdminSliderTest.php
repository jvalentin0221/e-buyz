<?php

namespace Tests\Feature;

use App\Models\Slider;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AdminSliderTest extends TestCase
{
    use DatabaseMigrations;

    public object $user;
    public object $slider;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'role' => 'admin'
        ]);
        $this->slider = Slider::factory()->create();
    }

    public function test_Admin_Can_Access_Slider_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/sliders/');

        $response->assertStatus(200);
    }

    public function test_Admin_Can_Store_Slider()
    {
        $file = UploadedFile::fake()->image('test.jpg');
        Storage::fake('public');

       $this->slider->slider_img =$file;

        $response =  $this->actingAs($this->user)->post('/admins/sliders/', $this->slider->toArray());

        $response->assertSessionHasNoErrors();
        $response->assertRedirect('admins/sliders/');
        $this->assertDatabaseHas('sliders',[
            'title' => $this->slider->title,
            'description' => $this->slider->description,
            'slider_img' => $this->slider->slider_img,
        ]);
    }

    public function test_Admin_Can_Access_Edit_Slider_Page()
    {
        $response =  $this->actingAs($this->user)->get('/admins/sliders/' . $this->slider->id . '/edit');

        $response->assertStatus(200);
    }

//    public function test_Admin_Can_Update_Slider()
//    {
//        $file = UploadedFile::fake()->image('test.jpg');
//        Storage::fake('public');
//
//
//        $this->slider->slider_img = $file;
//        $this->slider->title = 'new title';
//
//
//        $response =  $this->actingAs($this->user)->put('/admins/sliders/' . $this->slider->id, $this->slider->toArray());
//
//        $response->assertSessionHasNoErrors();
//        $response->assertRedirect('admins/sliders/');
//        $this->assertDatabaseHas('sliders',[
//            'title' => 'new title',
//            'description' => $this->slider->description,
//        ]);
//    }

    public function test_Admin_Can_Delete_Slider()
    {
        $response =  $this->actingAs($this->user)->delete('admins/sliders/' . $this->slider->id);

        $this->assertDatabaseCount('sliders', '0');
        $response->assertRedirect('admins/sliders/');
    }

}

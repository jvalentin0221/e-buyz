const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/scripts/populator.js', 'public/js/populator.js')
    .js('resources/js/scripts/imageChange.js', 'public/js/imageChange.js')
    .js('resources/js/scripts/sweetAlert.js', 'public/js/sweetAlert.js')
    .js('resources/js/scripts/ckeditor.js', 'public/js/ckeditor.js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();

# E-Buyz
A E-commerce project with admin dashboard to create custom e-shop.

## General Info
### The creation for admins has been removed and only access to one admin will be availible:
* EMAIL-admin@ebuyz.com
* PASSWORD- ebuyzadmin1234

Please feel free to use the admin user credentials provided to navigate your way through the exciting Admin panel.

If you become fatigue from creating so much content as admin, you can register and become a user. This will give you the ability
purchase items from "Your" new shop with the no-limit "Developer" credit card. 
###
* CC#-4242 4242 4242 4242  with any date and CVC code.

## Technologies
* HTML 5
* CSS 3
* Bootstrap 4
* PHP 8
* Laravel 8
* Javascript/Jquery
* Redis
* MYSQL
* Apache
* Amazon EC2
* Amazon SES
* Amazon S3
* Jenkins
* PHPUnit
* Jira
* Bitbucket

## Features
### List of features ready and TODOs for future development
* Customizable website through Admin Dashboard
* Order Tracking System
* Coupon System
* Simple product creation
* Multi-lanuguage

## To-do list:
### List of features ready and TODOs for future development
* Admin user deletion
* Admin chart dashboard
* Receive 10% coupon for profile update--Completed

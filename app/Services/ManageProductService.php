<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminMultiImageUpdateRequest;
use App\Interfaces\ImageStorageInterface;
use App\Models\MultiImg;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ManageProductService extends Controller
{
    public object  $imageStorage;

    public function __construct(ImageStorageInterface $imageStorage)
    {
        $this->imageStorage = $imageStorage;
    }

    public function updateMultiImg(AdminMultiImageUpdateRequest $request):RedirectResponse
    {
        if($request->has('multi_img')) {
            $imgs = $request->file('multi_img');
            foreach ($imgs as $id => $img) {
                $multiImg = MultiImg::findOrFail($id);
                $this->imageStorage->delete($request,'multi_img',$multiImg,'photo_name',$multiImg->product_id);
                $this->imageStorage->resizeMultipleImages($request,'multi_img',500,500,$multiImg->product_id);
                MultiImg::where('id', $multiImg->id)->update([
                    'photo_name' => $img->getClientOriginalName()
                ]);
            }
            $message = $this->createToastrMessage('Your images has been updated','success');
            return redirect(route('manage-products.index', Auth::user()->id))->with($message);
        } else {
            $message = $this->createToastrMessage('No changes has been made to your images','info');
            return redirect(route('manage-products.index', Auth::user()->id))->with($message);
        }
    }

    public function updateProductThumbnail(Request $request, Product $product):RedirectResponse
    {
        if ($request->has('product_thumbnail')) {
            $this->imageStorage->delete($request, 'product_thumbnail', $product, 'product_thumbnail', $product->id);
            $this->imageStorage->resizeImage($request, 'product_thumbnail', 500, 500, $product->id);
            $product->update([
                'product_thumbnail' => $request->file('product_thumbnail')
            ]);

            $message = $this->createToastrMessage('Your images has been updated', 'success');
            return redirect(route('manage-product.index', Auth::user()->id))->with($message);
        } else {
            $message = $this->createToastrMessage('No changes has been made to your thumbnail', 'info');
            return redirect(route('manage-products.index', Auth::user()->id))->with($message);
        }
    }

    public function deleteMultiImg($id,Request $request)
    {
        $img = MultiImg::findOrFail($id);
        $this->imageStorage->delete($request, 'multi_img', $img,'photo_name', $id);
        $img->delete();
        $message = $this->createToastrMessage('Your image has been deleted','success');
        return redirect(route('manage-products.index', $img->product_id))->with($message);

    }
}

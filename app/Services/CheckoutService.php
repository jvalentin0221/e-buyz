<?php

namespace App\Services;


class CheckoutService
{
    public function subtotal($cartItems):float
    {
        $subTotal = 0;
        foreach($cartItems as $cartItem) {
            $subTotal += $cartItem->product_price * $cartItem->requested_quantity;
        }
        return $subTotal;
    }

    public function removeCartItems($cartItems):void
    {
        foreach($cartItems as $cart) {
            $cart->delete();
            session()->forget([
                'colorsEN' . $cart->id,
                'sizesEN' . $cart->id,
                'colorsES' . $cart->id,
                'sizesES' . $cart->id
            ]);
        }
        session()->forget([
            'checkout' . auth()->user()->id,
            'discount' . auth()->user()->id,
            'coupon_name' .auth()->user()->id,
        ]);
    }

}

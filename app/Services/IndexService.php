<?php

namespace App\Services;

use App\Models\Brand;
use App\Models\Category;
use App\Models\CompanyInfo;
use App\Models\Product;
use App\Models\ProductExtension;
use App\Models\Slider;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class IndexService
{
    public function showProducts():Collection
    {
        return Product::where('status',1)
            ->with('productExtension', 'reviews')
            ->inRandomOrder()
            ->limit(150)
            ->get();
    }

    public function showHotDeals():Collection
    {
       return ProductExtension::where('hot_deals',1)
            ->with('product')
            ->latest()
            ->limit(50)
            ->get();
    }

    public function showSpecialOffers():Collection
    {
        return ProductExtension::where('special_offer',1)
            ->with('product')
            ->inRandomOrder()
            ->limit(20)
            ->get();
    }

    public function showNewProducts():Collection
    {
        return ProductExtension::where('new',1)
            ->with('product')
            ->latest()
            ->limit(50)
            ->get();
    }

    public function bestSellers()
    {
        return DB::table('order_items')
            ->select('product_id', DB::raw('count(*) as total'))
            ->groupBy('product_id')
            ->orderBy('total', 'desc')
            ->limit(50)
            ->pluck('product_id');
    }

    public function showBestProducts($bestSellers):Collection
    {
       return Product::where('status',1)
            ->whereIn('id', $bestSellers)
            ->with('productExtension', 'reviews')
            ->inRandomOrder()
            ->limit(50)
            ->get([
                'product_thumbnail',
                'id',
                'product_slug_en',
                'discount_percentage',
                'product_name_en',
                'product_name_es',
                'selling_price',
                'discount_price',
                'product_qty'
            ]);
    }

    public function showCategoryProducts():Collection
    {
      return Category::with('products')
            ->limit(100)
            ->get();
    }

    public function showProductTags()
    {
        return DB::table('products')
            ->select('product_tags_en','product_tags_es')
            ->groupBy('product_tags_en','product_tags_es')
            ->InRandomOrder()
            ->limit(100)
            ->get(['product_tags_en', 'product_tags_es']);
    }

    public function cacheBrands():array
    {
        return Cache::remember('brands', Carbon::now()->addMinutes(60), function() {
            $brands = Brand::latest()->limit(5)->get();
            $brandCount = count($brands);
            return compact(['brands','brandCount']);
        });
    }


    public function cacheCategoryProducts():array
    {
       return Cache::remember('categoryProducts', Carbon::now()->addMinutes(60), function() {
            $hotDeals = $this->showHotDeals();
            $specialOffers = $this->showSpecialOffers();
            $newProducts = $this->showNewProducts();
            $bestSellers = $this->bestSellers();
            $bestProducts = $this->showBestProducts($bestSellers);
            $products = $this->showProducts();
            $productTags = $this->showProductTags();
            return compact(['hotDeals', 'specialOffers', 'newProducts', 'bestSellers','bestProducts','products', 'productTags' ]);
        });
    }

    public function cacheCategoryLevels():array
    {
        return Cache::remember('categoryLevels', Carbon::now()->addMinutes(60), function() {
            $categories = $this->showCategoryProducts();
            $subcategories = SubCategory::get();
            $subsubcategories = SubSubCategory::get();
            return compact(['categories', 'subcategories', 'subsubcategories']);
        });
    }

    public function cacheSliderCompanyInfo():array
    {
        return Cache::remember('sliderCompanyInfo', Carbon::now()->addMinutes(60), function() {
            $sliders = Slider::where('status',1)->orderBy('id','DESC')->limit(3)->get();
            $companyInfo = CompanyInfo::find(1);
            return compact(['sliders','companyInfo']);
        });
    }

}

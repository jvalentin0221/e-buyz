<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewService extends Controller
{
    public function userReviewCheck($userReviews,Request $request):bool
    {
        foreach($userReviews as $userReview){
            if($request->product_id == $userReview->product_id) {
                return true;
            }
        }
        return false;
    }
}

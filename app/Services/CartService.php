<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddCartRequest;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartService extends Controller
{
    public function getEnglishColorName(Request $request):string
    {
        $sessionEN = session()->get('colorsEN' . $request->cart_id);
        $sessionES = session()->get('colorsES' . $request->cart_id);
        $sessionEN = explode(',',$sessionEN);
        $sessionES = explode(',',$sessionES);
        $sessionEN = array_map('ucfirst', $sessionEN);
        $sessionES = array_map('ucfirst', $sessionES);

        $sessionCount = count($sessionES) - 1;
        $requestColorIndex = 0;
        for($i=0; $i <= $sessionCount; $i++) {
            if($sessionES[$i] == $request->product_color_es) {
                $requestColorIndex = $i;
            }
        }

        $englishColor ='';
        for($i=0; $i <= $sessionCount; $i++) {
            if($i == $requestColorIndex) {
                $englishColor = $sessionEN[$i];
            }
        }
        return $englishColor;
    }

    public function getEnglishSizeName(Request $request):string
    {
        $sessionEN = session()->get('sizesEN' . $request->cart_id);
        $sessionES = session()->get('sizesES' . $request->cart_id);
        $sessionEN = explode(',',$sessionEN);
        $sessionES = explode(',',$sessionES);
        $sessionEN = array_map('ucfirst', $sessionEN);
        $sessionES = array_map('ucfirst', $sessionES);

        $sessionCount = count($sessionES) - 1;
        $requestSizeIndex = 0;
        for($i=0; $i <= $sessionCount; $i++) {
            if($sessionES[$i] == $request->product_size_es) {
                $requestSizeIndex = $i;
            }
        }

        $englishSize ='';
        for($i=0; $i <= $sessionCount; $i++) {
            if($i == $requestSizeIndex) {
                $englishSize = $sessionEN[$i];
            }
        }
        return $englishSize;
    }

    public function duplicateItemCheck(Request $request):bool
    {
        $cartItems = Cart::where('user_id', auth()->user()->id)->get(['product_id']);
        foreach($cartItems as $cartItem) {
            if($request->product_id == $cartItem->product_id) {
              return true;
            }
        }
        return false;
    }

    public function quantityProductCheck(Request $request):bool
    {
        $product = Product::findOrFail($request->product_id);
        if($product->product_qty < 1) {
            return true;
        }
        return false;
    }

    public function addToCart(Cart $cart,AddCartRequest $request):RedirectResponse
    {
        $quantityCheck = $this->quantityProductCheck($request);
        $duplicateItem = $this->duplicateItemCheck($request);
        if($duplicateItem === true) {
            $message = $this->createToastrMessage('This product has already been added to your cart.','info');
            return redirect()->back()->with($message);
        }
        if($quantityCheck === true) {
            $message = $this->createToastrMessage('This product is out of stock.','info');
            return redirect()->back()->with($message);
        }
        if(auth()->check()){
            try {
                $createCart = '';
                if(session()->get('language') == 'spanish') {
                    $createCart = $cart->create($request->validated() + [
                        'user_id' => auth()->user()->id,
                        'product_color_es' => $request->product_colors,
                        'product_size_es' => $request->product_sizes,
                    ]);
                } else {
                $createCart = $cart->create($request->validated() + [
                        'user_id' => auth()->user()->id,
                        'product_color_en' => $request->product_colors,
                        'product_size_en' => $request->product_sizes,
                    ]);
                }

                Session::put('colorsEN' . $createCart->id , $request->product_color_en);
                Session::put('colorsES' . $createCart->id , $request->product_color_es);
                Session::put('sizesEN' . $createCart->id , $request->product_size_en);
                Session::put('sizesES' . $createCart->id , $request->product_size_es);
                $message = $this->createToastrMessage('This product has been successfully added to your cart','success');
                return redirect()->back()->with($message);

            } catch (\Exception $e)  {
                $message = $this->createToastrMessage('You cannot add items to your cart at this time. Please try again later','info');
                return redirect()->back()->with($message);
            }
        } else {
            $message = $this->createToastrMessage('Please sign in to start adding items to your cart','info');
            return redirect()->back()->with($message);
        }
    }
}

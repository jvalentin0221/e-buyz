<?php

namespace App\Services;

use App\Models\Order;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class DashboardService
{
    public function pendingOrders():LengthAwarePaginator
    {
        return DB::table('orders')
            ->oldest('order_date')
            ->where('status', '=', 'pending')
            ->paginate(10);
    }

    public function allPendingOrders():int
    {
        $count = Order::where('status', '=', 'pending')->get();
        return count($count);
    }

    public function salesToday():float
    {
        $date = date('F d Y');
        return Order::where('order_date',$date)->sum('amount');
    }

    public function monthlySales():float
    {
        $month = date('F');
        return  Order::where('order_month',$month)->sum('amount');
    }

    public function yearlySales():float
    {
        $year = date('Y');
        return Order::where('order_year',$year)->sum('amount');
    }
}

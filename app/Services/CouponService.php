<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CouponService extends Controller
{
    public function addCoupon(Request $request)
    {
       $coupons = Coupon::select('coupon_name', 'coupon_discount', 'coupon_validity')->get();
       foreach ($coupons as $coupon ) {
           if($request->coupon_code == $coupon->coupon_name && $coupon->coupon_validity >= Carbon::now()->format('m/d/y')){
               $discount = $coupon->coupon_discount;
               $coupon_name = $coupon->coupon_name;
               Session::put('discount' . auth()->user()->id, $discount );
               Session::put('coupon_name' . auth()->user()->id, $coupon_name );
               $message = $this->createToastrMessage('Your coupon has been applied','success');
               return redirect(route('cart.index'))->with($message);
           }
       }

       $message = $this->createToastrMessage('Your coupon is invalid','error');
       return redirect(route('cart.index'))->with($message);
    }

    public function removeCoupon()
    {
        session()->forget('discount' . auth()->user()->id);
        $message = $this->createToastrMessage('Your Coupon has been removed','success');
        return redirect(route('cart.index'))->with($message);
    }
}

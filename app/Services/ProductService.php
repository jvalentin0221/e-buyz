<?php

namespace App\Services;

use App\Models\MultiImg;
use App\Models\ProductExtension;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductService
{
    public function createProduct($product, $productRequest, $productExtensionRequest, $categoryId):int
    {
       return DB::transaction(function () use($product, $productRequest,$productExtensionRequest, $categoryId) {
            $images = $productRequest->file('multi_img');
            $newProduct = $product->create($productRequest->validated() + [
                    'product_slug_en' => Str::slug($productRequest->product_name_en),
                    'product_slug_es' => Str::slug($productRequest->product_name_es),
                    'category_id' => $categoryId,
                    'status' => 1
                ]);
            ProductExtension::create($productExtensionRequest->validated() + [
                    'product_id' => $newProduct->id
                ]);
            foreach($images as $img) {
                MultiImg::create([
                    'product_id' => $newProduct->id,
                    'photo_name' => $img
                ]);
            }
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            return $newProduct->id;
        });
    }
}

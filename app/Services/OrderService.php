<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\View\View;

class OrderService extends Controller
{
    public function confirmedOrders():View
    {
        $orders = Order::where('status','confirmed')->latest()->paginate(25);
        return view('admin.order.confirmed_orders',compact('orders'));
    }

    public function processingOrders():View
    {
        $orders = Order::where('status','processing')->latest()->paginate(25);
        return view('admin.order.processing_orders',compact('orders'));
    }

    public function pickedOrders():View
    {
        $orders = Order::where('status','picked')->latest()->paginate(25);
        return view('admin.order.picked_orders',compact('orders'));
    }

    public function shippedOrders():View
    {
        $orders = Order::where('status','shipped')->latest()->paginate(25);
        return view('admin.order.shipped_orders',compact('orders'));
    }

    public function deliveredOrders():View
    {
        $orders = Order::where('status','delivered')->latest()->paginate(25);
        return view('admin.order.delivered_orders',compact('orders'));
    }

    public function returnedOrders():View
    {
        $orders = Order::where('return_date', '!=' , NULL)->latest()->paginate(25);
        return view('admin.order.returned_orders',compact('orders'));
    }

    public function cancelledOrders():View
    {
        $orders = Order::where('status','cancelled')->latest()->paginate(25);
        return view('admin.order.cancelled_orders',compact('orders'));
    }
}

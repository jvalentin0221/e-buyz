<?php

namespace App\Providers;

use App\Classes\Payments\CashOnDelivery;
use App\Interfaces\PaymentInterface;
use App\Classes\Payments\Stripe;
use App\Http\Controllers\Payment\StripeController;
use App\Http\Controllers\Payment\CashOnDeliveryController;
use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(StripeController::class)
            ->needs(PaymentInterface::class)
            ->give(Stripe::class);

        $this->app->when(CashOnDeliveryController::class)
            ->needs(PaymentInterface::class)
            ->give(CashOnDelivery::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

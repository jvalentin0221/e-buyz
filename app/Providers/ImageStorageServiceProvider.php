<?php

namespace App\Providers;

use App\Classes\ImageStorage\PublicFileStorage;
use App\Classes\ImageStorage\S3FileStorage;
use App\Interfaces\ImageStorageInterface;
use Illuminate\Support\ServiceProvider;

class ImageStorageServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $environment = $this->app->environment();

        if($environment == 'testing' || $environment == 'local' || $environment == 'development') {
            $this->app->bind(
                ImageStorageInterface::class,
                PublicFileStorage::class
            );
        } else {
            $this->app->bind(
                ImageStorageInterface::class,
                S3FileStorage::class
            );
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

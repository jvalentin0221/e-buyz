<?php

namespace App\Providers;

use App\Http\View\Composers\CartComposer;
use App\Providers\ImageStorageServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(ImageStorageServiceProvider::class);
        $this->app->register(PaymentServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        View::composer([
            'frontend.index',
            'frontend.show',
            'frontend.show_product_tags',
            'frontend.cart.index',
            'frontend.checkout.index',
            'frontend.payment.*',
            'frontend.product_categories.*',
            'frontend.search.*',
            'frontend.track.*',
            'user.*',
            'auth.*'
        ], CartComposer::class);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\ToastrBuilder;

class OnlyViewMyOrders
{
    use ToastrBuilder;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $orderRoute = $request->route('order');
        if(Auth::check() && Auth::user()->id == $orderRoute->user_id) {
            return $next($request);
        } else {
            $message = $this->createToastrMessage('You have no access rights to view other users orders','error');
            return redirect(route('index'))->with($message);
        }
    }
}

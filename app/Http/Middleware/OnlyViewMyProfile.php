<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Traits\ToastrBuilder;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OnlyViewMyProfile
{
    use ToastrBuilder;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $userRoute = $request->route('user');
        if(Auth::check() && Auth::user()->id == $userRoute->id) {
            return $next($request);
        } else {
            $message = $this->createToastrMessage('You have no access rights to another users profile','error');
            return redirect(route('index'))->with($message);
        }
    }
}

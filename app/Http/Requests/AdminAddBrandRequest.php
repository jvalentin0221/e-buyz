<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminAddBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_name_en' => 'required|max:255',
            'brand_name_es' => 'required|max:255',
            'brand_slug_en' => 'required|max:255',
            'brand_slug_es' => 'required|max:255',
            'brand_url' => 'required|max:255',
            'brand_image' => 'required|image|max:10000'
        ];
    }
}

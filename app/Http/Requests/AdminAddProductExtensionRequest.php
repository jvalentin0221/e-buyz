<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminAddProductExtensionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'short_description_en' => 'required|string|max:255',
            'short_description_es' => 'required|string|max:255',
            'long_description_en' => 'required|string|max:255',
            'long_description_es' => 'required|string|max:255',
            'product_code' => 'required|string|max:255',
            'hot_deals' => 'integer|max:255',
            'new' => 'integer|max:255',
            'special_offer' => 'integer|max:255',
            'special_deals' => 'integer|max:255',
        ];
    }
}

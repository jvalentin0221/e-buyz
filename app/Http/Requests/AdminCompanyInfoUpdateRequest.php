<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminCompanyInfoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'image|max:10000',
            'phone_number_one' => 'min:8|max:11',
            'phone_number_two' => 'min:8|max:11',
            'email' => 'max:255',
            'company_name' => 'max:255',
            'company_address' => 'max:255',
            'facebook' => 'max:255',
            'twitter' => 'max:255',
            'linkedin' => 'max:255',
            'youtube' => 'max:255',
        ];
    }
}

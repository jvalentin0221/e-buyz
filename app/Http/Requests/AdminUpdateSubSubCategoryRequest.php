<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminUpdateSubSubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_title' => 'string|max:255',
            'subcategory_title' => 'string|max:255',
            'subsubcategory_name_en' => 'max:255',
            'subsubcategory_name_es' => 'max:255',
            'subsubcategory_slug_en' => 'max:255',
            'subsubcategory_slug_es' => 'max:255',
        ];
    }
}

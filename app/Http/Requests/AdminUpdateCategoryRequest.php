<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminUpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_name_en' => 'max:255',
            'category_name_es' => 'max:255',
            'category_slug_en' => 'max:255',
            'category_slug_es' => 'max:255',
            'category_icon' => 'max:255'
        ];
    }
}

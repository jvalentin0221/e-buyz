<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|integer',
            'product_name_en' => 'required|string|max:255',
            'product_name_es' => 'required|string|max:255',
            'product_thumbnail' => 'required|string|max:255',
            'product_code' => 'required|string|max:255',
            'product_price' => 'required|numeric|max:100000',
            'product_slug_en' => 'required|string|max:255',
            'requested_quantity' => 'required|integer|max:100000',
            'quantity' => 'required|integer|max:10000',
        ];
    }
}

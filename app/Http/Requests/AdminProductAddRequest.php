<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminProductAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_title' => 'required|string|max:255',
            'subcategory_title' => 'required|string|max:255',
            'subsubcategory_title' => 'required|string|max:255',
            'product_name_en' => 'required|string|max:255',
            'product_name_es' => 'required|string|max:255',
            'product_qty' => 'required|integer|max:100000',
            'product_tags_en' => 'required|string|max:255',
            'product_tags_es' =>'required|string|max:255',
            'product_size_en' => 'max:255',
            'product_size_es' => 'max:255',
            'product_color_en' => 'max:255',
            'product_color_es' => 'max:255',
            'selling_price' => 'required|max:255',
            'discount_price' => 'required|max:255',
            'discount_percentage' => 'integer|max:100',
            'product_thumbnail' => 'required|image|max:10000',
            'status' => 'max:255'
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminManageProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_title' => 'string|max:255',
            'subcategory_title' => 'string|max:255',
            'subsubcategory_title' => 'string|max:255',
            'product_name_en' => 'string|max:255',
            'product_name_es' => 'string|max:255',
            'product_qty' => 'integer|max:100000',
            'product_tags_en' => 'string|max:255',
            'product_tags_es' =>'string|max:255',
            'product_size_en' => 'max:255',
            'product_size_es' => 'max:255',
            'product_color_en' => 'max:255',
            'product_color_es' => 'max:255',
            'selling_price' => 'numeric|max:100000',
            'discount_price' => 'numeric|max:100000',
            'discount_percentage' => 'integer|max:100',
            'status' => 'integer|max:255'
        ];
    }
}

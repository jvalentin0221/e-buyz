<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_method' => 'required|string|max:255',
            'shipping_name' => 'required|string|max:255',
            'shipping_email' => 'required|max:100|unique:users,email,'.auth()->user()->id,
            'shipping_phone' => 'required|string|min:8|max:11',
            'shipping_address' => 'required|string|max:255',
            'zip_code' => 'required|integer|min:10000|max:99999',
            'state' => 'required|string|max:2',
            'city' => 'required|string|max:255',
            'notes' => 'max:255',
        ];
    }
}

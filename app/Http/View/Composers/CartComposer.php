<?php

namespace App\Http\View\Composers;

use App\Models\Cart;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class CartComposer
{
    public function compose(View $view)
    {
        if(Auth::check()) {
            $userCartItems = Cart::where('user_id', Auth::user()->id)->get();
            $subTotal = 0;
            foreach($userCartItems as $cartItem) {
                $subTotal += $cartItem->product_price * $cartItem->requested_quantity;
            }

            $view->with('subTotal',$subTotal);
            $view->with('userCartItems', $userCartItems);
        }
    }
}

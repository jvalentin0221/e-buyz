<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminUpdatePasswordRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class AdminPasswordController extends Controller
{
    public function index(User $user):View
    {
        try{
            return view('admin.password.index', compact('user'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(AdminUpdatePasswordRequest $request, User $user):RedirectResponse
    {
        try{
            $hashedPassword = $user->password;

            if(Hash::check($request->old_password, $hashedPassword)) {
                $user->password = Hash::make($request->new_password);
                $user->save();
                $message = $this->createToastrMessage('Admin Password Updated Successfully','success');
                return redirect()->route('admin.dashboard')->with($message);
            } else {
                $message = $this->createToastrMessage('Current password is invalid','error');
                return redirect()->back()->with($message);
            }
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update your password at this time. Please try again at a later time','error');
            return redirect()->route('admin.dashboard')->with($message);
        }
    }
}

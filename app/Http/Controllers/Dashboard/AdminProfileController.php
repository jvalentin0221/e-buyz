<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminProfileEditRequest;
use App\Interfaces\ImageStorageInterface;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AdminProfileController extends Controller
{
    public object $imageStorage;

    public function __construct(ImageStorageInterface $imageStorage)
    {
        $this->imageStorage = $imageStorage;
    }

    public function index(User $user):View
    {
        try {
            return view('admin.profile.index', compact('user'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function edit(User $user):View
    {
        try{
            return view('admin.profile.edit', compact('user'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(AdminProfileEditRequest $request,User $user):RedirectResponse
    {
        try{
            if($request->hasFile('avatar')) {
                $this->imageStorage->delete($request, 'avatar', $user, 'avatar', Auth::user()->id);
                $this->imageStorage->upload($request, $user, 'avatar', Auth::user()->id );
            }
            $user->update([
                'name' => $request->name,
                'email' => $request->email
            ]);
            $message = $this->createToastrMessage('Admin Profile Updated Successfully','success');
            return redirect()->route('admin.profile', auth()->user()->id)->with($message);

        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update this profile at this time. Please try again at a later time','error');
            return redirect()->route('admin.profile', auth()->user()->id )->with($message);
        }
    }
}

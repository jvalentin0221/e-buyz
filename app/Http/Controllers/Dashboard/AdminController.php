<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Services\DashboardService;
use Illuminate\View\View;

class AdminController extends Controller
{
    public object $dashboardService;

    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    public function index():View
    {
        try{
            $allOrders = $this->dashboardService->allPendingOrders();
            $pendingOrders = $this->dashboardService->pendingOrders();
            $salesToday = $this->dashboardService->salesToday();
            $monthlySales = $this->dashboardService->monthlySales();
            $yearlySales = $this->dashboardService->yearlySales();
            return view('admin.index', compact(['pendingOrders', 'salesToday', 'monthlySales', 'yearlySales', 'allOrders']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }
}

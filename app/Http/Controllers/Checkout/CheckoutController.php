<?php

namespace App\Http\Controllers\Checkout;

use App\Http\Requests\AddCheckoutRequest;
use App\Models\Cart;
use App\Http\Controllers\Controller;
use App\Services\CheckoutService;
use App\Services\IndexService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class CheckoutController extends Controller
{
    public object $checkoutService;
    public object $indexService;

    public function __construct(CheckoutService $checkoutService, IndexService $indexService)
    {
        $this->checkoutService = $checkoutService;
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
    }

    public function index():View | RedirectResponse
    {
        try{
            $cartItems = Cart::where('user_id', Auth::user()->id)->get();
            $subTotal = $this->checkoutService->subtotal($cartItems);
            if($subTotal > 0) {
                return view('frontend.checkout.index', compact('cartItems'));
            } else {
                $message = $this->createToastrMessage('Your cart must have at least 1 item to proceed to checkout ','info');
                return redirect()->to('/')->with($message);
            }
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function store(AddCheckoutRequest $request):RedirectResponse
    {
        try{
            Session::put('checkout' . auth()->user()->id, $request->validated());
             if($request->payment_method === 'stripe'){
                return redirect()->route('stripe.index');
            } else {
                return redirect()->route('cod.index');
            }
        } catch (\Exception) {
            $message = $this->createToastrMessage('You cannot checkout at this time. Please try again at a later time','error');
            return redirect(route('frontend.checkout.index'))->with($message);
        }
    }
}

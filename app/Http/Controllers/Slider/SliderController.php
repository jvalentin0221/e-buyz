<?php

namespace App\Http\Controllers\Slider;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminSliderAddRequest;
use App\Http\Requests\AdminSliderUpdateRequest;
use App\Interfaces\ImageStorageInterface;
use App\Models\Slider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class SliderController extends Controller
{
    public object $imageStorage;

    public function __construct(ImageStorageInterface $imageStorage)
    {
        $this->imageStorage = $imageStorage;
    }

    public function index():View
    {
        try{
            $sliders = Slider::latest()->paginate(25);
            return view('admin.slider.index',compact('sliders'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function store(AdminSliderAddRequest $request, Slider $slider):RedirectResponse
    {
        try{
            $sliderId = $slider->create($request->validated())->id;
            $this->imageStorage->resizeImage($request,'slider_img',900,400,$sliderId);
            Cache::forget('sliderCompanyInfo');
            $message = $this->createToastrMessage('A new has slider has been created','success');
            return redirect(route('sliders.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot add this slider at this time. Please try again at a later time','error');
            return redirect(route('sliders.index'))->with($message);
        }
    }

    public function edit(Slider $slider):View
    {
        try{
            return view('admin.slider.edit',compact('slider'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(Slider $slider, AdminSliderUpdateRequest $request):RedirectResponse
    {
        try{
            $slider->fill($request->validated());
            if($slider->isDirty()) {
                if($request->has('slider_img')) {
                    $this->imageStorage->delete($request,'slider_img',$slider,'slider_img',$slider->id);
                    $this->imageStorage->resizeImage($request,'slider_img',900,400,$slider->id);
                }
                Cache::forget('sliderCompanyInfo');
                $slider->save();
                $message = $this->createToastrMessage('Your slider has been updated.', 'success');
                return redirect(route('sliders.index'))->with($message);
            } else {
                $message = $this->createToastrMessage('No new data has been updated.', 'info');
                return redirect(route('sliders.index'))->with($message);
            }
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update this slider at this time. Please try again at a later time','error');
            return redirect(route('sliders.index'))->with($message);
        }
    }

    public function destroy(Slider $slider, Request $request):RedirectResponse
    {
        try{
            $this->imageStorage->delete($request,'slider_img',$slider,'slider_img',$slider->id);
            $slider->delete();
            Cache::forget('sliderCompanyInfo');
            $message = $this->createToastrMessage('Your slider has been deleted.', 'success');
            return redirect(route('sliders.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update this slider at this time. Please try again at a later time','error');
            return redirect(route('sliders.index'))->with($message);
        }
    }
}

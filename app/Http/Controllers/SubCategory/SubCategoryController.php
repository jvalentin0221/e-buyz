<?php

namespace App\Http\Controllers\SubCategory;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAddSubCategoryRequest;
use App\Http\Requests\AdminUpdateSubCategoryRequest;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class SubCategoryController extends Controller
{
    public function index():View
    {
        try{
            $category = Category::orderBy('category_name_en', 'ASC')->get(['id','category_name_en']);
            $subcategories = SubCategory::latest()->paginate(25);
            return view('admin.subcategory.index', compact(['category', 'subcategories']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function store(AdminAddSubCategoryRequest $request,SubCategory $subcategory):RedirectResponse
    {
        try{
            $categoryId = Category::where('category_name_en', $request->category_title)->value('id');
            $subcategory->create($request->validated() + [
                    'category_id' => $categoryId
                ]);
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('A New Subcategory Has Been Created','success');
            return redirect(route('subcategories.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot add this subcategory at this time. Please try again at a later time','error');
            return redirect(route('subcategories.index'))->with($message);
        }
    }

    public function edit(SubCategory $subcategory):View
    {
        try{
            $category = Category::orderBy('category_name_en', 'ASC')->get(['id','category_name_en']);
            return view('admin.subcategory.edit', compact(['subcategory','category']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(AdminUpdateSubCategoryRequest $request, SubCategory $subcategory):RedirectResponse
    {
        try{
            $categoryId = Category::where('category_name_en', $request->category_title)->value('id');
            $subcategory->update($request->validated() + [
                    'category_id' => $categoryId
                ]);
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('Your Subcategory Has Been Updated','success');
            return redirect(route('subcategories.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update this subcategory at this time. Please try again at a later time','error');
            return redirect(route('subcategories.index'))->with($message);
        }
    }

    public function destroy(SubCategory $subcategory):RedirectResponse
    {
        try{
            $subcategory->delete();
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('Your subcategory has been deleted','success');
            return redirect(route('subcategories.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot delete this subcategory at this time. Please try again at a later time','error');
            return redirect(route('subcategories.index'))->with($message);
        }
    }
}

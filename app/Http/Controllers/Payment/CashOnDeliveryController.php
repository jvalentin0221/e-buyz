<?php

namespace App\Http\Controllers\Payment;

use App\Models\Cart;
use App\Services\CheckoutService;
use App\Http\Controllers\Controller;
use App\Interfaces\PaymentInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderMail;
use Illuminate\View\View;

class CashOnDeliveryController extends Controller
{
    public object $payment;
    public object $checkoutService;

    public function __construct(PaymentInterface $payment, CheckoutService $checkoutService)
    {
        $this->payment = $payment;
        $this->checkoutService = $checkoutService;
    }

    public function index():View
    {
        try{
            return $this->payment->view();
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function store():RedirectResponse
    {
        try{
            $cartItems = Cart::where('user_id', Auth::user()->id)->get();
            $subTotal = $this->checkoutService->subtotal($cartItems);

            if (Session::has('discount' . Auth::user()->id)) {
                $totalAmount = $subTotal - $subTotal * session()->get('discount' . Auth::user()->id) / 100;
            }else{
                $totalAmount = $subTotal;
            }
            $createOrder = $this->payment->store($totalAmount, $cartItems);
            $this->checkoutService->removeCartItems($cartItems);
            Mail::to($createOrder->email)->send(new OrderMail($createOrder));

            $message = $this->createToastrMessage('Your order has been placed successfully','success');
            return redirect(route('index'))->with($message);
        }  catch(\Exception){
            $message = $this->createToastrMessage('You cannot place this order at this time. Please try again at a later time','error');
            return redirect(route('index'))->with($message);
        }
    }
}

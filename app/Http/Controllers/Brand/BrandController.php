<?php

namespace App\Http\Controllers\Brand;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAddBrandRequest;
use App\Http\Requests\AdminUpdateBrandRequest;
use App\Interfaces\ImageStorageInterface;
use App\Models\Brand;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class BrandController extends Controller
{
    public object $imageStorage;

    public function __construct(ImageStorageInterface $imageStorage)
    {
        $this->imageStorage = $imageStorage;
    }

    public function index(Brand $brands):View
    {
        try{
            $brands = Brand::latest()->paginate(20);
            return view('admin.brand.index', compact(['brands']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function store(AdminAddBrandRequest $request, Brand $brand):RedirectResponse
    {
        try{
            $brandId = $brand->create($request->validated())->id;
            $this->imageStorage->resizeImage($request, 'brand_image','300', '300', $brandId);
            Cache::forget('brands');
            $message = $this->createToastrMessage('A New Brand Has Been Created','success');
            return redirect(route('brands.index'))->with($message);
        } catch(\Exception $exception){
            $message = $this->createToastrMessage('Your new brand cannot be created at this time.','error');
            return redirect(route('brands.index'))->with($message);
        }
    }


    public function edit(Brand $brand):View
    {
        try{
            return view('admin.brand.edit', compact('brand'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(AdminUpdateBrandRequest $request, Brand $brand):RedirectResponse
    {
        try{
            if($request->hasFile('brand_image')) {
                $brand->update($request->validated());
                $this->imageStorage->delete($request, 'brand_image', $brand, 'brand_name_en', $brand->id);
                $this->imageStorage->resizeImage($request, 'brand_image','300', '300', $brand->id);
                Cache::forget('brands');
                $message = $this->createToastrMessage('Your Brand Has Been Updated','success');
                return redirect(route('brands.index'))->with($message);
            } else {
                $brand->update($request->validated());
                Cache::forget('brands');
                $message = $this->createToastrMessage('Your Brand Has Been Updated','success');
                return redirect(route('brands.index'))->with($message);
            }
        } catch(\Exception $exception){
            $message = $this->createToastrMessage('Your cannot update this brand at the moment. Please try again at a later time','error');
            return redirect(route('brands.index'))->with($message);
        }
    }

    public function destroy(Request $request, Brand $brand):RedirectResponse
    {
        try{
            $this->imageStorage->delete($request, 'brand_image', $brand,'brand_name_en', $brand->id);
            $brand->delete();
            Cache::forget('brands');
            $message = $this->createToastrMessage('Your Brand Has Been Deleted','success');
            return redirect(route('brands.index'))->with($message);
        } catch(\Exception $exception){
            $message = $this->createToastrMessage('Your cannot delete this brand at the moment. Please try again at a later time','error');
            return redirect(route('brands.index'))->with($message);
        }
    }
}

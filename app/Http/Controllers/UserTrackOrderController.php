<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserTrackOrderController extends Controller
{
    public function checkInvoiceCode(Request $request): View | RedirectResponse
    {
        try{
            $invoiceCode = $request->invoice_code;
            $invoice = Order::where('invoice_no',$invoiceCode)->first();
            if ($invoice) {
                return view('frontend.track.index',compact('invoice'));
            }else{
                $message = $this->createToastrMessage('Invoice code is invalid','error');
                return redirect(route('user.order.index'))->with($message);
            }
        } catch(\Exception){
            $message = $this->createToastrMessage('An issue finding your invoice has occur. Please try again at a later time','error');
            return redirect(route('user.order.index'))->with($message);
        }
    }
}

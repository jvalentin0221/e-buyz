<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAddProductExtensionRequest;
use App\Http\Requests\AdminProductAddRequest;
use App\Interfaces\ImageStorageInterface;
use App\Models\Category;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AdminProductController extends Controller
{
    public object $imageStorage;
    public object $productService;

    public function __construct(ImageStorageInterface $imageStorage,ProductService $productService)
    {
        $this->imageStorage = $imageStorage;
        $this->productService = $productService;
    }

    public function index():View
    {
        try {
            $categories = Category::latest()->get();
            return view('admin.product.index', compact(['categories']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
      }

    public function store(AdminProductAddRequest $productRequest,AdminAddProductExtensionRequest $productExtensionRequest, Product $product):RedirectResponse
    {
        try{
            $categoryId = Category::where('category_name_en', $productRequest->category_title)->value('id');
            $newProductId = $this->productService->createProduct($product,$productRequest,$productExtensionRequest,$categoryId);
            $this->imageStorage->resizeImage($productRequest, 'product_thumbnail','400', '400', $newProductId);
            $this->imageStorage->resizeMultipleImages($productExtensionRequest, 'multi_img','500', '500', $newProductId);
            $message = $this->createToastrMessage('A New Product Has Been Created','success');
            return redirect(route('admin.product.index',))->with($message);
        }  catch(\Exception){
            $message = $this->createToastrMessage('You cannot add this product at this time. Please try again at a later time','error');
            return redirect(route('admin.product.index'))->with($message);
        }
    }
}

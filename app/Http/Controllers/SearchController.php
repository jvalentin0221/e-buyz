<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Services\IndexService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SearchController extends Controller
{
    public object $indexService;

    public function __construct(IndexService $indexService)
    {
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
    }

    public function searchProducts(Request $request):View | RedirectResponse
    {
        try{
            $search = $request->search;
            $searchProducts = Product::with('productExtension')
            ->where('product_name_en','LIKE',"%$search%")
            ->where('product_name_es','LIKE',"%$search%")->paginate(10);
            return view('frontend.search.index',compact(['searchProducts']));
        } catch(\Exception){
            $message = $this->createToastrMessage('An issue occurred during the search process. Please try again at a later time','error');
            return redirect(route('frontend.index'))->with($message);
        }
    }
}

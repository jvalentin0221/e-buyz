<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAddCategoryRequest;
use App\Http\Requests\AdminUpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class CategoryController extends Controller
{

    public function index(Category $categories):View
    {
        try{
            $categories = Category::latest()->paginate(25);
            return view('admin.category.index', compact('categories'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function store(AdminAddCategoryRequest $request, Category $category):RedirectResponse
    {
        try{
            $category->create($request->validated());
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('A New Category Has Been Created','success');
            return redirect(route('categories.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot add a category at this time. Please try again at a later time','error');
            return redirect(route('categories.index'))->with($message);
        }
    }

    public function edit(Category $category):View
    {
        try{
            return view('admin.category.edit', compact('category'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }


    public function update(AdminUpdateCategoryRequest $request, Category $category):RedirectResponse
    {
        try{
            $category->update($request->validated());
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('Your Category Has Been Updated','info');
            return redirect(route('categories.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update a category at this time. Please try again at a later time','error');
            return redirect(route('categories.index'))->with($message);
        }

    }

    public function destroy(Category $category):RedirectResponse
    {
        try {
            $category->delete();
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('Your Category Has Been Deleted', 'success');
            return redirect(route('categories.index'))->with($message);
        }  catch(\Exception){
            $message = $this->createToastrMessage('You cannot update a category at this time. Please try again at a later time','error');
            return redirect(route('categories.index'))->with($message);
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(LoginRequest $request): \Illuminate\Http\RedirectResponse
    {
        $input = $request->validated();

        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))) {
           return $this->redirectlogin();
        } else {
            return redirect()->back()->with('errorMsg', 'Your email/password is incorrect');
        }
    }
}

<?php

namespace App\Http\Controllers\Return;

use App\Models\Order;
use App\Services\IndexService;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ReturnController extends Controller
{
    public object $indexService;

    public function __construct(IndexService $indexService)
    {
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
    }

    public function index():View
    {
        try{
            $orders = Order::where('user_id',Auth::id())->where('return_reason','!=',NULL)->latest()->paginate(15);
            return view('user.return.index', compact( 'orders'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(Request $request, Order $order):RedirectResponse
    {
        try{
            $order->update([
                'return_date' => Carbon::now()->format('F d Y'),
                'return_reason' => $request->return_reason,
            ]);

            $message = $this->createToastrMessage('Your return request has been sent.','success');
            return redirect(route('user.return.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update this return at this time. Please try again at a later time','error');
            return redirect(route('user.return.index'))->with($message);
        }
    }
}

<?php

namespace App\Http\Controllers\Cancel;

use App\Models\Order;
use App\Http\Controllers\Controller;
use App\Services\IndexService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CancelController extends Controller
{
    public object $indexService;

    public function __construct(IndexService $indexService)
    {
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
    }

    public function index():View
    {
        try{
            $orders = Order::where('user_id',Auth::user()->id)->where('status','cancelled')->latest()->paginate(15);
            return view('user.cancel.index',compact('orders'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(Order $order):RedirectResponse
    {
        try{
            $order->update([
                'status' => 'cancelled',
                'cancel_date' => now()
            ]);
            $message = $this->createToastrMessage('Your order has been cancelled','success');
            return redirect()->route('user.order.index')->with($message);
        } catch(\Exception $exception){
            $message = $this->createToastrMessage('You cannot cancel this order as this time. Please try again at a later time','error');
            return redirect(route('user.order.index', Auth::user()->id))->with($message);
        }

    }
}

<?php

namespace App\Http\Controllers\PublishReview;

use App\Models\Review;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class PublishReviewController extends Controller
{
    public function index():View
    {
        try{
            $reviews = Review::where('status',1)->with('product','user')->latest()->paginate(25);
            return view('admin.publish-review.index', compact('reviews'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function destroy(Review $review):RedirectResponse
    {
        try{
            $review->delete();
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('This review has been deleted','success');
            return redirect(route('admin.publish_review.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot delete this review at this time. Please try again at a later time','error');
            return redirect(route('admin.publish_review.index'))->with($message);
        }
    }
}

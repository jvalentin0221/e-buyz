<?php

namespace App\Http\Controllers\Review;

use App\Http\Requests\AddReviewRequest;
use App\Http\Controllers\Controller;
use App\Models\Review;
use App\Services\ReviewService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class ReviewController extends Controller
{

    public object $reviewService;

    public function __construct(ReviewService $reviewService)
    {
        $this->reviewService = $reviewService;
    }

    public function index():View
    {
        try{
            $reviews = Review::where('status',0)->with('product','user')->latest()->paginate(25);
            return view('admin.review.index', compact('reviews'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function store(AddReviewRequest $request, Review $review):RedirectResponse
    {
        try{
            $userReviews = Review::where('user_id',auth()->user()->id)->latest()->get();
            $reviewCheck = $this->reviewService->userReviewCheck($userReviews,$request);
            if($reviewCheck == true) {
                $message = $this->createToastrMessage('A User can only leave one review per product.','error');
                return redirect(route('index.show', $request->product_slug_en))->with($message);
            }

            $review->create($request->validated() + [
                    'product_id' => $request->product_id,
                    'user_id' => Auth::user()->id,
                ]);

            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('A new review has been created. Awaiting approval from admin.','success');
            return redirect(route('index.show', $request->product_slug_en))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot add this review at this time. Please try again at a later time','error');
            return redirect(route('index.show', $request->product_slug_en))->with($message);
        }
    }

    public function update(Review $review):RedirectResponse
    {
        try{
            $review->update(['status' => 1]);
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('This review has been successfully approved','success');
            return redirect(route('admin.review.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update this review at this time. Please try again at a later time','error');
            return redirect(route('admin.review.index'))->with($message);
        }
    }
}

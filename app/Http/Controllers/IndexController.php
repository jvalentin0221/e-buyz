<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Review;
use App\Services\IndexService;
use Illuminate\View\View;

class IndexController extends Controller
{
    public object $indexService;

    public function __construct(IndexService $indexService)
    {
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
        $this->indexService->cacheBrands();
;    }

    public function index():View
    {
        return view('frontend.index');
    }

    public function show(string $slug):View
    {
        try{
            $product = Product::where('product_slug_en', $slug)->with('multiImgs', 'productExtension')->first();
            $relatedProducts = Product::where('category_id',$product->category_id)
                ->where('id','!=', $product->id)
                ->with('reviews')
                ->inRandomOrder()
                ->limit(50)
                ->get();

            $reviews = Review::with('product','user')
                ->where('product_id',$product->id)
                ->where('status',1)
                ->latest()
                ->get();

            $relatedReviews = Review::where('product_id',$product->id)->where('status',1)->latest()->get();
            $avgReviews = Review::where('product_id',$product->id)->where('status',1)->avg('rating');
            return view('frontend.show',compact(['product','relatedProducts','avgReviews','relatedReviews','reviews']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function showProductTags(string $tag):View
    {
        try{
            $tags = Product::where('status',1)
                ->where('product_tags_en',$tag)
                ->orderBy('id','DESC')
                ->paginate(9);

            return view('frontend.show_product_tags', compact('tags'));

        } catch(\Exception $exception) {
            return view('errors.404');
        }
    }

    public function showSubCategories(string $name):View
    {
        try{
            $subcategories_product = Product::where('status',1)
                ->where('subcategory_title', $name)
                ->with('productExtension','reviews')
                ->orderBy('id','DESC')
                ->paginate(10);

            return view('frontend.product_categories.sub_category', compact('subcategories_product'));
        } catch(\Exception $exception) {
            return view('errors.404');
        }
    }

    public function showSubSubCategories(string $name):View
    {
        try{
            $subsubcategories_product = Product::where('status',1)
                ->where('subsubcategory_title', $name)
                ->with('productExtension','reviews')
                ->orderBy('id','DESC')
                ->paginate(10);

            return view('frontend.product_categories.sub_sub_category', compact('subsubcategories_product'));
        } catch(\Exception $exception) {
            return view('errors.404');
        }
    }
}

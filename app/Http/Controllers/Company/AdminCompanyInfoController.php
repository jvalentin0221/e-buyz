<?php

namespace App\Http\Controllers\Company;

use App\Http\Requests\AdminCompanyInfoUpdateRequest;
use App\Interfaces\ImageStorageInterface;
use App\Http\Controllers\Controller;
use App\Models\CompanyInfo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class AdminCompanyInfoController extends Controller
{
    public object $imageStorage;

    public function __construct(ImageStorageInterface $imageStorage)
    {
        $this->imageStorage = $imageStorage;
    }

    public function edit(CompanyInfo $company_info):View
    {
        try{
            return view('admin.company.edit',compact('company_info'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(CompanyInfo $company_info, AdminCompanyInfoUpdateRequest $request):RedirectResponse
    {
        try{
            if($request->hasFile('logo')) {
                $company_info->update($request->validated());
                $this->imageStorage->delete($request, 'logo', $company_info, 'logo', $company_info->id);
                $this->imageStorage->upload($request, $company_info,'logo', $company_info->id);
                Cache::forget('sliderCompanyInfo');
                $message = $this->createToastrMessage('Your Company Info Has Been Updated','success');
                return redirect(route('admin.dashboard'))->with($message);
            } else {
                $company_info->update($request->validated());
                Cache::forget('sliderCompanyInfo');
                $message = $this->createToastrMessage('Your company info has ben updated','success');
                return redirect(route('admin.dashboard'))->with($message);
            }
        } catch (\Exception) {
            $message = $this->createToastrMessage('You cannot update company info at this time. Please try again at a later time','error');
            return redirect(route('admin.dashboard'))->with($message);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function english():RedirectResponse
    {
        session()->get('language');
        session()->forget('language');
        Session::put('language','english');
        return redirect()->back();
    }

    public function spanish():RedirectResponse
    {
        session()->get('language');
        session()->forget('language');
        Session::put('language','spanish');
        return redirect()->back();
    }
}

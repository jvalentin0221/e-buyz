<?php

namespace App\Http\Controllers\SubSubCategory;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAddSubSubCategoryRequest;
use App\Http\Requests\AdminUpdateSubSubCategoryRequest;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class SubSubCategoryController extends Controller
{
    public function index():View
    {
        try{
            $category = Category::orderBy('category_name_en', 'ASC')->get(['id','category_name_en']);
            $subSubCategories = SubSubCategory::latest()->paginate(25);
            return view('admin.subsubcategory.index', compact(['category', 'subSubCategories']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function store(AdminAddSubSubCategoryRequest $request,SubSubCategory $subsubcategory):RedirectResponse
    {
        try{
            $subcategoryId = SubCategory::where('subcategory_name_en', $request->subcategory_title)->value('id');
            $subsubcategory->create($request->validated() + [
                    'sub_category_id' => $subcategoryId
                ]);
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('A New Sub-subcategory Has Been Created','success');
            return redirect(route('subsubcategories.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot add this subsubcategory at this time. Please try again at a later time','error');
            return redirect(route('subsubcategories.index'))->with($message);
        }
    }

    public function edit(User $user, SubSubCategory $subsubcategory):View
    {
        try{
            $categories = Category::orderBy('category_name_en', 'ASC')->get(['id','category_name_en']);
            $subcategories = SubCategory::orderBy('subcategory_name_en','ASC')->get();
            $user = User::find(Auth::user()->id);
            return view('admin.subsubcategory.edit', compact(['subsubcategory', 'user','categories','subcategories']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(AdminUpdateSubSubCategoryRequest $request, SubSubCategory $subsubcategory):RedirectResponse
    {
        try{
            $subcategoryId = SubCategory::where('subcategory_name_en', $request->subcategory_title)->pluck('id');
            $subsubcategory->update($request->validated() + [
                    'subcategory_id' => $subcategoryId[0]
                ]);
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('Your Sub-Subcategory Has Been Updated','success');
            return redirect(route('subsubcategories.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update this subsubcategory at this time. Please try again at a later time','error');
            return redirect(route('subsubcategories.index'))->with($message);
        }
    }

    public function destroy(SubSubCategory $subsubcategory):RedirectResponse
    {
        try{
            $subsubcategory->delete();
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $message = $this->createToastrMessage('Your Sub-Subcategory has been deleted','success');
            return redirect(route('subsubcategories.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot delete this subsubcategory at this time. Please try again at a later time','error');
            return redirect(route('subsubcategories.index'))->with($message);
        }
    }
}

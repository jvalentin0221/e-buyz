<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdatePasswordRequest;
use App\Models\User;
use App\Services\IndexService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class UserPasswordController extends Controller
{
    public $indexService;

    public function __construct(IndexService $indexService)
    {
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
    }

    public function index(User $user):View
    {
        try{
            return view('user.password.index', compact('user'));
        }  catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(UserUpdatePasswordRequest $request,User $user):RedirectResponse
    {
        try{
            $hashedPassword = $user->password;
            if(Hash::check($request->old_password, $hashedPassword)) {
                $user->password = Hash::make($request->new_password);
                $user->save();
                $message = $this->createToastrMessage('User Password Updated Successfully','success');
                return redirect()->route('user.profile', auth()->user()->id)->with($message);
            } else {
                $message = $this->createToastrMessage('Current password is invalid','error');
                return redirect()->back()->with($message);
            }
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update your password at this time. Please try again at a later time','error');
            return redirect()->route('user.profile', auth()->user()->id)->with($message);
        }
    }
}

<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserProfileEditRequest;
use App\Interfaces\ImageStorageInterface;
use App\Mail\CouponMail;
use App\Models\User;
use App\Services\IndexService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class UserProfileController extends Controller
{
    public object $imageStorage;
    public object $indexService;

    public function __construct(ImageStorageInterface $imageStorage, IndexService $indexService)
    {
        $this->imageStorage = $imageStorage;
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
    }

    public function index(User $user):View
    {
        try{
            return view('user.edit', compact('user'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(UserProfileEditRequest $request,User $user):RedirectResponse
    {
        try{
            if(empty($user->address)) {
                Mail::to($user->email)->send(new CouponMail());
            }
            $user->update($request->validated());
            if($request->hasFile('avatar')) {
                $this->imageStorage->delete($request,'avatar',$user,'avatar', $user->id);
                $this->imageStorage->upload($request,$user,'avatar', $user->id);
            }
            $message = $this->createToastrMessage('User Profile Updated Successfully','success');
            return redirect()->route('user.profile', $user->id)->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update your profile at this time. Please try again at a later time','error');
            return redirect()->route('user.profile', $user->id)->with($message);
        }
    }
}

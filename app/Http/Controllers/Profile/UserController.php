<?php

namespace App\Http\Controllers\Profile;


use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\IndexService;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{
    public object $indexService;

    public function __construct(IndexService $indexService)
    {
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
    }

    public function index(User $user):View
    {
        try{
            $message ='';
            if(empty($user->address && $user->phone_number && $user->state && $user->zip_code && $user->city )) {
                $message ='Update your profile now and get a free 10% off on your first purchase';
            }
            return view('user.profile', compact(['user', 'message']));
        }  catch(\Exception $exception){
            return view('errors.404');
        }
    }
}

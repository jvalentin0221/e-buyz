<?php

namespace App\Http\Controllers\Order;

use App\Models\Order;
use App\Models\OrderItem;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class AdminOrderController extends Controller
{
    public function index():View
    {
        try{
            $orders = Order::where('status','Pending')->latest()->paginate(25);
            return view('admin.order.index',compact('orders'));
        }  catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function show(Order $order):View
    {
        try{
            $order = Order::where('id', $order->id)->with('order_items')->first();
            $orderItems = OrderItem::with('product')->where('order_id',$order->id)->latest()->get();
            return view('admin.order.show', compact( ['order', 'orderItems']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }
}

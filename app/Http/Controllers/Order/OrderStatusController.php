<?php

namespace App\Http\Controllers\Order;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class OrderStatusController extends Controller
{
    public function pendingToConfirmed(Order $order):RedirectResponse
    {
        try{
            Order::findOrFail($order->id)->update([
                'status' => 'confirmed',
                'confirmed_date' => now()
            ]);
            $message = $this->createToastrMessage('Your order has been confirmed successfully','success');
            return redirect(route('admin.order.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot confirm this order at this time. Please try again at a later time','error');
            return redirect(route('admin.order.index'))->with($message);
        }
    }

    public function confirmedToProcessing(Order $order):RedirectResponse
    {
        try{
            Order::findOrFail($order->id)->update([
                'status' => 'processing',
                'processing_date' => now()
            ]);
            $message = $this->createToastrMessage('Your order has been processed successfully','success');
            return redirect(route('admin.order.confirmed_orders'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot process this order at this time. Please try again at a later time','error');
            return redirect(route('admin.order.confirmed_orders'))->with($message);
        }
    }

    public function processingToPicked(Order $order):RedirectResponse
    {
        try{
            Order::findOrFail($order->id)->update([
                'status' => 'picked',
                'picked_date' => now()
            ]);
            $message = $this->createToastrMessage('Your order has been picked successfully','success');
            return redirect(route('admin.order.processing_orders'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot pick this order at this time. Please try again at a later time','error');
            return redirect(route('admin.order.processing_orders'))->with($message);
        }
    }

    public function pickedToShipped(Order $order):RedirectResponse
    {
        try{
            Order::findOrFail($order->id)->update([
                'status' => 'shipped',
                'shipped_date' => now()
            ]);
            $message = $this->createToastrMessage('Your order has been shipped successfully','success');
            return redirect(route('admin.order.picked_orders'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot ship this order at this time. Please try again at a later time','error');
            return redirect(route('admin.order.picked_orders'))->with($message);
        }
    }

    public function shippedToDelivered(Order $order):RedirectResponse
    {
        try{
            $product = OrderItem::where('order_id',$order->id)->get();
            foreach ($product as $item) {
                Product::where('id',$item->product_id)
                    ->update([
                        'product_qty' => DB::raw('product_qty-'.$item->qty),
                    ]);
            }
            Order::findOrFail($order->id)->update([
                'status' => 'delivered',
                'delivered_date' => now()
            ]);
            $message = $this->createToastrMessage('Your order has been delivered successfully','success');
            return redirect(route('admin.order.picked_orders'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot delivery this order at this time. Please try again at a later time','error');
            return redirect(route('admin.order.picked_orders'))->with($message);
        }
    }

    public function returnRequestToReturned(Order $order):RedirectResponse
    {
        try{
            Order::findOrFail($order->id)->update([
                'status' => 'returned',
                'return_date' => now()
            ]);
            $message = $this->createToastrMessage('Your order has been returned successfully','success');
            return redirect(route('admin.order.returned_orders'))->with($message);
        }  catch(\Exception){
            $message = $this->createToastrMessage('You cannot return this order at this time. Please try again at a later time','error');
            return redirect(route('admin.order.returned_orders'))->with($message);
        }
    }
}

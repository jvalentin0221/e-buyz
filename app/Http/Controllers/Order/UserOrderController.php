<?php

namespace App\Http\Controllers\Order;

use App\Models\Order;
use App\Models\OrderItem;
use App\Services\IndexService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserOrderController extends Controller
{
    public object $indexService;

    public function __construct(IndexService $indexService)
    {
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
    }

    public function index():View
    {
        try{
            $orders = Order::where('user_id', Auth::user()->id)
                ->latest()
                ->paginate(15);
            return view('user.order.index', compact( 'orders'));
        }  catch(\Exception){
            return view('errors.404');
        }
    }

    public function show(Order $order):View
    {
        try{
            $order = Order::where('id', $order->id)->with('order_items')->first();
            $orderItems = OrderItem::with('product','productExtension')
                ->where('order_id',$order->id)
                ->orderBy('id','DESC')
                ->get();
            return view('user.order.show', compact( ['order', 'orderItems']));
        } catch(\Exception){
            return view('errors.404');
        }
    }

    public function createInvoice(Order $order):Response | RedirectResponse
    {
        try{
            $order = Order::where('id', $order->id)->with('order_items')->first();
            $orderItems = OrderItem::with('product','productExtension')->where('order_id',$order->id)->get();
            $pdf = PDF::loadView('user.order.invoice',compact(['order','orderItems']))->setPaper('a4')->setOptions([
                'tempDir' => public_path(),
                'chroot' => public_path(),
            ]);
            return $pdf->download('invoice.pdf');
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot download your invoice at this time. Please try again at a later time','error');
            return redirect(route('user.order.index'))->with($message);
        }
    }
}

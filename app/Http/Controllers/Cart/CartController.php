<?php

namespace App\Http\Controllers\Cart;

use App\Models\Cart;
use App\Http\Controllers\Controller;
use App\Services\CartService;
use App\Services\IndexService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CartController extends Controller
{
    public object $indexService;
    public object $cartService;

    public function __construct(IndexService $indexService, CartService $cartService)
    {
        $this->cartService = $cartService;
        $this->indexService = $indexService;
        $this->indexService->cacheCategoryProducts();
        $this->indexService->cacheCategoryLevels();
        $this->indexService->cacheSliderCompanyInfo();
    }

    public function index():View
    {
        try{
            $cartItems = Cart::where('user_id', Auth::user()->id)->get();
            return view('frontend.cart.index',compact('cartItems'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(Request $request):RedirectResponse
    {
        try{
            $cartItem = Cart::find($request->cart_id);
            if($cartItem->quantity >= $request->requested_quantity) {
                if(session()->get('language') == 'spanish') {
                   $englishColor = $this->cartService->getEnglishColorName($request);
                   $englishSize = $this->cartService->getEnglishSizeName($request);
                    $cartItem->update([
                        "requested_quantity" => $request->requested_quantity,
                        "product_color_en" => $englishColor,
                        "product_color_es" => $request->product_color_es,
                        'product_size_en' => $englishSize,
                        'product_size_es' => $request->product_size_es,
                    ]);
                } else {
                    $cartItem->update([
                        "requested_quantity" => $request->requested_quantity,
                        "product_color_en" => $request->product_color_en,
                        'product_size_en' => $request->product_size_en,
                    ]);
                }
                $message = $this->createToastrMessage('Your cart item has been updated','success');
                return redirect(route('cart.index'))->with($message);
            } else {
                $message = $this->createToastrMessage('Your requested quantity exceeds the amount available','info');
                return redirect(route('cart.index'))->with($message);
            }
        } catch(\Exception $exception){
            $message = $this->createToastrMessage('Your Cart cannot be updated at this time. Please try again at a later time','error');
            return redirect(route('cart.index'))->with($message);
        }
    }

    public function delete(Request $request):RedirectResponse
    {
        try{
            session()->forget([
                'discount' . auth()->user()->id,
                'colorsEN' . $request->cart_id,
                'colorsES' . $request->cart_id,
                'sizesEN' . $request->cart_id,
                'sizesES' . $request->cart_id
            ]);
            $cartItem = Cart::find($request->cart_id);
            $cartItem->delete();
            $message = $this->createToastrMessage('Your cart item has been successfully deleted','success');
            return redirect(route('cart.index'))->with($message);
        } catch(\Exception $exception){
            $message = $this->createToastrMessage('Your Cart cannot be deleted at this time. Please try again at a later time','error');
            return redirect(route('cart.index'))->with($message);
        }
    }
}

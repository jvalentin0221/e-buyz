<?php

namespace App\Http\Controllers\ManageProduct;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminManageProductUpdateRequest;
use App\Http\Requests\AdminProductExtensionUpdateRequest;
use App\Interfaces\ImageStorageInterface;
use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;

class ManageProductController extends Controller
{
    public object $imageStorage;

    public function __construct(ImageStorageInterface $imageStorage)
    {
        $this->imageStorage = $imageStorage;
    }

    public function index():View
    {
        try{
            $products = Product::latest()->with('productExtension')->paginate(25);
            return view('admin.manage-product.index', compact('products'));
        }  catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function edit(Product $manage_product):View
    {
        try{
            $product = $manage_product;
            $categories = Category::latest()->get();
            $subcategories = SubCategory::latest()->get();
            $subsubcategories = SubSubCategory::latest()->get();
            return view('admin.manage-product.edit', compact(['categories', 'subcategories', 'subsubcategories', 'product']));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(AdminManageProductUpdateRequest $productRequest, AdminProductExtensionUpdateRequest $productExtensionRequest, Product $manage_product):RedirectResponse
    {
        try{
            $manage_product->fill($productRequest->validated() + [
                    'product_slug_en' => Str::slug($productRequest->product_name_en),
                    'product_slug_es' => Str::slug($productRequest->product_name_es),
                ]);

            $manage_product->productExtension->fill($productExtensionRequest->validated() + [
                    'product_id' => $manage_product->id
                ]);

            if ($manage_product->isDirty() || $manage_product->productExtension->isDirty()) {
                DB::transaction(function () use ($manage_product) {
                    $manage_product->save();
                    $manage_product->productExtension->save();
                });
                Cache::forget('categoryLevels');
                Cache::forget('categoryProducts');
                $message = $this->createToastrMessage('Your product has been updated.', 'success');
                return redirect(route('manage-products.index'))->with($message);
            } else {
                $message = $this->createToastrMessage('No new data has been updated.', 'info');
                return redirect(route('manage-products.index'))->with($message);
            }
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update this product at this time. Please try again at a later time','error');
            return redirect(route('manage-products.index'))->with($message);
        }

    }

    public function destroy(Request $request, Product $manage_product):RedirectResponse
    {
        try{
            $this->imageStorage->delete($request,'product_thumbnail',$manage_product,'product_thumbnail', $manage_product->id);
            foreach ($manage_product->multiimgs as $img) {
                $this->imageStorage->delete($request,'multi_img',$img,'photo_name', $manage_product->id);
            }
            Cache::forget('categoryLevels');
            Cache::forget('categoryProducts');
            $manage_product->delete();

            $message = $this->createToastrMessage('Your product has been deleted.', 'success');
            return redirect(route('manage-products.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot delete this product at this time. Please try again at a later time','error');
            return redirect(route('manage-products.index'))->with($message);
        }
    }
}

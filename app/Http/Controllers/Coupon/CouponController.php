<?php

namespace App\Http\Controllers\Coupon;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminAddCouponRequest;
use App\Http\Requests\AdminUpdateCouponRequest;
use App\Models\Coupon;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CouponController extends Controller
{
    public function index():View
    {
        try{
            $coupons = Coupon::latest()->paginate(25);
            return view('admin.coupon.index', compact('coupons'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function store(AdminAddCouponRequest $request, Coupon $coupon):RedirectResponse
    {
        try{
            $coupon->create($request->validated());
            $message = $this->createToastrMessage('A new coupon has been created','success');
            return redirect(route('coupons.index'))->with($message);
        }  catch(\Exception){
            $message = $this->createToastrMessage('You cannot add a coupon at this time. Please try again at a later time','error');
            return redirect(route('coupons.index'))->with($message);
        }
    }

    public function edit(Coupon $coupon):View
    {
        try{
            return view('admin.coupon.edit', compact('coupon'));
        } catch(\Exception $exception){
            return view('errors.404');
        }
    }

    public function update(AdminUpdateCouponRequest $request, Coupon $coupon):RedirectResponse
    {
        try{
            $coupon->update($request->validated());
            $message = $this->createToastrMessage('Your Coupon Has Been Updated','success');
            return redirect(route('coupons.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot update this coupon at this time. Please try again at a later time','error');
            return redirect(route('coupons.index'))->with($message);
        }
    }

    public function destroy(Coupon $coupon):RedirectResponse
    {
        try{
            $coupon->delete();
            $message = $this->createToastrMessage('Your coupon has been deleted','success');
            return redirect(route('coupons.index'))->with($message);
        } catch(\Exception){
            $message = $this->createToastrMessage('You cannot delete this coupon at this time. Please try again at a later time','error');
            return redirect(route('coupons.index'))->with($message);
        }
    }
}

<?php

namespace App\Interfaces;

interface ImageStorageInterface
{
    //Key name is the name of the field in DB
    public function upload($request, $model, $key_name,$fileId);

    public function delete($request, $key_name, $model, $column_name, $fileId);

    public function resizeImage($request, $key_name, $width, $height, $fileId);

    public function resizeMultipleImages($request, $key_name, $width, $height, $fileId);
}

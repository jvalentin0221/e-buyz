<?php

namespace App\Interfaces;

interface PaymentInterface
{
    public function view();

    public function store($totalAmount,$cartItems);
}

<?php

namespace App\Classes\ImageStorage;

use App\Interfaces\ImageStorageInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PublicFileStorage implements ImageStorageInterface
{
    public function upload($request, $model, $key_name, $fileId)
    {
        $file = $request->file($key_name);
        $filename = $file->getClientOriginalName();
        $file->storeAs($key_name . '/' . $fileId, $filename, 'public');
        $model->update([
            $key_name => $filename,
        ]);
    }

    public function delete($request, $key_name, $model, $column_name, $fileId)
    {
        Storage::disk('public')->delete('/' . $key_name . '/' . $fileId . '/' . $model->$column_name);
    }

    public function resizeImage($request, $key_name, $width, $height, $fileId)
    {
        $file = $request->file($key_name);
        $file = $file->getClientOriginalName();
        $image =  Image::make($request->file($key_name))->resize($width, $height);
        $image = $image->stream();
        Storage::disk('public')->put($key_name . '/' . $fileId . '/' . $file, $image->__toString());
    }

    public function resizeMultipleImages($request, $key_name, $width, $height, $fileId)
    {
        $images = $request->file($key_name);
        foreach($images as $img) {
            $file = $img->getClientOriginalName();
            $image =  Image::make($img)->resize($width, $height);
            $image = $image->stream();
            Storage::disk('public')->put($key_name . '/' . $fileId . '/' . $file, $image->__toString());
        }
    }
}

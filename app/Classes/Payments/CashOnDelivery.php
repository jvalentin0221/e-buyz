<?php

namespace App\Classes\Payments;

use App\Interfaces\PaymentInterface;
use App\Models\Order;
use App\Models\OrderItem;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CashOnDelivery implements PaymentInterface
{
    public  function view()
    {
        return view('frontend.payment.cod');
    }

    public function store($totalAmount, $cartItems)
    {
        $checkoutSession = session()->get('checkout' . Auth::user()->id);

        $createOrder = Order::Create([
            'user_id' => Auth::user()->id,
            'name' => $checkoutSession['shipping_name'] ?? Auth::user()->name,
            'email' => $checkoutSession['shipping_email'] ?? Auth::user()->email,
            'address' => $checkoutSession['shipping_address'] ?? Auth::user()->address,
            'state' => $checkoutSession['state'] ?? Auth::user()->state,
            'phone' => $checkoutSession['shipping_phone'] ?? Auth::user()->phone_number ,
            'zip_code' => $checkoutSession['zip_code'] ?? Auth::user()->zip_code ,
            'notes' => $checkoutSession['notes'] ?? '',
            'payment_method' => 'COD',
            'payment_type' => 'cash_on_delivery',
            'currency' => 'usd',
            'amount' => number_format($totalAmount,2, '.', ''),
            'invoice_no' => 'EB'.mt_rand(10000000,99999999),
            'order_date' => Carbon::now()->format('F d Y'),
            'order_month' => Carbon::now()->format('F'),
            'order_year' => Carbon::now()->format('Y'),
            'status' => 'pending',
        ]);

        foreach ($cartItems as $cart) {
            OrderItem::create([
                'order_id' => $createOrder->id,
                'product_id' => $cart->product_id,
                'color' => $cart->product_color_en,
                'size' => $cart->product_size_en,
                'qty' => $cart->requested_quantity,
                'price' => $cart->product_price,
            ]);
        }

        return $createOrder;
    }
}

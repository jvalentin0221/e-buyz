<?php

namespace App\Classes\Payments;

use App\Interfaces\PaymentInterface;
use App\Models\Order;
use App\Models\OrderItem;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Stripe implements PaymentInterface
{
    public  function view()
    {
        return view('frontend.payment.stripe');
    }

    public function store($totalAmount, $cartItems)
    {
        $checkoutSession = session()->get('checkout' . Auth::user()->id);
        $stripe = new \Stripe\StripeClient(
            config('keys.stripe_key')
        );

       $createIntent = $stripe->paymentIntents->create([
            'amount' => number_format($totalAmount,2, '.', '') * 100,
            'description' => 'E-Buyz',
            'currency' => 'usd',
            'payment_method_types' => ['card'],
            'metadata' => ['order_id' => uniqid()]
        ]);

        $confirmIntent = $stripe->paymentIntents->confirm(
            $createIntent->id,
            ['payment_method' => 'pm_card_visa']
        );

        $createOrder = Order::Create([
            'user_id' => Auth::user()->id,
            'name' => $checkoutSession['shipping_name'] ?? Auth::user()->name,
            'email' => $checkoutSession['shipping_email'] ?? Auth::user()->email,
            'state' => $checkoutSession['state'] ?? Auth::user()->state,
            'address' => $checkoutSession['shipping_address'] ?? Auth::user()->address,
            'phone' => $checkoutSession['shipping_phone'] ?? Auth::user()->phone_number ,
            'zip_code' => $checkoutSession['zip_code'] ?? Auth::user()->zip_code ,
            'notes' => $checkoutSession['notes'] ?? '',
            'payment_method' => 'Stripe',
            'payment_type' => 'Stripe',
            'transaction_id' => $confirmIntent->id,
            'currency' => $confirmIntent->currency,
            'amount' => number_format($totalAmount,2, '.', ''),
            'order_number' => $confirmIntent->metadata->order_id,
            'invoice_no' => 'EB'.mt_rand(10000000,99999999),
            'order_date' => Carbon::now()->format('F d Y'),
            'order_month' => Carbon::now()->format('F'),
            'order_year' => Carbon::now()->format('Y'),
            'status' => 'pending',
        ]);

        foreach ($cartItems as $cart) {
            OrderItem::create([
                'order_id' => $createOrder->id,
                'product_id' => $cart->product_id,
                'color' => $cart->product_color_en,
                'size' => $cart->product_size_en,
                'qty' => $cart->requested_quantity,
                'price' => number_format($cart->product_price,2, '.', '')
            ]);
        }

        return $createOrder;
    }
}

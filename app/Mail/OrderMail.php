<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $createOrder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($createOrder)
    {
        $this->createOrder = $createOrder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('jvalentin0221@gmail.com')->view('mail.order_mail',compact($this->createOrder))->subject('E-Buyz Receipt');
    }
}

<?php

namespace App\Traits;

trait ToastrBuilder
{
    public function createToastrMessage(string $message, string $alert): array
    {
       return array(
            'message' => $message,
            'alert-type' => $alert,
        );
    }
}

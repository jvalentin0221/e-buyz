<?php

namespace App\Traits;

use Illuminate\Http\RedirectResponse;

trait Redirectable
{
    public function redirectLogin():RedirectResponse
    {
        if(Auth()->user()->role == 'admin'){
            return redirect()->route('admin.dashboard', auth()->user()->id);
        }
        else if(Auth()->user()->role == 'user') {
            return redirect()->route('user.profile', auth()->user()->id);
        }
    }

    public function redirectRegister(): string
    {
        if(Auth()->user()->role == 'admin'){
            return route('admin.dashboard', auth()->user()->id);
        }
        else if(Auth()->user()->role == 'user') {
            return route('user.profile', auth()->user()->id);
        }
    }
}

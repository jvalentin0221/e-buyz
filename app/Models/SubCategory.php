<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class SubCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'category_title',
        'subcategory_name_en',
        'subcategory_name_es',
        'subcategory_slug_en',
        'subcategory_slug_es',
    ];

    public function setSubcategorySlugEnAttribute($value)
    {
        $this->attributes['subcategory_slug_en'] = Str::slug($this->subcategory_name_en);
    }

    public function setSubcategorySlugEsAttribute($value)
    {
        $this->attributes['subcategory_slug_es'] = Str::slug($this->subcategory_name_es);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function sub_sub_categories()
    {
        return $this->hasMany(SubSubCategory::class,'sub_category_id','id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Slider extends Model
{
    use HasFactory;

    protected $fillable = [
        'slider_img',
        'title',
        'description',
        'status'
    ];

    public function setSliderImgAttribute($value)
    {
        $environment = App::environment();
        if ($environment === 'testing') {
            $this->attributes['slider_img'] = $value;
        } else {
            $filename = $value->getClientOriginalName();
            $this->attributes['slider_img'] = $filename;
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class MultiImg extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function setPhotoNameAttribute($value)
    {
        $environment = App::environment();
        if ($environment === 'testing') {
            $this->attributes['photo_name'] = $value;
        } else {
            $filename = $value->getClientOriginalName();
            $this->attributes['photo_name'] = $filename;
        }
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}

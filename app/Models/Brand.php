<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class Brand extends Model
{
    use HasFactory;

    protected $fillable = [
        'brand_name_en',
        'brand_name_es',
        'brand_slug_en',
        'brand_slug_es',
        'brand_url',
        'brand_image',
    ];

    public function setBrandImageAttribute($value)
    {
        $environment = App::environment();
        if ($environment === 'testing') {
            $this->attributes['brand_image'] = $value;
        } else {
            $filename = $value->getClientOriginalName();
            $this->attributes['brand_image'] = $filename;
        }
    }

    public function setBrandSlugEnAttribute($value)
    {
        $this->attributes['brand_slug_en'] = Str::slug($this->brand_name_en);
    }

    public function setBrandSlugEsAttribute($value)
    {
        $this->attributes['brand_slug_es'] = Str::slug($this->brand_name_es);
    }
}

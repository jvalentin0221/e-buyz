<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class ProductExtension extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function setProductThumbnailAttribute($value)
    {
        $environment = App::environment();
        if ($environment === 'testing') {
            $this->attributes['product_thumbnail'] = $value;
        } else {
//            $filename = $value->getClientOriginalName();
            $this->attributes['product_thumbnail'] = $value;
        }
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}

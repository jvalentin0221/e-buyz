<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function setProductThumbnailAttribute($value)
    {
        $environment = App::environment();
        if ($environment === 'testing') {
            $this->attributes['product_thumbnail'] = $value;
        } else {
            $filename = $value->getClientOriginalName();
            $this->attributes['product_thumbnail'] = $filename;
        }
    }

    public function setDiscountPercentageAttribute($value)
    {
        $amount = $this->selling_price - $this->discount_price;
        $discount = ($amount/$this->selling_price) * 100;
        $this->attributes['discount_percentage'] = round($discount);
    }

    public function productExtension()
    {
        return $this->hasOne(ProductExtension::class);
    }

    public function multiImgs()
    {
        return $this->hasMany(MultiImg::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function order_items()
    {
        return $this->belongsTo(OrderItem::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function setCouponNameAttribute($value)
    {
        $this->attributes['coupon_name'] = strtoupper($value);
    }


    public function getCouponValidityAttribute(string $value)
    {
        return $this->attributes['coupon_validity'] = (new Carbon($value))->format('m/d/y');
    }
}

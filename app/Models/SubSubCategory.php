<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class SubSubCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'sub_category_id',
        'category_title',
        'subcategory_title',
        'subsubcategory_name_en',
        'subsubcategory_name_es',
        'subsubcategory_slug_en',
        'subsubcategory_slug_es',
    ];

    public function setSubSubCategorySlugEnAttribute($value)
    {
        $this->attributes['subsubcategory_slug_en'] = Str::slug($this->subsubcategory_name_en);
    }

    public function setSubSubCategorySlugEsAttribute($value)
    {
        $this->attributes['subsubcategory_slug_es'] = Str::slug($this->subsubcategory_name_es);
    }

    public function sub_categories()
    {
        return $this->hasMany(SubCategory::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'category_title','category_name_en');
    }
}

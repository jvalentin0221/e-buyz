<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_name_en',
        'category_name_es',
        'category_slug_en',
        'category_slug_es',
        'category_icon',
    ];

    public function getCategoryNameEnAttribute($value)
    {
        return ucfirst($value);
    }

    public function getCategoryNameEsAttribute($value)
    {
        return ucfirst($value);
    }

    public function setCategorySlugEnAttribute($value)
    {
        $this->attributes['category_slug_en'] = Str::slug($this->category_name_en);
    }

    public function setCategorySlugEsAttribute($value)
    {
        $this->attributes['category_slug_es'] = Str::slug($this->category_name_es);
    }

    public function sub_categories()
    {
        return $this->hasOne(SubCategory::class,'category_id');
    }
    public function sub_sub_categories()
    {
        return $this->hasMany(SubSubCategory::class,'category_title','category_name_en');
    }
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}

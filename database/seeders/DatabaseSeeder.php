<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
//            BrandSeeder::class,
            CategorySeeder::class,
            SubCategorySeeder::class,
            SubSubCategorySeeder::class,
//            ProductSeeder::class,
            ProductExtensionSeeder::class,
            MultiImgSeeder::class,
            OrderSeeder::class,
            OrderItemSeeder::class,
            SliderSeeder::class,
            CouponSeeder::class,
            CompanyInfoSeeder::class,
            ReviewSeeder::class,
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\ProductExtension;
use Illuminate\Database\Seeder;

class ProductExtensionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = ProductExtension::factory(500)->make();
        $products->chunk(100)->each(function($chunk) {
            ProductExtension::insert($chunk->toArray());
        });
    }
}

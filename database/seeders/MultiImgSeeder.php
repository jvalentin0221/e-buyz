<?php

namespace Database\Seeders;

use App\Models\MultiImg;
use Illuminate\Database\Seeder;

class MultiImgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MultiImg::factory()->count(50)->create();
    }
}

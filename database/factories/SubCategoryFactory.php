<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id'         => Category::all()->random()->id,
            'category_title'      => Category::all()->random()->category_name_en,
            'subcategory_name_en' => $this->faker->text(10),
            'subcategory_slug_en' => $this->faker->slug(),
            'subcategory_name_es' => $this->faker->text(10),
            'subcategory_slug_es' => $this->faker->slug(),
        ];
    }
}

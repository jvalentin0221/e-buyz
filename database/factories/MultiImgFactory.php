<?php

namespace Database\Factories;

use App\Models\MultiImg;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class MultiImgFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MultiImg::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => Product::all()->random()->id,
            'photo_name' => $this->faker->text(),
        ];
    }
}

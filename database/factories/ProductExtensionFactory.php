<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\ProductExtension;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductExtensionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductExtension::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'product_id' => Product::all()->random()->id,
            'product_id' => Product::factory()->create()->id,
            'short_description_en' => $this->faker->text(),
            'short_description_es' => $this->faker->text(),
            'long_description_en' => $this->faker->text(),
            'long_description_es' => $this->faker->text(),
            'product_code' => $this->faker->text(),
            'hot_deals' => 1,
            'new' => 1,
            'special_offer' => 1,
            'special_deals' => 1,
        ];
    }
}

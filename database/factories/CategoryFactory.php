<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_name_en' => $this->faker->text(10),
            'category_slug_en' => $this->faker->slug(),
            'category_name_es' => $this->faker->text(10),
            'category_slug_es' => $this->faker->slug(),
            'category_icon' => $this->faker->text()
        ];
    }
}

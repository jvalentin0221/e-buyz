<?php

namespace Database\Factories;

use App\Models\CompanyInfo;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyInfoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompanyInfo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'logo' => $this->faker->text(10),
            'phone_number_one' => $this->faker->numberBetween(1111111111,9999999999),
            'phone_number_two' => $this->faker->numberBetween(1111111111,9999999999),
            'email' => $this->faker->email,
            'company_name' => $this->faker->text(15),
            'company_address' => $this->faker->address,
            'facebook' => $this->faker->url,
            'twitter' => $this->faker->url,
            'linkedin' => $this->faker->url,
            'youtube' => $this->faker->url,
        ];
    }
}

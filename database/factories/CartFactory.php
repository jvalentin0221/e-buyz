<?php

namespace Database\Factories;

use App\Models\Cart;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CartFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cart::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => Product::all()->random()->id,
            'user_id' => User::all()->random()->id,
            'product_thumbnail' => $this->faker->text,
            'product_name_en' => $this->faker->text,
            'product_name_es' => $this->faker->text,
            'product_color_en' => $this->faker->text,
            'product_color_es' => $this->faker->text,
            'product_size_en' => $this->faker->text,
            'product_size_es' => $this->faker->text,
            'product_code' => $this->faker->text,
            'product_price' => $this->faker->numberBetween(0,100),
            'product_slug_en' => $this->faker->text,
            'requested_quantity' => $this->faker->numberBetween(0, 1000),
            'quantity' => $this->faker->numberBetween(0,1000),
        ];
    }
}

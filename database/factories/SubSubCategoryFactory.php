<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\SubCategory;
use App\Models\SubSubCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubSubCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubSubCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sub_category_id'        => SubCategory::all()->random()->id,
            'category_title'         => Category::all()->random()->category_name_en,
            'subcategory_title'      => SubCategory::all()->random()->subcategory_name_en,
            'subsubcategory_name_en' => $this->faker->text(10),
            'subsubcategory_slug_en' => $this->faker->slug(),
            'subsubcategory_name_es' => $this->faker->text(10),
            'subsubcategory_slug_es' => $this->faker->slug(),
        ];
    }
}

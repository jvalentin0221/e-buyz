<?php

namespace Database\Factories;

use App\Models\Brand;
use Illuminate\Database\Eloquent\Factories\Factory;

class BrandFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Brand::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
             'brand_name_en' => 'Apple',
             'brand_slug_en' => 'Apple',
             'brand_name_es' => 'Apple',
             'brand_slug_es' => 'Apple',
             'brand_url' => $this->faker->url,
             'brand_image' => $this->faker->text()
        ];
    }
}

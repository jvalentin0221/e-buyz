<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => Category::all()->random()->id,
            'category_title' => Category::all()->random()->category_name_en,
            'subcategory_title' => $this->faker->text(20),
            'subsubcategory_title' => $this->faker->text(20),
            'product_name_en' => $this->faker->text(20),
            'product_name_es' => $this->faker->text(20),
            'product_slug_en' => $this->faker->slug(),
            'product_slug_es' => $this->faker->slug(),
            'product_qty' => $this->faker->randomDigit(),
            'product_tags_en' =>  $this->faker->text(10),
            'product_tags_es' =>  $this->faker->text(10),
            'product_size_en' =>  $this->faker->word(),
            'product_size_es' =>  $this->faker->word(),
            'product_color_en' =>  $this->faker->word(),
            'product_color_es' =>  $this->faker->word(),
            'product_thumbnail' => $this->faker->text(),
            'selling_price' => $this->faker->numberBetween(100, 1000),
            'discount_price' => $this->faker->numberBetween(1,100),
            'discount_percentage' => $this->faker->numberBetween(0,100),
            'status' => 1
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Coupon;
use Illuminate\Database\Eloquent\Factories\Factory;

class CouponFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Coupon::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'coupon_name' => $this->faker->text(10),
            'coupon_discount' => $this->faker->numberBetween(0,100),
            'coupon_validity' => $this->faker->date('Y-m-d'),
            'status' => 1
        ];
    }
}

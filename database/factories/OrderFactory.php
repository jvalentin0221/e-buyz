<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'address' => $this->faker->address,
            'state' => 'FL',
            'phone' => $this->faker->realTextBetween(8,11),
            'zip_code' => $this->faker->numberBetween(10000,99999),
            'payment_type' => $this->faker->uuid,
            'payment_method' => 'Stripe',
            'transaction_id' => $this->faker->uuid,
            'currency' => 'usd',
            'amount' => $this->faker->numberBetween(1,1000),
            'order_number' => $this->faker->uuid,
            'invoice_no' => $this->faker->uuid,
            'order_date' => $this->faker->date(),
            'order_month' => $this->faker->month('now'),
            'order_year' => $this->faker->year('now'),
            'status' => "pending"
        ];
    }
}

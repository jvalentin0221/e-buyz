<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category_id')->unsigned();
            $table->string('category_title');
            $table->string('subcategory_title');
            $table->string('subsubcategory_title');
            $table->string('product_name_en');
            $table->string('product_name_es');
            $table->string('product_slug_en');
            $table->string('product_slug_es');
            $table->integer('product_qty');
            $table->string('product_tags_en');
            $table->string('product_tags_es');
            $table->string('product_size_en')->nullable();
            $table->string('product_size_es')->nullable();
            $table->string('product_color_en')->nullable();
            $table->string('product_color_es')->nullable();
            $table->string('product_thumbnail');
            $table->float('selling_price');
            $table->integer('discount_percentage')->nullable();
            $table->float('discount_price')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

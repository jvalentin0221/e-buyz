<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->string('product_name_en');
            $table->string('product_name_es');
            $table->string('product_thumbnail');
            $table->string('product_color_en')->nullable();
            $table->string('product_color_es')->nullable();
            $table->string('product_size_en')->nullable();
            $table->string('product_size_es')->nullable();
            $table->string('product_slug_en')->nullable();
            $table->string('product_code');
            $table->float('product_price');
            $table->integer('quantity');
            $table->integer('requested_quantity');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
